package com.firmanpro.pwoptimization.helper;

import android.app.Activity;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static java.lang.Double.parseDouble;

/**
 * Created by firmanmac on 8/21/17.
 */

public class MyConverting extends Activity {
    public final static String char_titik = "\\.";
    public final static String char_cacing = "\\~";
    public final static String char_koma = "\\,";

    public static String beautyDate(String strCurrentDate){
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date newDate = null;
        try {
            newDate = fmt.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        fmt = new SimpleDateFormat("EEE, d MMM yyyy");//MMM d, yyyy
        String date = fmt.format(newDate);
        return date;
    }

    public static String simple_date(String strCurrentDate){
        SimpleDateFormat fmt = new SimpleDateFormat("EEE, d MMM yyyy");
        Date newDate = null;
        try {
            newDate = fmt.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        fmt = new SimpleDateFormat("yyyy-MM-dd");
        String date = fmt.format(newDate);
        return date;
    }

    public static String format_angka_space(String angka){
        return NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(angka))).replace("."," ");
    }

    public static String format_angka(String angka){
        return NumberFormat.getInstance(Locale.GERMANY).format(parseDouble(String.valueOf(angka)));
    }

    public static String format_desimal_2(int angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static String format_desimal_2(float angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static String format_desimal_2(long angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static String format_desimal_2(double angka){
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(angka);
    }

    public static Spanned format_html(String teks){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(teks, Html.FROM_HTML_MODE_COMPACT);
        }else {
            return Html.fromHtml(teks);
        }
    }

    public static String[] Split(String value, String regex){
        return value.toString().trim().split(regex);
    }

    public static String DecodeBase64(String base64){
        byte[] valueDecoded= new byte[0];
        try {
            valueDecoded = Base64.decode(base64.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
        }
        return new String(valueDecoded);
    }

    public static String beautyTime(int second){
        String str = "";
        int resultJam = second / 3600;
        double resultMenit = ((double) second - ((double)resultJam * 3600.0)) / 60.0;

        if (resultJam > 0) {
            if (Math.round(resultMenit) > 0) {
                str = String.valueOf(resultJam) + " Jam " + String.valueOf(Math.round(resultMenit)) + " Menit";
            }
            else {
                str = String.valueOf(resultJam) + " Jam";
            }
        }
        else {
            str = String.valueOf(Math.round(resultMenit)) + " Menit";
        }

        return str;
    }

    public static String meterToKM(int meter){
        return format_desimal_2((double) meter / 1000.0) + " Km";
    }

    public static String secondToMinutes(int minutes){
        return format_desimal_2((double) minutes / 60.0) + " m";
    }

}
