package com.firmanpro.pwoptimization.helper;

/**
 * Created by firmanmac on 10/29/17.
 */

public class EnumProHelper {

    /**Paramater Statis*/
    public final String statusTersedia = "Tersedia";
    public final String statusTerpilih = "Terpilih";
    public final String statusNull = "Null";
    public final String greedyByProfit = "P";
    public final String greedyByWeight = "W";
    public final String greedyByDensity = "D";
    public final String keyLastItem = "Last Item";
    public final String maxProfit = "Max Profit";
    public final String maxProfit1 = "Max Profit 1";
    public final String maxProfit2 = "Max Profit 2";
    public final String maxProfit3 = "Max Profit 3";
    public final String minWeight = "Min Weight";
    public final String minWeight1 = "Min Weight 1";
    public final String minWeight2 = "Min Weight 2";
    public final String minWeight3 = "Min Weight 3";
    public final String maxDensity = "Max Density";
    public final String maxDensity1 = "Max Density 1";
    public final String maxDensity2 = "Max Density 2";
    public final String maxDensity3 = "Max Density 3";
    public final String keyModeGreedy = "modeGreedy";
    public final String keyProfitMaksimal = "Profit Maksimal";
    public final String keyBobotMinimal = "Bobot Minimal";
    public final String keyDensitasTertinggi = "Densitas Tertinggi";
    public final String keyShippingID= "keyShippingID";
    public final String keyIsAddNewCustomer = "keyIsAddNewCustomer";
    public final String keyCustomersID = "keyCustomersID";
    public final String keyCustomersProvince = "keyCustomersProvince";
    public final String keyPathDistance = "keyPathDistance";
    public final String keyPathDuration = "keyPathDuration";
    public final String keyPathLegs = "keyPathLegs";
    public final String keyGudang = "Gudang";
    public final String keyIsMenuShipping = "keyIsMenuShipping";
    public final String keyNo = "keyNo";
    public final String keyShortestRoutesID = "ShortestRoutesID";
    public final String keyOptimizationType = "OptimizationType";
    public final String keyTypeGoodWeight = "Good Weight";
    public final String keyTypeBadWeight = "Bad Weight";
    public final String keyTypeCombination = "Combination";
    public final String keyCustID = "keyCustID";
    public final String keyStatusOk = "OK";
    public final String keyStatusCancel = "Cancel";
    public final String keyModeFindDistance = "keyModeFindDistance";
    public final String keyShippingIDFindDistance = "keyShippingIDFindDistance";
    public final String yes = "yes";
    public final String no = "no";

}
