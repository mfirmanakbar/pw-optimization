package com.firmanpro.pwoptimization.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by firmanmac on 10/31/17.
 */

public class MySession {

    /**
     * #init first in class
     * MySession.beginInitialization(ParameterActivity.this);
     *
     * #set session
     * MySession.setSessionGlobal(MySession.KEY,"VALUE");
     *
     * #get session
     * MySession.getSessionGlobal(MySession.KEY);
     **/

    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;

    public static final String key_session_truck_maxcapacity = "key_session_truck_maxcapacity";
    public static final String key_session_warehouse_lat = "key_session_warehouse_lat";
    public static final String key_session_warehouse_lng = "key_session_warehouse_lng";
    public static final String key_session_api_customer_import = "key_session_api_customer_import";
    public static final String key_session_api_ordered_import = "key_session_api_ordered_import";
    public static final String key_session_shortest_routes_rest = "key_session_shortest_routes_rest";

    public static void beginInitialization(Context context){
        pref = context.getSharedPreferences("MySession", 0); /**0 - for private mode*/
        editor = pref.edit();
    }

    public static void setSessionGlobal(String key,String val){
        editor.putString(key, val);
        editor.commit();
    }

    public static String getSessionGlobal(String key){
        return pref.getString(key,null);
    }

    public static void clearSessionGlobal(String key){
        editor.remove(key);
        editor.commit();
    }

    public static void sessionClearEverything(){
        editor.clear();
        editor.commit();
    }

}
