package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.sqlite.repo.CustomerRepo;
import com.firmanpro.pwoptimization.sqlite.repo.OrderedRepo;
import com.firmanpro.pwoptimization.sqlite.repo.SeqRoutesRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingchildRepo;
import com.firmanpro.pwoptimization.sqlite.table.ShippingChildTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingTable;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.HashMap;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 12/9/17.
 */

public class StatisticsService {

    private CustomerRepo customerRepo;
    private OrderedRepo orderedRepo;
    private SeqRoutesRepo seqRoutesRepo;
    private ShippingRepo shippingRepo;
    private EnumProHelper enumProHelper = new EnumProHelper();

    public void getSumAllResult(Context context, TextView txtSumCustomers, TextView txtSumPackingOrder, TextView txtSumWeights, TextView txtSumProfits,
                                TextView txtSumGoodWeight, TextView txtSumBadWeight, TextView txtSumShipping, TextView txtSumDistance, TextView txtSumDuration) {

        txtSumCustomers.setText(MyConverting.format_angka(String.valueOf(countCustomers(context))) + " Store");
        txtSumPackingOrder.setText(MyConverting.format_angka(String.valueOf(countOrdered(context))) + " Pcs");
        txtSumWeights.setText(MyConverting.format_angka(String.valueOf(sumWeight(context))) + " gram");
        txtSumProfits.setText("Rp " + MyConverting.format_angka(String.valueOf(sumProfit(context))));
        txtSumGoodWeight.setText(MyConverting.format_angka(String.valueOf(countGoodWeight(context))) + " Items");
        txtSumBadWeight.setText(MyConverting.format_angka(String.valueOf(countBadWeight(context))) + " Items");
        txtSumShipping.setText(MyConverting.format_angka(String.valueOf(countShipping(context))) + " times");
        txtSumDistance.setText(sumDistance(context));
        txtSumDuration.setText(sumDuration(context));
    }

    private String sumDistance(Context context) {
        shippingRepo = new ShippingRepo(context);
        return MyConverting.meterToKM(shippingRepo.getSumDistance(false, enumProHelper.keyStatusOk));
    }

    private String sumDuration(Context context) {
        shippingRepo = new ShippingRepo(context);
        return MyConverting.secondToMinutes(shippingRepo.getSumDuration(false, enumProHelper.keyStatusOk));
    }

    private int countShipping(Context context) {
        shippingRepo = new ShippingRepo(context);
        return shippingRepo.countingRows(false, enumProHelper.keyStatusOk);
    }

    private int countBadWeight(Context context) {
        seqRoutesRepo = new SeqRoutesRepo(context);
        return seqRoutesRepo.countingRows(false);
    }

    private int countGoodWeight(Context context) {
        seqRoutesRepo = new SeqRoutesRepo(context);
        return seqRoutesRepo.countingRows(true);
    }

    private long sumProfit(Context context) {
        orderedRepo = new OrderedRepo(context);
        return orderedRepo.getSumProfit();
    }

    private long sumWeight(Context context) {
        orderedRepo = new OrderedRepo(context);
        return orderedRepo.getSumWeight();
    }

    private int countOrdered(Context context) {
        orderedRepo = new OrderedRepo(context);
        return orderedRepo.countingRows();
    }

    private int countCustomers(Context context) {
        customerRepo = new CustomerRepo(context);
        return customerRepo.countingRows();
    }

    private ArrayList<String> ar_shipping_id = new ArrayList<String>();
    private ArrayList<String> ar_shipping_weight = new ArrayList<String>();
    private ArrayList<String> ar_shipping_profit = new ArrayList<String>();
    private ArrayList<String> ar_shipping_item = new ArrayList<String>();
    private ArrayList<String> ar_shipping_distance = new ArrayList<String>();

    public void getChartResult(Context context, int typeChart, LineChart chartStatistics) {
        /**
         * typeChart 0 Item PCS
         * typeChart 1 Weight KG
         * typeChart 2 Profit RP
         * typeChart 3 Distance KM
         * */

        if (getShipping(context) > 0) {

            //clear chart
            chartStatistics.highlightValues(null);
            chartStatistics.invalidate();
            chartStatistics.clear();

            Legend l = chartStatistics.getLegend();
            l.setWordWrapEnabled(true);
            l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
            l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
            l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
            l.setDrawInside(false);
            l.setTextColor(Color.parseColor("#ffffff"));
            l.setTextSize(12f);

            chartStatistics.getDescription().setEnabled(false);
            chartStatistics.setDrawGridBackground(false);

            YAxis rightAxis = chartStatistics.getAxisRight();
            rightAxis.setEnabled(false);

            YAxis leftAxis = chartStatistics.getAxisLeft();
            leftAxis.setTextSize(12f);
            leftAxis.setTextColor(Color.WHITE);
            leftAxis.setXOffset(15f);

            XAxis xAxis = chartStatistics.getXAxis();
            xAxis.setTextColor(Color.parseColor("#ffffff"));
            xAxis.setTextSize(12f);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setAxisMinimum(0f);
            xAxis.setGranularity(1f);
            xAxis.setLabelCount(ar_shipping_id.size());
            xAxis.setValueFormatter(null);
            xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());

            drawChart(context, chartStatistics, typeChart);

        }else {
            Toasty.error(context,"No Data.", Toast.LENGTH_SHORT).show();
        }

    }

    private void drawChart(Context context, LineChart chartStatistics, int typeChart) {

        String title = "";
        int colorChart = 0, colorChartCircle = 0;
        ArrayList<Entry> entries1 = new ArrayList<Entry>();

        if (typeChart == 0){
            title = "Result by Item";
            colorChart = ContextCompat.getColor(context, R.color.colorBlue);
            colorChartCircle = ContextCompat.getColor(context, R.color.colorBlueDark);
            for (int a = 0; a < ar_shipping_item.size(); a++) {
                entries1.add(new Entry(a, Integer.parseInt(ar_shipping_item.get(a))));
            }
        }else if (typeChart == 1){
            title = "Result by Weight (Kg)";
            colorChart = ContextCompat.getColor(context, R.color.colorRed);
            colorChartCircle = ContextCompat.getColor(context, R.color.colorRedDark);
            for (int a = 0; a < ar_shipping_weight.size(); a++) {
                entries1.add(new Entry(a, (float) (Float.parseFloat(ar_shipping_weight.get(a)) / 1000.0)));
            }
        }else if (typeChart == 2){
            title = "Result by Profit (Rp)";
            colorChart = ContextCompat.getColor(context, R.color.colorGreen);
            colorChartCircle = ContextCompat.getColor(context, R.color.colorGreenSeaDark);
            for (int a = 0; a < ar_shipping_profit.size(); a++) {
                entries1.add(new Entry(a, Integer.valueOf(ar_shipping_profit.get(a))));
            }
        }else if (typeChart == 3){
            title = "Result by Distance (Km)";
            colorChart = ContextCompat.getColor(context, R.color.colorOrange);
            colorChartCircle = ContextCompat.getColor(context, R.color.colorOrangeDark);
            for (int a = 0; a < ar_shipping_distance.size(); a++) {
                entries1.add(new Entry(a, (float) (Float.parseFloat(ar_shipping_distance.get(a)) / 1000.0)));
            }
        }

        LineDataSet set1 = new LineDataSet(entries1, title);
        set1.setDrawIcons(false);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(colorChart);
        set1.setCircleColor(colorChartCircle);
        set1.setLineWidth(5f);
        set1.setCircleRadius(8f);
        set1.setDrawCircleHole(true);
        set1.setValueTextSize(12f);
        set1.setDrawFilled(false);
        set1.setFormLineWidth(4f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);
        set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        set1.setValueTextColor(Color.WHITE);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);
        chartStatistics.setData(data);

    }

    private class MyCustomXAxisValueFormatter implements IAxisValueFormatter {
        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            //update label dengan length yang bisa berubah2
            if(ar_shipping_id.size() > (int) value) {
                return ar_shipping_id.get((int) value);
            } else return null;
        }
    }

    private ShippingTable shippingTable;
    private ShippingchildRepo shippingchildRepo;
    private ShippingChildTable shippingChildTable;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;

    private int getShipping(Context context) {
        clearArray();
        shippingRepo = new ShippingRepo(context);
        shippingTable = new ShippingTable();
        theList = shippingRepo.getAllData(false, enumProHelper.keyStatusOk);
        if (theList.size()>0) {
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                ar_shipping_id.add(gen.get(shippingTable.key_shippingID));
                ar_shipping_weight.add(gen.get(shippingTable.key_totalWeigth));
                ar_shipping_profit.add(gen.get(shippingTable.key_totalProfit));
                ar_shipping_item.add(gen.get(shippingTable.key_totalItem));
                ar_shipping_distance.add(gen.get(shippingTable.key_totalDistance));
            }
        }
        return ar_shipping_id.size();
    }

    private void clearArray() {
        ar_shipping_id.clear();
        ar_shipping_weight.clear();
        ar_shipping_profit.clear();
        ar_shipping_item.clear();
        ar_shipping_distance.clear();
    }
}
