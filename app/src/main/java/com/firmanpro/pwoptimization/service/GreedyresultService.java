package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.adapter.GreedyresultAdapter;
import com.firmanpro.pwoptimization.centerformula.KnapsackGreedy;
import com.firmanpro.pwoptimization.entity.GreedyresultEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.sqlite.repo.OrderedRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingchildRepo;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 11/1/17.
 */

public class GreedyresultService {

    private KnapsackGreedy knapsackGreedy = new KnapsackGreedy();
    private HashMap<String, ArrayList<String>> hashArGBResult = new HashMap<String, ArrayList<String>>();
    private ArrayList<String> ar_gbp_result = new ArrayList<String>();
    private ArrayList<String> ar_gbw_result = new ArrayList<String>();
    private ArrayList<String> ar_gbd_result = new ArrayList<String>();
    private OrderedRepo orderedRepo;
    private OrderedTable orderedTable;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private EnumProHelper enumProHelper = new EnumProHelper();

    private String data_orderID = "";
    private int data_weight = 0, data_profit = 0, total_weight1 = 0, total_weight2 = 0, total_weight3 = 0,
            total_profit1 = 0, total_profit2 = 0, total_profit3 = 0, itemsProfit = 0, itemsWeight = 0, itemsDensity = 0;
    private boolean data_profitMax = false, data_weightMin = false, data_densityMax = false;
    private GreedyresultEntity greedyresultEntity;
    private ShippingService shippingService = new ShippingService();
    private long maxVolumeTruckTotal = 0;

    private String typeResultx;
    private int totalItemx;
    private long maxcapacityx, totalWeigthx, totalProfitx;
    private String finalShippingIDparent = "";
    private ArrayList<String> ar_shipping_density_orderID = new ArrayList<String>();
    private ArrayList<String> ar_shipping_weight_orderID = new ArrayList<String>();
    private ArrayList<String> ar_shipping_profit_orderID = new ArrayList<String>();
    private OrderedService orderedService = new OrderedService();

    public boolean loadDataDelivery(Context context, List<GreedyresultEntity> greedyresultList, GreedyresultAdapter greedyresultAdapter,
                                    TextView txtTotalWeight1, TextView txtTotalWeight2, TextView txtTotalWeight3, TextView txtTotalProfit1,
                                    TextView txtTotalProfit2, TextView txtTotalProfit3, String custID, TextView txtTotalItem1,
                                    TextView txtTotalItem2, TextView txtTotalItem3){

        /**clean data formula*/
        knapsackGreedy.cleanDataBarang();
        /**get data dari SQLite*/
        orderedRepo = new OrderedRepo(context);
        orderedTable = new OrderedTable();
        theList = orderedRepo.getAllDataByStatus(enumProHelper.statusTersedia, custID);
        if (theList.size()>0) {
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                /**save data ke formula*/
                knapsackGreedy.saveDataBarang(
                        a, gen.get(orderedTable.key_orderID),
                        gen.get(orderedTable.key_customerID), Integer.valueOf(gen.get(orderedTable.key_productWeight)),
                        Integer.valueOf(gen.get(orderedTable.key_productProfit))
                );
            }
            hashArGBResult.clear();
            hashArGBResult = knapsackGreedy.getResultAllGreedy(context);
            if (hashArGBResult.size()>0){
                ar_gbp_result = hashArGBResult.get(knapsackGreedy.key_ar_gbp_result);
                ar_gbw_result = hashArGBResult.get(knapsackGreedy.key_ar_gbw_result);
                ar_gbd_result = hashArGBResult.get(knapsackGreedy.key_ar_gbd_result);
                loadDataFromSQLite(context, greedyresultList, greedyresultAdapter, txtTotalWeight1, txtTotalWeight2, txtTotalWeight3,
                        txtTotalProfit1, txtTotalProfit2, txtTotalProfit3, custID, txtTotalItem1, txtTotalItem2, txtTotalItem3);
            }else {
                Toasty.error(context, "Tidak ada data.", Toast.LENGTH_SHORT, true).show();
            }
            return true;
        }
        return false;
    }


    //fix untuk optimasi knapsack hanya untuk 1 customers maka save di sini
    private String finalCustID = "";
    private void loadDataFromSQLite(Context context, List<GreedyresultEntity> greedyresultList, GreedyresultAdapter greedyresultAdapter,
                                    TextView txtTotalWeight1, TextView txtTotalWeight2, TextView txtTotalWeight3, TextView txtTotalProfit1,
                                    TextView txtTotalProfit2, TextView txtTotalProfit3, String custID, TextView txtTotalItem1,
                                    TextView txtTotalItem2, TextView txtTotalItem3) {
        finalCustID = custID;
        /**get data dari SQLite*/
        orderedRepo = new OrderedRepo(context);
        orderedTable = new OrderedTable();
        theList = orderedRepo.getAllDataByStatus(enumProHelper.statusTersedia, custID);
        if (theList.size()>0) {
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                /**fixing get data*/
                data_orderID = gen.get(orderedTable.key_orderID);
                data_weight = Integer.valueOf(gen.get(orderedTable.key_productWeight));
                data_profit = Integer.valueOf(gen.get(orderedTable.key_productProfit));
                data_profitMax = searchPmaxWminDMax(data_orderID, ar_gbp_result);
                data_weightMin = searchPmaxWminDMax(data_orderID, ar_gbw_result);
                data_densityMax = searchPmaxWminDMax(data_orderID, ar_gbd_result);
                /**total validation*/
                if (data_profitMax){
                    total_weight1 += data_weight;
                    total_profit1 += data_profit;
                    itemsProfit ++;
                    ar_shipping_profit_orderID.add(data_orderID);
                }
                if (data_weightMin){
                    total_weight2 += data_weight;
                    total_profit2 += data_profit;
                    itemsWeight ++;
                    ar_shipping_weight_orderID.add(data_orderID);
                }
                if (data_densityMax){
                    total_weight3 += data_weight;
                    total_profit3 += data_profit;
                    itemsDensity ++;
                    ar_shipping_density_orderID.add(data_orderID);
                }
                /**tampilkan ke GUI*/
                greedyresultEntity = new GreedyresultEntity();
                greedyresultEntity.setGrorderID(data_orderID);
                greedyresultEntity.setGrWeight(data_weight);
                greedyresultEntity.setGrProfit(data_profit);
                greedyresultEntity.setGrTakeProfit(data_profitMax);
                greedyresultEntity.setGrTakeWeight(data_weightMin);
                greedyresultEntity.setGrTakeDensity(data_densityMax);
                /**set list entity to adapter*/
                greedyresultList.add(greedyresultEntity);
                greedyresultAdapter.notifyDataSetChanged();
            }
            /**footer first row*/
            txtTotalWeight1.setText(MyConverting.format_angka(String.valueOf(total_weight1)));
            txtTotalWeight2.setText(MyConverting.format_angka(String.valueOf(total_weight2)));
            txtTotalWeight3.setText(MyConverting.format_angka(String.valueOf(total_weight3)));
            /**footer second row*/
            txtTotalProfit1.setText(MyConverting.format_angka(String.valueOf(total_profit1)));
            txtTotalProfit2.setText(MyConverting.format_angka(String.valueOf(total_profit2)));
            txtTotalProfit3.setText(MyConverting.format_angka(String.valueOf(total_profit3)));
            /**footer thrid row*/
            txtTotalItem1.setText(String.valueOf(itemsProfit));
            txtTotalItem2.setText(String.valueOf(itemsWeight));
            txtTotalItem3.setText(String.valueOf(itemsDensity));
        }
    }

    private boolean searchPmaxWminDMax(String data_orderID, ArrayList<String> ar_result) {
        for (int i = 0; i < ar_result.size(); i++) {
            if (data_orderID.equals(ar_result.get(i)))
                return true;
        }
        return false;
    }


    private ShippingchildRepo shippingchildRepo;
    public void saveShipping(Context context, String greedyTypeResult){
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity) != null) {
            maxVolumeTruckTotal = Integer.valueOf(MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity));
            /**final value*/
            if (greedyTypeResult.equals(enumProHelper.greedyByDensity)) {
                typeResultx = enumProHelper.maxDensity;
                maxcapacityx = maxVolumeTruckTotal;
                totalItemx = itemsDensity;
                totalWeigthx = total_weight3;
                totalProfitx = total_profit3;
            }else if (greedyTypeResult.equals(enumProHelper.greedyByWeight)) {
                typeResultx = enumProHelper.minWeight;
                maxcapacityx = maxVolumeTruckTotal;
                totalItemx = itemsWeight;
                totalWeigthx = total_weight2;
                totalProfitx = total_profit2;
            }else if (greedyTypeResult.equals(enumProHelper.greedyByProfit)) {
                typeResultx = enumProHelper.maxProfit;
                maxcapacityx = maxVolumeTruckTotal;
                totalItemx = itemsProfit;
                totalWeigthx = total_weight1;
                totalProfitx = total_profit1;
            }

            //kita cek dulu di table shipping udah ada CUSTID yg ini belum
            //kalau ada maka jadi LAST ITEM
            //kalau belum ada maka sesuaikan dengan status maxP minW maxD yg dipilih
            shippingchildRepo = new ShippingchildRepo(context);
            if (shippingchildRepo.isExist(finalCustID)){
                typeResultx = enumProHelper.keyLastItem;
            }

            /**Log.d("xsurvive1", typeResultx);
            Log.d("xsurvive2", String.valueOf(maxcapacityx));
            Log.d("xsurvive3", String.valueOf(totalItemx));
            Log.d("xsurvive4", String.valueOf(totalWeigthx));
            Log.d("xsurvive5", String.valueOf(totalProfitx));*/

            if (totalItemx > 0) {
                saveShippingOptimization(context, greedyTypeResult, typeResultx, maxcapacityx, totalItemx, totalWeigthx, totalProfitx);
                Toasty.success(context, "Berhasil disimpan pada tabel Shipping.", Toast.LENGTH_SHORT, true).show();
            }else {
                Toasty.error(context, "Tidak ada item dari greedy result.", Toast.LENGTH_SHORT, true).show();
            }
        }else {
            Toasty.error(context, "Mohon isi parameter maksimal kapasitas transportasi pengiriman.", Toast.LENGTH_SHORT, true).show();
        }
    }

    private void saveShippingOptimization(Context context, String greedyTypeResult, String typeResultx,
                                               long maxcapacityx, int totalItemx, long totalWeigthx, long totalProfitx) {
        finalShippingIDparent = shippingService.saveShipping(context, typeResultx, maxcapacityx,
                totalItemx, totalWeigthx, totalProfitx, enumProHelper.keyStatusOk);
        saveShippingChild(context, finalShippingIDparent, greedyTypeResult);
    }

    private void saveShippingChild(Context context, String finalShippingIDparent, String greedyTypeResult) {
        if (greedyTypeResult.equals(enumProHelper.greedyByDensity)) {
            checkDataAndSave(context, ar_shipping_density_orderID, finalShippingIDparent);
        }else if (greedyTypeResult.equals(enumProHelper.greedyByWeight)) {
            checkDataAndSave(context, ar_shipping_weight_orderID, finalShippingIDparent);
        }else if (greedyTypeResult.equals(enumProHelper.greedyByProfit)) {
            checkDataAndSave(context, ar_shipping_profit_orderID, finalShippingIDparent);
        }
    }

    private void checkDataAndSave(Context context, ArrayList<String> arrayList, String finalShippingIDparent) {
        if (arrayList.size() > 0){
            for (int i = 0; i < arrayList.size(); i++) {
                shippingService.saveShippingChild(context, finalShippingIDparent, arrayList.get(i));
                orderedService.updateStatusOrdered(context, arrayList.get(i), enumProHelper.statusTerpilih);
            }
        }
    }


}
