package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.activity.ShippingroutesActivity;
import com.firmanpro.pwoptimization.adapter.SeqroutesAdapter;
import com.firmanpro.pwoptimization.adapter.ShippingAdapter;
import com.firmanpro.pwoptimization.entity.SeqroutesEntity;
import com.firmanpro.pwoptimization.entity.ShippingEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.sqlite.repo.SeqRoutesRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingchildRepo;
import com.firmanpro.pwoptimization.sqlite.table.SeqRoutesTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingChildTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 11/2/17.
 */

public class ShippingService {

    private final String TAG = "shipp_ser";
    private ShippingRepo shippingRepo;
    private ShippingTable shippingTable;
    private ShippingchildRepo shippingchildRepo;
    private ShippingChildTable shippingChildTable;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private ShippingEntity shippingEntity;
    private EnumProHelper enumProHelper = new EnumProHelper();

    public void deleteAllShipping(Context context){
        shippingRepo = new ShippingRepo(context);
        shippingTable = new ShippingTable();
        shippingRepo.deleteAllData();
    }

    public void deleteAllShippingChild(Context context){
        shippingchildRepo = new ShippingchildRepo(context);
        shippingChildTable = new ShippingChildTable();
        shippingchildRepo.deleteAllData();
    }

    public String saveShipping(Context context, String typeResult, long maxcapacity,
                             int totalItem, long totalWeigth, long totalProfit, String status){
        shippingRepo = new ShippingRepo(context);
        shippingTable = new ShippingTable();
        shippingTable.typeResult = typeResult;
        shippingTable.maxcapacity = maxcapacity;
        shippingTable.totalItem = totalItem;
        shippingTable.totalWeigth = totalWeigth;
        shippingTable.totalProfit = totalProfit;
        shippingTable.status = status;
        return shippingRepo.insert(shippingTable);
    }

    public void saveShippingChild(Context context, String shippingID, String orderID){
        shippingchildRepo = new ShippingchildRepo(context);
        shippingChildTable = new ShippingChildTable();
        shippingChildTable.shippingID = shippingID;
        shippingChildTable.orderID = orderID;
        shippingchildRepo.insert(shippingChildTable);
    }

    public boolean getShipping(Context context, List<ShippingEntity> shippingList, ShippingAdapter shippingAdapter){
        shippingRepo = new ShippingRepo(context);
        shippingTable = new ShippingTable();
        theList = shippingRepo.getAllData(true,"");
        if (theList.size()>0) {
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);

                shippingEntity = new ShippingEntity();
                shippingEntity.setShippingID(gen.get(shippingTable.key_shippingID));
                shippingEntity.setMaxcapacity(Integer.valueOf(gen.get(shippingTable.key_maxcapacity)));
                shippingEntity.setTypeResult(gen.get(shippingTable.key_typeResult));
                shippingEntity.setTotalItem(Integer.valueOf(gen.get(shippingTable.key_totalItem)));
                shippingEntity.setTotalWeigth(Integer.valueOf(gen.get(shippingTable.key_totalWeigth)));
                shippingEntity.setTotalProfit(Integer.valueOf(gen.get(shippingTable.key_totalProfit)));
                shippingEntity.setTotalDistance(Integer.valueOf(gen.get(shippingTable.key_totalDistance)));
                shippingEntity.setTotalDuration(Integer.valueOf(gen.get(shippingTable.key_totalDuration)));
                shippingEntity.setStatus(gen.get(shippingTable.key_status));

                shippingList.add(shippingEntity);
                shippingAdapter.notifyDataSetChanged();

            }
            return true;
        }
        return false;
    }

    private SeqRoutesRepo seqRoutesRepo;
    private SeqroutesEntity seqroutesEntity;
    public void getSeqRoutes(Context context, List<SeqroutesEntity> seqroutesList, SeqroutesAdapter seqroutesAdapter) {
        seqRoutesRepo = new SeqRoutesRepo(context);
        theList = seqRoutesRepo.getAllData();
        if (theList.size() > 0){
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);

                Log.d(TAG, gen.get(SeqRoutesTable.key_idSeq)
                        + " - " + gen.get(SeqRoutesTable.key_customersID)
                        + " - " + gen.get(SeqRoutesTable.key_totalWeight)
                        + " - " + gen.get(SeqRoutesTable.key_isGoodWeight)
                        + " - " + gen.get(SeqRoutesTable.key_statusDss)
                );

                seqroutesEntity = new SeqroutesEntity();
                seqroutesEntity.setIdSeq(Integer.parseInt(gen.get(SeqRoutesTable.key_idSeq)));
                seqroutesEntity.setCustomersID(gen.get(SeqRoutesTable.key_customersID));
                seqroutesEntity.setTotalWeight(Long.parseLong(gen.get(SeqRoutesTable.key_totalWeight)));
                seqroutesEntity.setIsGoodWeight(Integer.parseInt(gen.get(SeqRoutesTable.key_isGoodWeight)));
                seqroutesEntity.setStatusDss(gen.get(SeqRoutesTable.key_statusDss));

                seqroutesList.add(seqroutesEntity);
                seqroutesAdapter.notifyDataSetChanged();

            }
        }
    }

    private ArrayList<String> ar_com_id = new ArrayList<String>();
    private ArrayList<Long> ar_com_weight = new ArrayList<Long>();
    private ArrayList<Long> ar_com_profit = new ArrayList<Long>();
    private ArrayList<Long> ar_com_distance = new ArrayList<Long>();
    private ArrayList<String> ar_com_id_final = new ArrayList<String>();
    private ArrayList<Long> ar_com_weight_final = new ArrayList<Long>();
    private ArrayList<Long> ar_com_profit_final = new ArrayList<Long>();
    private ArrayList<Long> ar_com_distance_final = new ArrayList<Long>();
    private long currentWeight = 0, customeWeight = 0, maxCapacity = 0;
    private boolean getCombination = false;
    public void getCombination(Context context, TextView txtBeforeShipping, TextView txtAfterShipping,
                               TextView txtBeforeDistance, TextView txtAfterDistance, TextView txtCombinationCustID,
                               TextView txtCombinationWeight, TextView txtCombinationProfit, TextView txtFindDistance) {
        ar_com_id.clear();
        ar_com_weight.clear();
        ar_com_profit.clear();
        ar_com_id_final.clear();
        ar_com_weight_final.clear();
        ar_com_profit_final.clear();

        shippingRepo = new ShippingRepo(context);
        theList = shippingRepo.getAllData(false, enumProHelper.keyStatusOk);
        Log.d("superman", String.valueOf(theList.size()));
        if (theList.size() > 0){
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                ar_com_id.add(gen.get(shippingTable.key_shippingID));
                ar_com_weight.add(Long.valueOf(gen.get(shippingTable.key_totalWeigth)));
                ar_com_profit.add(Long.valueOf(gen.get(shippingTable.key_totalProfit)));
                ar_com_distance.add(Long.valueOf(gen.get(shippingTable.key_totalDistance)));
            }
            maxCapacity = getMaxCapacity(context);
            checkCombination(context, maxCapacity, ar_com_id, ar_com_weight, ar_com_profit, ar_com_distance, txtBeforeShipping, txtAfterShipping,
                    txtBeforeDistance, txtAfterDistance, txtCombinationCustID, txtCombinationWeight, txtCombinationProfit, txtFindDistance);
        }
    }

    private void checkCombination(Context context, long maxCapacity, ArrayList<String> ar_com_id, ArrayList<Long> ar_com_weight, ArrayList<Long> ar_com_profit,
                                  ArrayList<Long> ar_com_distance, TextView txtBeforeShipping, TextView txtAfterShipping, TextView txtBeforeDistance,
                                  TextView txtAfterDistance, TextView txtCombinationCustID, TextView txtCombinationWeight, TextView txtCombinationProfit,
                                  TextView txtFindDistance) {
        for (int a = 0; a < ar_com_id.size(); a++) {
            currentWeight = ar_com_weight.get(a);
            for (int b = a+1; b < ar_com_id.size(); b++) {
                customeWeight = ar_com_weight.get(b);
                currentWeight += customeWeight;
                if (currentWeight <= maxCapacity){
                    if (!getCombination) {
                        Log.d("idshipoke", ar_com_id.get(a));
                        ar_com_id_final.add(ar_com_id.get(a));
                        ar_com_weight_final.add(ar_com_weight.get(a));
                        ar_com_profit_final.add(ar_com_profit.get(a));
                        ar_com_distance_final.add(ar_com_distance.get(a));
                    }
                    Log.d("idshipoke", ar_com_id.get(b));
                    ar_com_id_final.add(ar_com_id.get(b));
                    ar_com_weight_final.add(ar_com_weight.get(b));
                    ar_com_profit_final.add(ar_com_profit.get(b));
                    ar_com_distance_final.add(ar_com_distance.get(b));
                    getCombination = true;
                }else {
                    currentWeight -= customeWeight;
                }
            }
            if (getCombination){
                break;
            }
        }
        if (ar_com_id_final.size() > 0){
            showCombination(context, ar_com_id_final, ar_com_weight_final, ar_com_profit_final, ar_com_distance_final, txtBeforeShipping, txtAfterShipping,
                    txtBeforeDistance, txtAfterDistance, txtCombinationCustID, txtCombinationWeight, txtCombinationProfit, txtFindDistance);
        }
    }

    private long totalWeightCombination = 0, totalProfitCombination = 0, totalDistanceCombination = 0;
    private Intent intent;
    private String strShippingID = "";
    private void showCombination(final Context context, ArrayList<String> ar_com_id_final, ArrayList<Long> ar_com_weight_final, ArrayList<Long> ar_com_profit_final,
                                 ArrayList<Long> ar_com_distance_final, TextView txtBeforeShipping, TextView txtAfterShipping,
                                 TextView txtBeforeDistance, TextView txtAfterDistance, TextView txtCombinationCustID,
                                 TextView txtCombinationWeight, TextView txtCombinationProfit, TextView txtFindDistance) {

        txtCombinationCustID.setText(String.valueOf(ar_com_id_final));
        txtBeforeShipping.setText(String.valueOf(ar_com_id_final.size()) + " times");
        txtAfterShipping.setText("1 times");

        strShippingID = "";

        for (int a = 0; a < ar_com_id_final.size(); a++) {
            strShippingID += ar_com_id_final.get(a) + "-";
            totalWeightCombination += ar_com_weight_final.get(a);
            totalProfitCombination += ar_com_profit_final.get(a);
            totalDistanceCombination += ar_com_distance_final.get(a);
        }

        txtCombinationWeight.setText(MyConverting.format_angka(String.valueOf(totalWeightCombination)));
        txtCombinationProfit.setText(MyConverting.format_angka(String.valueOf(totalProfitCombination)));
        txtBeforeDistance.setText(MyConverting.meterToKM((int)totalDistanceCombination));

        txtFindDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(context, ShippingroutesActivity.class);
                intent.putExtra(enumProHelper.keyShippingIDFindDistance, strShippingID);
                intent.putExtra(enumProHelper.keyModeFindDistance, enumProHelper.yes);
                context.startActivity(intent);
            }
        });

    }

    private long getMaxCapacity(Context context) {
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity) != null){
            return Long.valueOf(MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity));
        }
        return 0;
    }

    public void updateAllStatus(Context context, String status){
        shippingRepo = new ShippingRepo(context);
        shippingRepo.updateAllStatus(status);
    }

    public void updateStatus(Context context, String shippingID, String status){
        shippingRepo = new ShippingRepo(context);
        shippingRepo.updateStatus(shippingID, status);
    }

    public void combineShipping(Context context, String combinationCustID, AlertDialog alertDialog) {
        //update status shipping id combinasi jadi CANCEL
        //Save Shipping baru
        //Save Shipping child baru dengan looping shipping ID nya
        Toasty.info(context, "Sedang diproses "+combinationCustID, Toast.LENGTH_SHORT).show();
        combinationCustID = combinationCustID.replace("[","").replace("]","").trim();
        String[] split = combinationCustID.toString().trim().split("\\,");
        if (split.length > 0) {
            //Save Shipping
            String finalShippingID = saveShipping(context, enumProHelper.keyTypeCombination,
                    getMaxCapacity(context), 0, 0, 0, enumProHelper.keyStatusOk);
            //Update status sambil save shipping child
            for (int a = 0; a < split.length; a++) {
                String shipID = split[a].trim();
                if (!shipID.trim().equals("")) {
                    Log.d("superman", shipID);
                    updateStatus(context, shipID, enumProHelper.keyStatusCancel);
                    saveShipChildFromOldShipping(context, shipID, finalShippingID);
                }
                if (a+1 == split.length){
                    updateTotal(context, finalShippingID, alertDialog);
                }
            }
        }

    }

    private void updateTotal(Context context, String finalShippingID, AlertDialog alertDialog) {
        SeqRoutesService seqRoutesService = new SeqRoutesService();
        seqRoutesService.updateSumShippingParent(context, finalShippingID);
        alertDialog.dismiss();
    }

    private void saveShipChildFromOldShipping(Context context, String shipID, String finalSHippingID) {
        shippingchildRepo = new ShippingchildRepo(context);
        theList = shippingchildRepo.getAllData(shipID);
        if (theList.size() > 0) {
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                saveShippingChild(context, finalSHippingID, gen.get(ShippingChildTable.key_orderID));
            }
        }
    }

}
