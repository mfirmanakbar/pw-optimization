package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MySession;

/**
 * Created by firmanmac on 11/1/17.
 */

public class AdminService {

    private EnumProHelper enumProHelper = new EnumProHelper();
    private RoutesService routesService = new RoutesService();
    private ShippingService shippingService = new ShippingService();
    private CustomerService customerService = new CustomerService();
    private OrderedService orderedService = new OrderedService();
    private LegsService legsService = new LegsService();
    private SeqRoutesService seqRoutesService = new SeqRoutesService();

    public void resetToImportDummy(Context context, RelativeLayout reLay_loading, int typeDatabase){
        Toast.makeText(context, "Comming soon", Toast.LENGTH_SHORT).show();
        deleteLocalDatabase(context, reLay_loading);
        customerService.createCustomersServer(context, reLay_loading, false, typeDatabase);
        orderedService.createOrderedServer(context, reLay_loading, false, typeDatabase);
    }

    public void resetToImportServer(final Context context, final RelativeLayout reLay_loading){
        deleteLocalDatabase(context, reLay_loading);
        Log.d("_ser", "masuk import");
        customerService.createCustomersServer(context, reLay_loading, true, 0);
        orderedService.createOrderedServer(context, reLay_loading, true, 0);
    }

    private void deleteLocalDatabase(final Context context, final RelativeLayout reLay_loading) {
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                reLay_loading.setVisibility(View.VISIBLE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                customerService.deleteAllCustomer(context);
                orderedService.deleteAllOrdered(context);
                shippingService.deleteAllShipping(context);
                shippingService.deleteAllShippingChild(context);
                routesService.deleteAllRoutes(context);
                legsService.deleteAllLegs(context);
                seqRoutesService.deleteAll(context);
                resetStatusOrdered(context);
                MySession.beginInitialization(context);
                if (MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest) != null) {
                    MySession.clearSessionGlobal(MySession.key_session_shortest_routes_rest);
                }
                return null;
            }
        }.execute();
    }

    public void deleteDatabase(final Context context, final RelativeLayout reLay_loading,
                               final boolean cbShipping, final boolean cbRoutes, final boolean cbLegs) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                reLay_loading.setVisibility(View.VISIBLE);
            }
            @Override
            protected Void doInBackground(Void... voids) {
                if (cbShipping){
                    shippingService.deleteAllShipping(context);
                    shippingService.deleteAllShippingChild(context);
                }
                if (cbRoutes){
                    routesService.deleteAllRoutes(context);
                }
                if (cbLegs){
                    legsService.deleteAllLegs(context);
                }
                MySession.beginInitialization(context);
                if (MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest) != null) {
                    MySession.clearSessionGlobal(MySession.key_session_shortest_routes_rest);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                reLay_loading.setVisibility(View.GONE);
            }
        }.execute();
    }


    public void resetStatusOrdered(Context context) {
        orderedService.updateStatusData(context, enumProHelper.statusTersedia);
    }

    public void resetStatusSeqRoutes(Context context) {
        seqRoutesService.updateStatusAllSame(context, "");
    }

    public void resetStatusShipping(Context context) {
        shippingService.updateAllStatus(context, enumProHelper.keyStatusOk);
    }

}
