package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.sqlite.repo.CustomerRepo;
import com.firmanpro.pwoptimization.sqlite.table.CustomerTable;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 11/24/17.
 */

public class SinglerouteService {

    private final String TAG = "sr_ser";
    private CustomerRepo customerRepo;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private LatLng originFinal, destFinal;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private String logStr = "";
    int total_distance = 0; //meters
    int total_duration = 0; //second

    public void clickFindRoutes(Context context, GoogleMap mMap, String cust_origin, String cust_dest,
                                TextView txtPopupSRDistance, TextView txtPopupSRDuration, TextView txtSrLog,
                                TextView txtSrTdis, TextView txtSrTdur, ScrollView scSrLog) {

        Toasty.info(context, cust_origin + " - " + cust_dest, Toast.LENGTH_SHORT).show();

        if (!logStr.equals("")){
            logStr = logStr + "\n\n";
        }
        logStr += cust_origin + " - " + cust_dest;
        txtSrLog.setText(logStr);
        scrollDown(scSrLog);

        if (cust_origin.equals(enumProHelper.keyGudang)){
            originFinal = getGudangLatLng(context);
        }else {
            originFinal = getCustomerLatLng(context, cust_origin);
        }
        destFinal = getCustomerLatLng(context, cust_dest);
        if (originFinal != null && destFinal != null){
            String urlnya = getDirectionsUrl(originFinal, destFinal);
            getDirectionsByUrl(context, mMap, urlnya, txtPopupSRDistance, txtPopupSRDuration, txtSrLog, txtSrTdis, txtSrTdur, scSrLog);
        }else {
            Toasty.error(context, "Customer tidak ditemukan", Toast.LENGTH_SHORT).show();
        }
    }

    private LatLng getCustomerLatLng(Context context, String custID) {
        customerRepo = new CustomerRepo(context);
        theList = customerRepo.getByCustID(custID);
        if (theList.size()>0) {
            gen = theList.get(0);
            return new LatLng(Double.valueOf(gen.get(CustomerTable.key_customerLat)), Double.valueOf(gen.get(CustomerTable.key_customerLng)));
        }
        return null;
    }

    private LatLng getGudangLatLng(Context context) {
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_warehouse_lat) != null
                && MySession.getSessionGlobal(MySession.key_session_warehouse_lng) != null
                && !MySession.getSessionGlobal(MySession.key_session_warehouse_lat).equals("")
                && !MySession.getSessionGlobal(MySession.key_session_warehouse_lng).equals("")){
            return new LatLng(
                    Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lat)),
                    Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lng)));
        }
        return null;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest){
        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin+"&"+str_dest+"&"+sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                +output+"?"+parameters;
        return url;
    }

    private void getDirectionsByUrl(final Context context, final GoogleMap mMap,
                                    final String urlnya, final TextView txtPopupSRDistance,
                                    final TextView txtPopupSRDuration, final TextView txtSrLog,
                                    final TextView txtSrTdis, final TextView txtSrTdur,
                                    final ScrollView scSrLog) {
        //isProgressBarShow(true);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                com.android.volley.Request.Method.GET, urlnya, "",
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jObject = new JSONObject(response.toString());
                            String status = jObject.getString("status");
                            if(status.equals("OK")) {

                                /**mulai*/
                                JSONArray jRoutes = null;
                                JSONArray jLegs = null;
                                int distance = 0; //meters
                                int duration = 0; //second

                                jRoutes = response.getJSONArray("routes");

                                /** Traversing all routes */
                                /**jRoutes.length() //kita pake 1 rute aja asumsi
                                 * sementara dr uji coba bahwa
                                 * default free API 1 route*/
                                for(int i=0;i<1;i++){
                                    jLegs = ( (JSONObject)jRoutes.get(i))
                                            .getJSONArray("legs");

                                    distance =
                                            (Integer) ((JSONObject)((JSONObject)jLegs
                                            .get(i)).get("distance")).get("value");
                                    duration =
                                            (Integer) ((JSONObject)((JSONObject)jLegs
                                            .get(i)).get("duration")).get("value");

                                    Log.d(TAG, String.valueOf(distance));
                                    Log.d(TAG, String.valueOf(duration));

                                    total_distance += distance;
                                    total_duration += duration;

                                    txtPopupSRDistance.setText(MyConverting.meterToKM(distance));
                                    txtPopupSRDuration.setText(MyConverting.beautyTime(duration));

                                    logStr += "\nJarak: " + MyConverting.meterToKM(distance) + "\nDurasi: " + MyConverting.beautyTime(duration);
                                    txtSrLog.setText(logStr);
                                    txtSrTdis.setText(MyConverting.meterToKM(total_distance));
                                    txtSrTdur.setText(MyConverting.beautyTime(total_duration));
                                    scrollDown(scSrLog);

                                    drawSteps(context, mMap, jLegs);

                                }
                                /**selesai*/

                            }else {
                                //isProgressBarShow(false);
                                Toast.makeText(context,"Sorry, data not found !",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {}
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage(), TAG);
                Log.d(TAG, "FAIL!");
                try {
                    //isProgressBarShow(false);
                    Toast.makeText(context,"Request error !",
                            Toast.LENGTH_SHORT).show();
                }catch (Exception a){}
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }

    private void drawSteps(Context context, GoogleMap mMap, JSONArray jLegs) {
        try {
            List<List<HashMap<String, String>>> routes =
                    new ArrayList<List<HashMap<String,String>>>() ;
            JSONArray jSteps = null;
            List path = new ArrayList<HashMap<String, String>>();
            for(int j=0;j<jLegs.length();j++){
                jSteps = ( (JSONObject)jLegs.get(j))
                        .getJSONArray("steps");
                /** Traversing all steps */
                for(int k=0;k<jSteps.length();k++){
                    String polyline = "";
                    polyline = (String)((JSONObject)((JSONObject)jSteps
                            .get(k)).get("polyline")).get("points");
                    List<LatLng> list = decodePoly(polyline);
                    /** Traversing all points */
                    for(int l=0;l<list.size();l++){
                        HashMap<String, String> hm =
                                new HashMap<String, String>();
                        hm.put("lat", Double
                                .toString(((LatLng)list.get(l))
                                        .latitude) );
                        hm.put("lng", Double
                                .toString(((LatLng)list.get(l))
                                        .longitude) );
                        path.add(hm);
                    }
                }
                routes.add(path);
            }
            nowShowDraw(context, mMap, routes);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1)
                    != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1)
                    != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    private ArrayList<LatLng> points = null;
    private PolylineOptions lineOptions = null;
    private void nowShowDraw(Context context, GoogleMap mMap, List<List<HashMap<String, String>>> result) {
        for(int i=0;i<result.size();i++){
            points = new ArrayList<LatLng>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for(int j=0;j<path.size();j++){
                HashMap<String,String> point = path.get(j);
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);
            }
            lineOptions.addAll(points);
            lineOptions.color(Color.BLUE);
        }
        // Drawing polyline in the Google Map for the i-th route
        mMap.addPolyline(lineOptions).setWidth(5);
    }

    private void scrollDown(final ScrollView scrollView){
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

}
