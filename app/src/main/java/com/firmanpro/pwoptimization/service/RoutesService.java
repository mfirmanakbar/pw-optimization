package com.firmanpro.pwoptimization.service;

import android.content.Context;

import com.firmanpro.pwoptimization.sqlite.repo.LogRoutesRepo;
import com.firmanpro.pwoptimization.sqlite.table.LogRoutesTable;

/**
 * Created by firmanmac on 11/10/17.
 */

public class RoutesService {

    private LogRoutesRepo routesRepo;
    private LogRoutesTable routesTable;

    public void deleteAllRoutes(Context context) {
        routesRepo = new LogRoutesRepo(context);
        routesTable = new LogRoutesTable();
        routesRepo.deleteAllData();
    }

}
