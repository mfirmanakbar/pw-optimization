package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.sqlite.repo.OrderedRepo;
import com.firmanpro.pwoptimization.sqlite.repo.SeqRoutesRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingchildRepo;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;
import com.firmanpro.pwoptimization.sqlite.table.SeqRoutesTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 12/7/17.
 */

public class SeqRoutesService {

    private final String TAG = "seqr_ser";
    private SeqRoutesTable seqRoutesTable;
    private SeqRoutesRepo seqRoutesRepo;
    private OrderedRepo orderedRepo;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;

    public void deleteAll(Context context) {
        seqRoutesRepo = new SeqRoutesRepo(context);
        seqRoutesRepo.deleteAllData();
    }

    public void insertSeq(Context context, String custID) {

        Log.d(TAG, custID);

        seqRoutesRepo = new SeqRoutesRepo(context);
        seqRoutesTable = new SeqRoutesTable();
        orderedRepo = new OrderedRepo(context);

        //prepare insert
        int maxCapacity = getMaxCapacity(context);
        long totalWeight = orderedRepo.getSumWeight(custID.trim());
        int isGoodWeights = isGoodWeight(totalWeight, maxCapacity);

        //insert one by one
        seqRoutesTable.customersID = custID.trim();
        seqRoutesTable.totalWeight = totalWeight;
        seqRoutesTable.isGoodWeight = isGoodWeights;
        seqRoutesTable.statusDss = "";
        seqRoutesRepo.insert(seqRoutesTable);

    }

    private int getMaxCapacity(Context context) {
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity) != null) {
            return Integer.parseInt(MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity));
        }
        return 0;
    }

    private int isGoodWeight(long totalWeight, long maxCapacity){
        if (totalWeight <= maxCapacity){
            return 1;
        }
        return 0;
    }

    private int goodWeight = 0, badWeight = 0;
    private boolean doAutoGoodWeghts = false;
    private ArrayList<String> ar_good_cust = new ArrayList<String>();
    private ArrayList<String> ar_bad_cust = new ArrayList<String>();
    private int totalGood = 0;
    private String custIDStrGood = "";
    private int maxCapacity = 0;

    public void getAllData(Context context, TextView txtGoodWeights, TextView txtBadWeights){

        ar_good_cust.clear();
        ar_bad_cust.clear();

        maxCapacity = getMaxCapacity(context);

        seqRoutesRepo = new SeqRoutesRepo(context);
        seqRoutesTable = new SeqRoutesTable();
        orderedRepo = new OrderedRepo(context);

        theList = seqRoutesRepo.getAllData();
        if (theList.size() > 0){
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);

                Log.d(TAG, gen.get(SeqRoutesTable.key_idSeq)
                        + " - " + gen.get(SeqRoutesTable.key_customersID)
                        + " - " + gen.get(SeqRoutesTable.key_totalWeight)
                        + " - " + gen.get(SeqRoutesTable.key_isGoodWeight)
                        + " - " + gen.get(SeqRoutesTable.key_statusDss)
                );

                if (gen.get(SeqRoutesTable.key_isGoodWeight).equals("1")){
                    goodWeight += 1;
                    //kalau status kosong artinya belum diproses
                    if (gen.get(SeqRoutesTable.key_statusDss).equals("")) {
                        doAutoGoodWeghts = true;
                        Log.d("okegood0", gen.get(SeqRoutesTable.key_customersID));
                        Log.d("okegood1", String.valueOf(totalGood));
                        totalGood += Integer.parseInt(gen.get(SeqRoutesTable.key_totalWeight));
                        Log.d("okegood2", String.valueOf(totalGood));
                        if (totalGood <= maxCapacity) {
                            Log.d("okegood3", String.valueOf(totalGood));
                            custIDStrGood += gen.get(SeqRoutesTable.key_customersID).trim() + "-";
                            if (a == theList.size() - 1) {
                                ar_good_cust.add(custIDStrGood);
                                Log.d("okegood4", custIDStrGood);
                                custIDStrGood = "";
                                totalGood = 0;
                            }
                        } else {
                            Log.d("okegood5", custIDStrGood);
                            ar_good_cust.add(custIDStrGood);
                            custIDStrGood = "";
                            totalGood = 0;
                            custIDStrGood += gen.get(SeqRoutesTable.key_customersID).trim() + "-";
                            Log.d("okegood6", custIDStrGood);
                        }
                    }
                }else if (gen.get(SeqRoutesTable.key_isGoodWeight).equals("0")){
                    badWeight += 1;
                    //kalau status kosong artinya belum diproses
                    if (gen.get(SeqRoutesTable.key_statusDss).equals("")) {
                        ar_bad_cust.add(gen.get(SeqRoutesTable.key_customersID));
                    }
                }
            }

            txtGoodWeights.setText(String.valueOf(goodWeight));
            txtBadWeights.setText(String.valueOf(badWeight));

            if (doAutoGoodWeghts) {
                //ada sisa di custIDStrGood masukin ke array good weight
                if (!custIDStrGood.equals("")){
                    ar_good_cust.add(custIDStrGood);
                }
                //Toasty.info(context,"Auto save Good Weights is running ...",Toast.LENGTH_LONG).show();
                Log.d("okegoodX", String.valueOf(ar_good_cust));
                processGoodWeight(context, ar_good_cust, ar_bad_cust);
            }else {
                //Toasty.warning(context,"Please, process Bad Weights.",Toast.LENGTH_SHORT).show();
            }
        }

    }

    private String finalShippingIDparent = "";
    private ShippingService shippingService;
    private ArrayList<String> ar_orderID_from_tborder = new ArrayList<String>();
    private OrderedService orderedService = new OrderedService();
    private EnumProHelper enumProHelper = new EnumProHelper();

    private void processGoodWeight(Context context, ArrayList<String> ar_good_cust, ArrayList<String> ar_bad_cust) {
        for (int a = 0; a < ar_good_cust.size(); a++) {
            //Log.d(TAG,"GOOD : " + ar_good_cust.get(a));
            String custIDAll = ar_good_cust.get(a);
            if (custIDAll.length() > 3) {
                shippingService = new ShippingService();
                finalShippingIDparent = shippingService.saveShipping(
                        context, enumProHelper.keyTypeGoodWeight, getMaxCapacity(context),
                        0, 0, 0, enumProHelper.keyStatusOk
                );
                //DI SPLIT DULU
                String[] rowsplit = custIDAll.toString().trim().split("\\-");
                for (int b = 0; b < rowsplit.length; b++) {
                    if (!rowsplit[b].equals("")) {
                        prepareSaveToShippingChild(context, rowsplit[b], finalShippingIDparent);
                    }
                }
            }
        }
    }


    private void prepareSaveToShippingChild(Context context, String custID, String finalShippingIDparent) {
        orderedRepo = new OrderedRepo(context);
        ar_orderID_from_tborder = orderedRepo.getOrderIDByCustID(custID);
        saveToShippingChild(context, ar_orderID_from_tborder, custID, finalShippingIDparent);
    }

    private void saveToShippingChild(Context context, ArrayList<String> arrayList, String custID, String finalShippingIDparent) {
        if (arrayList.size() > 0){
            for (int i = 0; i < arrayList.size(); i++) {
                shippingService.saveShippingChild(context, finalShippingIDparent, arrayList.get(i));
                orderedService.updateStatusOrdered(context, arrayList.get(i), enumProHelper.statusTerpilih);
            }
        }
        updateStatusSeq(context, custID);
        updateSumShippingParent(context, finalShippingIDparent);
    }

    private void updateStatusSeq(Context context, String custID) {
        seqRoutesRepo = new SeqRoutesRepo(context);
        seqRoutesRepo.updateStatusByCustID("done", custID);
    }

    private ShippingchildRepo shippingchildRepo;

    public void updateSumShippingParent(Context context, String finalShippingIDparent) {
        int totalItem = 0;
        long totalWeight = 0;
        long totalProfit = 0;
        shippingchildRepo = new ShippingchildRepo(context);
        theList = shippingchildRepo.sumTotalWeightProfit(finalShippingIDparent);
        if (theList.size() > 0){
            totalItem = theList.size();
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                totalWeight += Long.parseLong(gen.get(OrderedTable.key_productWeight));
                totalProfit += Long.parseLong(gen.get(OrderedTable.key_productProfit));
            }
            updateTotalShipping(context, finalShippingIDparent, totalItem, totalWeight, totalProfit);
        }
    }

    private ShippingRepo shippingRepo;
    private void updateTotalShipping(Context context, String finalShippingIDparent, int totalItem, long totalWeight, long totalProfit) {
        shippingRepo = new ShippingRepo(context);
        shippingRepo.updateTotal(totalItem, totalWeight, totalProfit, finalShippingIDparent);
    }

    private int getTotalItem(Context context, String finalShippingIDparent) {
        shippingchildRepo = new ShippingchildRepo(context);
        return shippingchildRepo.countingRows(finalShippingIDparent);
    }


    public void updateStatusAllSame(Context context, String status) {
        seqRoutesRepo = new SeqRoutesRepo(context);
        seqRoutesRepo.updateStatusAllSame(status);
    }

    public void processBadWeights(Context context) {
        for (int b = 0; b < ar_bad_cust.size(); b++) {
            Log.d(TAG,"BADS : " + ar_bad_cust.get(b));
        }
    }

}
