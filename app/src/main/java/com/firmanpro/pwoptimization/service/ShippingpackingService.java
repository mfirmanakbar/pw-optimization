package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.util.Log;

import com.firmanpro.pwoptimization.adapter.OrderedAdapter;
import com.firmanpro.pwoptimization.entity.OrderedEntity;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingchildRepo;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingChildTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by firmanmac on 11/8/17.
 */

public class ShippingpackingService {

    private ShippingchildRepo shippingchildRepo;
    private ShippingChildTable shippingChildTable;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private OrderedEntity orderedEntity;

    public void getOrdered(Context context, List<OrderedEntity> orderList, OrderedAdapter orderedAdapter, String shippingIDExtra) {
        OrderedTable orderedTable = new OrderedTable();
        shippingchildRepo = new ShippingchildRepo(context);
        shippingChildTable = new ShippingChildTable();
        theList = shippingchildRepo.getAllData(shippingIDExtra);
        if (theList.size()>0) {
            Log.d("gonama0", "Size theList " + String.valueOf(theList.size()));
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);

                orderedEntity = new OrderedEntity();
                orderedEntity.setOrderID(gen.get(shippingChildTable.key_orderID));
                orderedEntity.setProductName(gen.get(orderedTable.key_productName));
                orderedEntity.setProductStatus(gen.get(orderedTable.key_productStatus));
                orderedEntity.setCustomerID(gen.get(orderedTable.key_customerID));
                orderedEntity.setProductWeight(Integer.valueOf(gen.get(orderedTable.key_productWeight)));
                orderedEntity.setProductProfit(Integer.valueOf(gen.get(orderedTable.key_productProfit)));

                orderList.add(orderedEntity);
                orderedAdapter.notifyDataSetChanged();

            }
        }
    }

}
