package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.sqlite.repo.LogRoutesRepo;
import com.firmanpro.pwoptimization.sqlite.repo.OrderedRepo;
import com.firmanpro.pwoptimization.sqlite.table.LogRoutesTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 11/27/17.
 */

public class FinalRoutesService {

    private OrderedRepo orderedRepo;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private LogRoutesRepo logRoutesRepo;
    private String logRoutes;
    private String[] rowsplit, colsplit;
    private ArrayList<String> ar_shortest_custID_by_sequence = new ArrayList<String>();
    private ArrayList<String> ar_shortest_sequence = new ArrayList<String>();
    public ArrayList<String> ar_custID_need_not_to_optimization = new ArrayList<String>();
    private String custIDForOptimization = "";
    private MainService mainService = new MainService();
    private long sumWeightResult = 0;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private AlertDialog alertDialog;

    public int getShortestRoutes(Context context, String shortestRoutesID){
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest) != null
                && !MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest).equals("")){
            //ambil sisa rute
            logRoutes = MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest);
            Log.d("iamyugiA", String.valueOf(logRoutes));
            splitAndSet(logRoutes, true);
        }else {
            //ambil rute lengkap lalu simpan sisa rute ke session
            logRoutesRepo = new LogRoutesRepo(context);
            theList = logRoutesRepo.getOneData(shortestRoutesID);
            if (theList.size() > 0) {
                gen = theList.get(0);
                logRoutes = gen.get(LogRoutesTable.key_logBestRoutesSplit);
                Log.d("iamyugiB", String.valueOf(logRoutes));
                splitAndSet(logRoutes, false);
            }
        }
        return ar_shortest_custID_by_sequence.size();
    }

    private void splitAndSet(String logRoutes, boolean isFromSession) {
        ar_shortest_custID_by_sequence.clear();
        rowsplit = logRoutes.toString().trim().split("\\-");
        Log.d("iamyugi1", String.valueOf(rowsplit.length));
        int index = 0;
        if (!isFromSession){
            index = 1;
        }
        for (int a = 0; a < rowsplit.length; a++) {
            colsplit = rowsplit[a].toString().trim().split("\\~");
            ar_shortest_custID_by_sequence.add(colsplit[index].toString());
            if (a == rowsplit.length-1 && isFromSession){
                ar_shortest_custID_by_sequence.add(colsplit[1].toString());
            }
        }
        Log.d("iamyugi3", String.valueOf(ar_shortest_custID_by_sequence));
    }

    public String getShortestRoutesAll(Context context){
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest) != null
                && !MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest).equals("")){
            //ambil sisa rute
            return MySession.getSessionGlobal(MySession.key_session_shortest_routes_rest);
        }else {
            //ambil rute lengkap lalu simpan sisa rute ke session
            logRoutesRepo = new LogRoutesRepo(context);
            theList = logRoutesRepo.getOneData(enumProHelper.keyShortestRoutesID);
            if (theList.size() > 0) {
                gen = theList.get(0);
                return gen.get(LogRoutesTable.key_logBestRoutesSplit);
            }
        }
        return null;
    }

    public String prepareOptimizeType3(Context context, int optimizationType) {
        if (getShortestRoutes(context, enumProHelper.keyShortestRoutesID) > 0) {
            orderedRepo = new OrderedRepo(context);
            Log.d("iamyugi4", String.valueOf(ar_shortest_custID_by_sequence));
            for (int a = 0; a < ar_shortest_custID_by_sequence.size(); a++) {
                sumWeightResult += orderedRepo.getSumWeight(ar_shortest_custID_by_sequence.get(a));
                if (sumWeightResult <= mainService.getTruckMaxCapacity(context)) {
                    //save semua custID yang gak perlu pake Greedy langsung save ke shipping
                    ar_custID_need_not_to_optimization.add(ar_shortest_custID_by_sequence.get(a));
                } else {
                    sumWeightResult -= orderedRepo.getSumWeight(ar_shortest_custID_by_sequence.get(a));
                    //save satu custID yang perlu pake Greedy dahulu
                    custIDForOptimization = ar_shortest_custID_by_sequence.get(a);
                    if (optimizationType == 3) {
                        popupOptimization3(context, ar_shortest_custID_by_sequence, ar_custID_need_not_to_optimization,
                                sumWeightResult, mainService.getTruckMaxCapacity(context), custIDForOptimization);
                    }
                    return custIDForOptimization;
                }
            }
        }
        return "";
    }

    public long getCapacityCustIDSkip(Context context) {
        int sumWeightResult = 0;
        if (getShortestRoutes(context, enumProHelper.keyShortestRoutesID) > 0) {
            orderedRepo = new OrderedRepo(context);
            for (int a = 0; a < ar_shortest_custID_by_sequence.size(); a++) {
                sumWeightResult += orderedRepo.getSumWeight(ar_shortest_custID_by_sequence.get(a));
                if (sumWeightResult > mainService.getTruckMaxCapacity(context)) {
                    sumWeightResult -= orderedRepo.getSumWeight(ar_shortest_custID_by_sequence.get(a));
                    return mainService.getTruckMaxCapacity(context) - sumWeightResult;
                }
            }
        }
        return sumWeightResult;
    }

    public ArrayList<String> getCustIDSkipOnly(Context context) {
        int sumWeightResult = 0;
        ArrayList<String> ar_getCustIDSkipOnly = new ArrayList<String>();
        if (getShortestRoutes(context, enumProHelper.keyShortestRoutesID) > 0) {
            orderedRepo = new OrderedRepo(context);
            for (int a = 0; a < ar_shortest_custID_by_sequence.size(); a++) {
                sumWeightResult += orderedRepo.getSumWeight(ar_shortest_custID_by_sequence.get(a));
                if (sumWeightResult <= mainService.getTruckMaxCapacity(context)) {
                    ar_getCustIDSkipOnly.add(ar_shortest_custID_by_sequence.get(a));
                } else {
                    return ar_getCustIDSkipOnly;
                }
            }
        }
        return ar_getCustIDSkipOnly;
    }

    private void popupOptimization3(Context context, ArrayList<String> ar_shortest_custID_by_sequence,
                                    ArrayList<String> ar_custID_need_not_to_optimization, long sumWeightResult,
                                    long truckMaxCapacity, String custIDForOptimization) {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_optimization3, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        Button btnOt3Close = (Button)dialogView.findViewById(R.id.btnOt3Close);
        TextView txtOt3ShortestRoutes = (TextView)dialogView.findViewById(R.id.txtOt3ShortestRoutes);
        TextView txtOt3SkipThisCustomers = (TextView)dialogView.findViewById(R.id.txtOt3SkipThisCustomers);
        TextView txtOt3CurrentWeight = (TextView)dialogView.findViewById(R.id.txtOt3CurrentWeight);
        TextView txtOt3MaxCapacity = (TextView)dialogView.findViewById(R.id.txtOt3MaxCapacity);
        TextView txtOt3NeedOptimization = (TextView)dialogView.findViewById(R.id.txtOt3NeedOptimization);

        txtOt3ShortestRoutes.setText(String.valueOf(ar_shortest_custID_by_sequence));
        txtOt3SkipThisCustomers.setText(String.valueOf(ar_custID_need_not_to_optimization));
        txtOt3CurrentWeight.setText(MyConverting.format_angka(String.valueOf(sumWeightResult)) + " gram");
        txtOt3MaxCapacity.setText(MyConverting.format_angka(String.valueOf(truckMaxCapacity)) + " gram");
        txtOt3NeedOptimization.setText(custIDForOptimization);

        btnOt3Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }

}
