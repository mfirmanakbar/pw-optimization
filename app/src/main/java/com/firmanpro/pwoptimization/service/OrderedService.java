package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firmanpro.pwoptimization.adapter.OrderedAdapter;
import com.firmanpro.pwoptimization.config.ApiAddress;
import com.firmanpro.pwoptimization.config.DataStatic;
import com.firmanpro.pwoptimization.entity.ApiOrderedEntity;
import com.firmanpro.pwoptimization.entity.OrderedEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.sqlite.repo.OrderedRepo;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 10/29/17.
 */

public class OrderedService {

    private String TAG = "ord_ser";
    private OrderedRepo orderedRepo;
    private OrderedTable orderedTable;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private OrderedEntity orderedEntity;
    private EnumProHelper enumsHelper = new EnumProHelper();
    private CustomerService customerService = new CustomerService();

    private JsonObjectRequest jsonObjectRequest;
    private Gson gsonRes;
    private ApiOrderedEntity apiOrderedEntity = new ApiOrderedEntity();

    public void createOrderedServer(final Context context, final RelativeLayout reLay_loading, boolean isOnline, int typeDatabase){
        if (isOnline) {
            jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    ApiAddress.api_import_ordered,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            gsonRes = new GsonBuilder().create();
                            apiOrderedEntity = gsonRes.fromJson(response.toString(), ApiOrderedEntity.class);
                            Log.d(TAG, response.toString());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.getMessage());
                }
            });
            jsonObjectRequest.setRetryPolicy(
                    new DefaultRetryPolicy(
                            60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT //after custome
                    )
            );
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsonObjectRequest);
            queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
                @Override
                public void onRequestFinished(Request<String> request) {
                    setDataOrdered(context, apiOrderedEntity, reLay_loading);
                    Log.d("gonam", "done ordered");
                }
            });
        }else {
            gsonRes = new GsonBuilder().create();
            if (typeDatabase == 1) {
                apiOrderedEntity = gsonRes.fromJson(DataStatic.import_tb_ordered, ApiOrderedEntity.class);
            }else if (typeDatabase == 2){
                apiOrderedEntity = gsonRes.fromJson(DataStatic.import_tb_ordered_2, ApiOrderedEntity.class);
            }
            setDataOrdered(context, apiOrderedEntity, reLay_loading);
        }
    }

    private void setDataOrdered(Context context, ApiOrderedEntity apiOrderedEntity, RelativeLayout reLay_loading) {
        for (int i = 0; i < i + 1; i++) {
            try {
                if (apiOrderedEntity.getImport_tb_ordered().get(i) != null) {
                    saveOrdered(context,
                            apiOrderedEntity.getImport_tb_ordered().get(i).getOrderID(),
                            apiOrderedEntity.getImport_tb_ordered().get(i).getProductName(),
                            apiOrderedEntity.getImport_tb_ordered().get(i).getProductStatus(),
                            apiOrderedEntity.getImport_tb_ordered().get(i).getCustomerID(),
                            (int) apiOrderedEntity.getImport_tb_ordered().get(i).getProductWeight(),
                            (int) apiOrderedEntity.getImport_tb_ordered().get(i).getProductProfit()
                    );
                } else {
                    break;
                }
            } catch (Exception e) {
                break;
            }
        }
        reLay_loading.setVisibility(View.GONE);
        Toasty.success(context, "Ordered - Database Server berhasil disimpan.", Toast.LENGTH_SHORT, true).show();
    }

    public void saveOrdered(Context context, String orderID, String productName, String productStatus,
                            String customerID, int productWeight, int productProfit){
        orderedRepo = new OrderedRepo(context);
        orderedTable = new OrderedTable();
        orderedTable.orderID = orderID;
        orderedTable.productName = productName;
        orderedTable.productStatus = productStatus;
        orderedTable.customerID = customerID;
        orderedTable.productWeight = productWeight;
        orderedTable.productProfit = productProfit;
        orderedRepo.insert(orderedTable);
    }

    public boolean getOrdered(Context context, List<OrderedEntity> orderList, OrderedAdapter orderedAdapter){
        orderedRepo = new OrderedRepo(context);
        orderedTable = new OrderedTable();
        theList = orderedRepo.getAllData();
        if (theList.size()>0) {
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);

                printThisLog(gen, orderedTable, a);

                orderedEntity = new OrderedEntity();
                orderedEntity.setOrderID(gen.get(orderedTable.key_orderID));
                orderedEntity.setProductName(gen.get(orderedTable.key_productName));
                orderedEntity.setProductStatus(gen.get(orderedTable.key_productStatus));
                orderedEntity.setCustomerID(gen.get(orderedTable.key_customerID));
                orderedEntity.setProductWeight(Integer.valueOf(gen.get(orderedTable.key_productWeight)));
                orderedEntity.setProductProfit(Integer.valueOf(gen.get(orderedTable.key_productProfit)));

                orderList.add(orderedEntity);
                orderedAdapter.notifyDataSetChanged();
            }
            return true;
        }else {
            Toasty.error(context,"Mohon import/reset database.", Toast.LENGTH_SHORT).show();
            //context.startActivity(new Intent(context, AdminActivity.class));
        }
        return false;
    }

    private void printThisLog(HashMap<String, String> gen, OrderedTable orderedTable, int a) {
        Log.d(TAG+String.valueOf(a), gen.get(orderedTable.key_orderID));
        Log.d(TAG+String.valueOf(a), gen.get(orderedTable.key_productName));
        Log.d(TAG+String.valueOf(a), gen.get(orderedTable.key_productStatus));
        Log.d(TAG+String.valueOf(a), gen.get(orderedTable.key_customerID));
        Log.d(TAG+String.valueOf(a), gen.get(orderedTable.key_productWeight));
        Log.d(TAG+String.valueOf(a), gen.get(orderedTable.key_productProfit));
    }

    public void updateStatusOrdered(Context context, String orderID, String productStatus){
        orderedRepo = new OrderedRepo(context);
        orderedTable = new OrderedTable();
        orderedTable.orderID = orderID;
        orderedTable.productStatus = productStatus;
        orderedRepo.updateStatusByOrderID(orderedTable);
    }

    public void updateStatusData(Context context, String status){
        orderedRepo = new OrderedRepo(context);
        orderedRepo.updateStatusAllData(status);
    }

    public void deleteAllOrdered(Context context){
        orderedRepo = new OrderedRepo(context);
        orderedTable = new OrderedTable();
        orderedRepo.deleteAllData();
    }

}
