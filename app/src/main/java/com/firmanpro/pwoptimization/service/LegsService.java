package com.firmanpro.pwoptimization.service;

import android.content.Context;

import com.firmanpro.pwoptimization.sqlite.repo.LegsRepo;
import com.firmanpro.pwoptimization.sqlite.table.LegsTable;

/**
 * Created by firmanmac on 11/24/17.
 */

public class LegsService {

    private LegsRepo legsRepo;
    private LegsTable legsTable;

    public void saveLegs(Context context, String legsData, String shippingID) {
        legsRepo = new LegsRepo(context);
        legsTable = new LegsTable();
        legsTable.shippingID = shippingID;
        legsTable.legsData = legsData;
        legsRepo.insert(legsTable);
    }

    public void deleteLegs(Context context, String shippingID) {
        legsRepo = new LegsRepo(context);
        legsRepo.deleteByShippingID(shippingID);
    }

    public void deleteAllLegs(Context context) {
        legsRepo = new LegsRepo(context);
        legsRepo.deleteAllData();
    }

}
