package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.activity.ShippingActivity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.GuiHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.sqlite.repo.CustomerRepo;
import com.firmanpro.pwoptimization.sqlite.repo.LegsRepo;
import com.firmanpro.pwoptimization.sqlite.repo.LogRoutesRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingRepo;
import com.firmanpro.pwoptimization.sqlite.repo.ShippingchildRepo;
import com.firmanpro.pwoptimization.sqlite.table.CustomerTable;
import com.firmanpro.pwoptimization.sqlite.table.LegsTable;
import com.firmanpro.pwoptimization.sqlite.table.LogRoutesTable;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingChildTable;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 11/9/17.
 */

public class ShippingroutesService {

    private final String TAG = "routesx_ser";

    private ArrayList<String> ar_title = new ArrayList<String>();
    private ArrayList<Double> ar_lat = new ArrayList<Double>();
    private ArrayList<Double> ar_lng = new ArrayList<Double>();
    private ArrayList<String> ar_region = new ArrayList<String>();

    private EnumProHelper enumProHelper = new EnumProHelper();
    private HashMap<Integer, Integer> ar_all_distance = new HashMap<Integer, Integer>();
    private HashMap<Integer, Integer> ar_all_duration = new HashMap<Integer, Integer>();
    private HashMap<Integer, JSONArray> ar_all_jLegs = new HashMap<Integer, JSONArray>();

    /**
     * jangan diclear kecuali button reset diklik
     * karena di sini palygon disimpan
     * */
    private ArrayList<JSONArray> ar_best_jLegs = new ArrayList<JSONArray>();
    private ArrayList<Integer> ar_best_ori = new ArrayList<Integer>();
    private ArrayList<Integer> ar_best_dst = new ArrayList<Integer>();

    private LegsRepo legsRepo;
    private LegsTable legsTable;
    private LogRoutesTable routesTable;
    private ShippingChildTable shippingChildTable;
    private LogRoutesRepo routesRepo;
    private ShippingchildRepo shippingchildRepo;
    private CustomerRepo customerRepo;
    private ArrayList<HashMap<String, String>> theList, theList2, theList3, theList4;
    private HashMap<String, String> gen, gen2, gen3, gen4;

    private View marker;
    private TextView txtForMaps;
    private LatLng latLngValue;
    private Marker custMarker;

    private ProgressBar loadingBars;
    private Button btnResetPath, btnAllPath, btnBestPath;
    private TextView txtLogAllPath, txtLogBestPath, txtLogBestFinal;
    private ScrollView scLogAllPath, scLogBestPath;

    private int lastIndexOri = 0;

    private ArrayList<LatLng> points = null;
    private PolylineOptions lineOptions = null;

    private String logBestRoutesSplit ="";

    private String shippingIDExtra = "";

    private LegsService legsService = new LegsService();

    private JSONArray jsonArrayLegs = null;

    private LogRoutesRepo logRoutesRepo;

    public void theConstructors(ProgressBar loadingBars, ScrollView scLogAllPath, ScrollView scLogBestPath,
                                TextView txtLogAllPath, TextView txtLogBestPath, Button btnResetPath, Button btnAllPath,
                                Button btnBestPath, TextView txtLogBestFinal) {
        this.loadingBars = loadingBars;
        this.scLogAllPath = scLogAllPath;
        this.scLogBestPath = scLogBestPath;
        this.txtLogAllPath = txtLogAllPath;
        this.txtLogBestPath = txtLogBestPath;
        this.btnResetPath = btnResetPath;
        this.btnAllPath = btnAllPath;
        this.btnBestPath = btnBestPath;
        this.txtLogBestFinal = txtLogBestFinal;
    }

    private void isProgressBarShow(boolean value) {
        if (value)
            this.loadingBars.setVisibility(View.VISIBLE);
        else
            this.loadingBars.setVisibility(View.GONE);
    }

    private void scrollDown(final ScrollView scrollView){
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    /**untuk mendapatkan data lat lng customers*/
    public void getCustomersLocation(Context context, String shippingIDExtra, GoogleMap mMap, boolean isMenuShipping,
                                     boolean modeFindDistance, String shippingIDForFindDistance) {

        /**
         * isMenuShipping adalah parameter untuk menentukakan apakah activity ini
         * dibuka atas keperluan menu shipping yang menampilkan/generate data shipping routes
         * atau hanya untuk generate/menampilkan shortest routes untuk keperluan
         * optimization type 3
         * */

        this.shippingIDExtra = shippingIDExtra;

        /**clear first*/
        if (ar_title.size() > 0){
            ar_title.clear();
            ar_region.clear();
            ar_lat.clear();
            ar_lng.clear();
        }

        /**set titik awal ke array*/
        MySession.beginInitialization(context);
        ar_title.add("Lokasi Awal");
        ar_region.add("-");
        ar_lat.add(Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lat)));
        ar_lng.add(Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lng)));

        /**show titik awal ke maps*/
        setCustomersMarkerOnMaps(context, mMap, ar_title.get(0), ar_region.get(0),
                ar_lat.get(0), ar_lng.get(0), true);

        /**set camera zoom & focus*/
        setCamFocusAndZoom(mMap, ar_lat.get(0), ar_lng.get(0), 8.0f);

        if(!modeFindDistance) {
            /**get customers by ordered || all customers*/
            if (isMenuShipping) {
                //get data customers sesuai ordered
                getDataCustomersOrdered(context, mMap);
            } else {
                //get data all customers
                getAllDataCustomersNoFilter(context, mMap);
            }

            /**cek data history pada tabel routesTable*/
            routesRepo = new LogRoutesRepo(context);
            routesTable = new LogRoutesTable();
            theList = routesRepo.getOneData(shippingIDExtra);
            if (theList.size() > 0) {
                /**jika tabel routesTable ada isinya
                 * maka munculkan histori rute/path
                 * serta save data ke array*/

                btnResetPath.setVisibility(View.VISIBLE);
                btnAllPath.setVisibility(View.GONE);
                btnBestPath.setVisibility(View.GONE);

                //isRoutesFromDB = true;
                gen = theList.get(0);
                txtLogAllPath.setText(gen.get(routesTable.key_logAllPath));
                txtLogBestPath.setText(gen.get(routesTable.key_logBestPath));
                txtLogBestFinal.setText(gen.get(routesTable.key_logFinalPath));

                //draw final best path from database SQLite
                drawFinalBestPathDatabase(context, mMap, shippingIDExtra);

            } else {
                /**jika tabel routesTable gak ada isinya
                 * maka hanya save data ke array*/

                btnResetPath.setVisibility(View.GONE);
                btnAllPath.setVisibility(View.VISIBLE);
                btnBestPath.setVisibility(View.GONE);
                //isRoutesFromDB = false;

            }
        }else {
            //get data customers sesuai shipping id yang dicari
            getDataCustomersByShippingID(context, mMap, shippingIDForFindDistance);
        }
    }

    private void getDataCustomersByShippingID(Context context, GoogleMap mMap, String shippingIDForFindDistance) {
        shippingchildRepo = new ShippingchildRepo(context);
        String[] rowsplit = shippingIDForFindDistance.toString().trim().split("\\-");
        for (int a = 0; a < rowsplit.length; a++) {
            if (!rowsplit[a].equals("")){
                String custID = shippingchildRepo.getCustIDByShippingID(rowsplit[a]);
                Log.d("someicecream", custID);
                if (!custID.equals("")){
                    customerRepo = new CustomerRepo(context);
                    theList = customerRepo.getByCustID(custID);
                    gen = theList.get(0);
                    ar_title.add(gen.get(OrderedTable.key_customerID));
                    ar_region.add(gen.get(CustomerTable.key_customerProvince));
                    ar_lat.add(Double.valueOf(gen.get(CustomerTable.key_customerLat)));
                    ar_lng.add(Double.valueOf(gen.get(CustomerTable.key_customerLng)));
                    setCustomersMarkerOnMaps(context, mMap, gen.get(OrderedTable.key_customerID),
                            gen.get(CustomerTable.key_customerProvince), Double.valueOf(gen.get(CustomerTable.key_customerLat)),
                            Double.valueOf(gen.get(CustomerTable.key_customerLng)), false);
                }
            }
        }
        btnResetPath.setVisibility(View.GONE);
        btnAllPath.setVisibility(View.VISIBLE);
        btnBestPath.setVisibility(View.GONE);
    }

    private void getAllDataCustomersNoFilter(Context context, GoogleMap mMap) {
        customerRepo = new CustomerRepo(context);
        theList4 = customerRepo.getAllData();
        if (theList4.size()>0) {
            for (int a = 0; a < theList4.size(); a++) {
                gen4 = theList4.get(a);
                ar_title.add(gen4.get(OrderedTable.key_customerID));
                ar_region.add(gen4.get(CustomerTable.key_customerProvince));
                ar_lat.add(Double.valueOf(gen4.get(CustomerTable.key_customerLat)));
                ar_lng.add(Double.valueOf(gen4.get(CustomerTable.key_customerLng)));
                setCustomersMarkerOnMaps(context, mMap, gen4.get(OrderedTable.key_customerID),
                        gen4.get(CustomerTable.key_customerProvince), Double.valueOf(gen4.get(CustomerTable.key_customerLat)),
                        Double.valueOf(gen4.get(CustomerTable.key_customerLng)), false);

            }
        }
    }

    private void drawFinalBestPathDatabase(Context context, GoogleMap mMap, String shippingIDExtra) {
        legsRepo = new LegsRepo(context);
        legsTable = new LegsTable();
        theList3 = legsRepo.getDataByShippingID(shippingIDExtra);
        if (theList3.size() > 0) {
            for (int i = 0; i < theList3.size(); i++) {
                gen3 = theList3.get(i);
                try {
                    jsonArrayLegs = new JSONArray(String.valueOf(gen3.get(legsTable.key_legsData)));
                    drawSteps(context, mMap, jsonArrayLegs,false, 0, 0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getDataCustomersOrdered(Context context, GoogleMap mMap) {
        shippingchildRepo = new ShippingchildRepo(context);
        shippingChildTable = new ShippingChildTable();
        theList = shippingchildRepo.getAllData(shippingIDExtra);
        if (theList.size() > 0) {

            //save semua duplikat data
            List<String> custIDBeforeDistinct = new ArrayList<>();
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                custIDBeforeDistinct.add(gen.get(OrderedTable.key_customerID));
            }
            Set<String> custIDAfterDistinct = new HashSet<>();
            custIDAfterDistinct.addAll(custIDBeforeDistinct);
            custIDBeforeDistinct.clear();

            //save data unik
            custIDBeforeDistinct.addAll(custIDAfterDistinct);

            if (custIDAfterDistinct.size() > 0){

                //get data unik
                for (String strCustID : custIDAfterDistinct){
                    customerRepo = new CustomerRepo(context);
                    theList2 = customerRepo.getByCustID(strCustID);
                    if (theList2.size()>0) {
                        for (int b = 0; b < theList2.size(); b++) {
                            gen2 = theList2.get(b);
                            ar_title.add(gen2.get(OrderedTable.key_customerID));
                            ar_region.add(gen2.get(CustomerTable.key_customerProvince));
                            ar_lat.add(Double.valueOf(gen2.get(CustomerTable.key_customerLat)));
                            ar_lng.add(Double.valueOf(gen2.get(CustomerTable.key_customerLng)));
                            setCustomersMarkerOnMaps(context, mMap, gen2.get(OrderedTable.key_customerID),
                                    gen2.get(CustomerTable.key_customerProvince), Double.valueOf(gen2.get(CustomerTable.key_customerLat)),
                                    Double.valueOf(gen2.get(CustomerTable.key_customerLng)), false);
                        }
                    }
                }

            }

        }
    }

    private void setCamFocusAndZoom(GoogleMap mMap, Double lat, Double lng, float camZomm) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), camZomm));
    }

    private void setCustomersMarkerOnMaps(Context context, GoogleMap mMap, String titles, String region,
                                          Double lat, Double lng, boolean isWarehouseLatLng) {
        latLngValue = new LatLng(Double.valueOf(lat), Double.valueOf(lng));
        if (isWarehouseLatLng) {
            marker = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.icon_for_maps_b, null);
            txtForMaps = (TextView) marker.findViewById(R.id.txtForMaps);
            txtForMaps.setText("0");
        }else{
            marker = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.icon_for_maps, null);
            txtForMaps = (TextView) marker.findViewById(R.id.txtForMaps);
            txtForMaps.setText(titles.replace("CUS", ""));
        }
        custMarker = mMap.addMarker(new MarkerOptions()
                .position(latLngValue)
                .title(titles + " - " + region)
                .icon(BitmapDescriptorFactory.fromBitmap(GuiHelper.createDrawableFromView(context, marker))));
    }

    public void clickButton(final Context context, Button btnResetPath, Button btnAllPath,
                            Button btnBestPath, final GoogleMap mMap, final boolean isMenuShipping, final boolean modeFindDistance) {
        btnResetPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickResetDataMaps(context, mMap);
            }
        });
        btnAllPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * load all alternative/bad routes
                 * validation for minimal 2 customers location to create a routes
                 * */
                if (ar_title.size() > 1) {
                    clickFindAllBadRoutes(context, mMap, lastIndexOri, 1);
                }
            }
        });
        btnBestPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickBestPath(context, mMap, isMenuShipping, modeFindDistance);
            }
        });
    }

    private void clickResetDataMaps(Context context, GoogleMap mMap) {
        ar_all_distance.clear();
        ar_all_duration.clear();
        ar_all_jLegs.clear();
        ar_best_jLegs.clear();
        ar_best_ori.clear();
        ar_best_dst.clear();
        lastIndexOri = 0;
        reloadMaps(context, mMap);
        txtLogAllPath.setText("");
        txtLogBestPath.setText("");
        txtLogBestFinal.setText("");
        isProgressBarShow(false);
        btnResetPath.setVisibility(View.GONE);
        btnAllPath.setVisibility(View.VISIBLE);
        btnBestPath.setVisibility(View.GONE);
        logBestRoutesSplit = "";
    }

    private void reloadMaps(Context context, GoogleMap mMap) {
        //clear maps, hapus semua rute dan marker
        mMap.clear();
        //set marker titik awal
        setCustomersMarkerOnMaps(context, mMap, ar_title.get(0), ar_region.get(0),
                ar_lat.get(0), ar_lng.get(0), true);
        //set semua marker
        for (int i = 1; i < ar_title.size(); i++) {
            setCustomersMarkerOnMaps(context, mMap,
                    ar_title.get(i),
                    ar_region.get(i),
                    ar_lat.get(i),
                    ar_lng.get(i),
                    false);
        }
    }

    private long sumDistance = 0;
    private void clickBestPath(Context context, GoogleMap mMap,
                               boolean isMenuShipping, boolean modeFindDistance) {
        if (ar_all_distance.size() > 0) {
            Map<Integer, String> map = sortByValues(ar_all_distance);
            Set set2 = map.entrySet();
            Iterator iterator2 = set2.iterator();
            while(iterator2.hasNext()) {
                btnResetPath.setVisibility(View.VISIBLE);
                btnAllPath.setVisibility(View.VISIBLE);
                btnBestPath.setVisibility(View.GONE);
                Map.Entry me2 = (Map.Entry)iterator2.next();

                //reload maps untuk di isi marker & path ulang
                reloadMaps(context, mMap);
                //isi log best path
                String textBetsPath =
                        ar_title.get(lastIndexOri) + " ke " + ar_title.get((Integer) me2.getKey()) + "\n"
                                + "Jarak Rute : " + MyConverting.meterToKM((Integer) me2.getValue()) + "\n"
                                + "Durasi Rute: " + MyConverting.beautyTime(ar_all_duration.get(me2.getKey())) + "\n\n";
                String oldLogBestPath = txtLogBestPath.getText().toString();
                txtLogBestPath.setText(oldLogBestPath + textBetsPath);
                scrollDown(scLogBestPath);

                sumDistance += Long.valueOf(String.valueOf(me2.getValue()));

                //final rute untuk database dengan format logBestRoutesSplit "ori~dst~distance~duration-"
                String ruteFinal = "";
                if (lastIndexOri==0){
                    ruteFinal = "0 - " + ar_title.get((Integer) me2.getKey());
                    logBestRoutesSplit = "0"
                            + "~" + ar_title.get((Integer) me2.getKey())
                            + "~" + String.valueOf(me2.getValue())
                            + "~" + String.valueOf(ar_all_duration.get(me2.getKey())) + "-";
                }else{
                    ruteFinal = txtLogBestFinal.getText().toString() + " - " + ar_title.get((Integer) me2.getKey());
                    logBestRoutesSplit += ar_title.get(lastIndexOri)
                            + "~" + ar_title.get((Integer) me2.getKey())
                            + "~" + String.valueOf(me2.getValue())
                            + "~" + String.valueOf(ar_all_duration.get(me2.getKey())) + "-";
                }
                txtLogBestFinal.setText(ruteFinal);

                //draw best rute
                ar_best_jLegs.add(ar_all_jLegs.get(me2.getKey()));
                ar_best_ori.add(lastIndexOri);
                ar_best_dst.add((int) me2.getKey());
                for (int a = 0; a < ar_best_jLegs.size(); a++) {
                    drawSteps(context, mMap, ar_best_jLegs.get(a),
                            false, lastIndexOri, (int) me2.getKey());
                }
                lastIndexOri = (int) me2.getKey();
                ar_all_distance.clear();
                ar_all_duration.clear();
                ar_all_jLegs.clear();

                if (!modeFindDistance) {
                    //simpan ke database
                    if (ar_best_jLegs.size() + 1 == ar_title.size()) {
                        Log.d("tersenyum2", "simpan ke database 3");

                        Log.d("distcountingx", shippingIDExtra);
                        if (shippingIDExtra.equals(enumProHelper.keyShortestRoutesID)) {
                            //proses part 1
                            SeqRoutesService seqRoutesService = new SeqRoutesService();
                            seqRoutesService.deleteAll(context);
                            String[] rowsplit = ruteFinal.toString().trim().split("\\-");
                            for (int b = 1; b < rowsplit.length; b++) {
                                seqRoutesService.insertSeq(context, rowsplit[b].trim());
                            }
                        }

                        //proses part 2
                        btnResetPath.setVisibility(View.VISIBLE);
                        btnAllPath.setVisibility(View.GONE);
                        btnBestPath.setVisibility(View.GONE);
                        saveRoutes(context,
                                txtLogAllPath.getText().toString(),
                                txtLogBestPath.getText().toString(),
                                txtLogBestFinal.getText().toString(),
                                logBestRoutesSplit);
                        for (int a = 0; a < ar_best_jLegs.size(); a++) {
                            if (a == 0) {
                                //kalau save untuk pertama kali hapus semua datanya dulu. lalu save semuanya
                                //fungsi hapus ini cuma sekali aja saat index 0 sisanya tetep save data
                                legsService.deleteLegs(context, shippingIDExtra);
                            }
                            saveLegs(context, String.valueOf(ar_best_jLegs.get(a)));
                        }
                    }
                }else {
                    ShippingActivity.distanceResultFind = String.valueOf(sumDistance);
                }
                break;
            }
        }
    }

    private void saveLegs(Context context, String legsData) {
        legsService.saveLegs(context, legsData, shippingIDExtra);
    }

    public void saveRoutes(Context context, String logAllPath, String logBestPath, String logFinalPath,
                           String logBestRoutesSplit){
        routesRepo = new LogRoutesRepo(context);
        routesTable = new LogRoutesTable();
        routesTable.shippingID = shippingIDExtra;
        routesTable.logAllPath = logAllPath;
        routesTable.logBestPath = logBestPath;
        routesTable.logFinalPath = logFinalPath;
        routesTable.logBestRoutesSplit = logBestRoutesSplit;
        routesRepo.deleteByShippingID(shippingIDExtra);
        routesRepo.insert(routesTable);
        updateTotalDistance(context, shippingIDExtra, logBestRoutesSplit);
    }

    private ShippingRepo shippingRepo;
    private void updateTotalDistance(Context context, String shippingIDExtra, String logBestRoutesSplit) {

        //logBestRoutesSplit: final rute untuk database dengan format "ori~dst~distance~duration-"

        long distances = 0, duration = 0;
        String[] rowsplit = logBestRoutesSplit.toString().trim().split("\\-");
        for (int a = 0; a < rowsplit.length; a++) {
            if (!rowsplit[a].equals("")){
                String[] cols1 = rowsplit[a].toString().trim().split("\\~");
                distances += Long.parseLong(cols1[2]);
                duration += Long.parseLong(cols1[3]);
            }
        }

        shippingRepo = new ShippingRepo(context);
        //update DB distances
        shippingRepo.updateTotalDistance(shippingIDExtra, String.valueOf(distances));
        //update DB duration
        shippingRepo.updateTotalDuration(shippingIDExtra, String.valueOf(duration));

    }

    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest){
        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin+"&"+str_dest+"&"+sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
        return url;
    }

    private void clickFindAllBadRoutes(Context context, GoogleMap mMap, int index_ori, int index_dst) {

        Log.d("tersenyum0", String.valueOf(index_ori) + " ke " + String.valueOf(index_dst));
        Log.d("tersenyum1", ar_title.get(index_ori) + " ke " + ar_title.get(index_dst));

        if (index_dst < ar_title.size()) {

            for (int q = index_dst; q < ar_title.size(); q++) {
                if (q + 1 == ar_title.size()) {
                    //untuk hide loading saat find all bad path
                    isProgressBarShow(false);
                }
                if (!ar_best_dst.contains(q)) {
                    isProgressBarShow(true);
                    btnResetPath.setVisibility(View.GONE);
                    btnAllPath.setVisibility(View.GONE);
                    btnBestPath.setVisibility(View.GONE);
                    LatLng origin = new LatLng(ar_lat.get(index_ori), ar_lng.get(index_ori));
                    LatLng dest = new LatLng(ar_lat.get(q), ar_lng.get(q));
                    String urlnya = getDirectionsUrl(origin, dest);
                    getDirectionsByUrl(context, mMap, urlnya, index_ori, q);
                    break;
                }
            }

        }

    }

    private void getDirectionsByUrl(final Context context, final GoogleMap mMap, final String urlnya, final int index_ori, final int index_dst) {

        isProgressBarShow(true);

        Log.d("urlnyaz", urlnya);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, urlnya, "",
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jObject = new JSONObject(response.toString());
                            String status = jObject.getString("status");
                            if(status.equals("OK")) {

                                /**mulai*/
                                JSONArray jRoutes = null;
                                JSONArray jLegs = null;
                                int distance = 0; //meters
                                int duration = 0; //second

                                jRoutes = response.getJSONArray("routes");

                                /** Traversing all routes */
                                /**jRoutes.length() //kita pake 1 rute aja asumsi sementara dr uji coba bahwa default free API 1 route*/
                                for(int i=0;i<1;i++){
                                    jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");

                                    distance = (Integer) ((JSONObject)((JSONObject)jLegs.get(i)).get("distance")).get("value");
                                    duration = (Integer) ((JSONObject)((JSONObject)jLegs.get(i)).get("duration")).get("value");
                                    Log.d(TAG, String.valueOf(distance)); Log.d(TAG, String.valueOf(duration));

                                    ar_all_distance.put(index_dst, distance);
                                    ar_all_duration.put(index_dst, duration);
                                    ar_all_jLegs.put(index_dst, jLegs);

                                    scrollDown(scLogAllPath);
                                    String textRowPath =
                                            ar_title.get(index_ori) + " ke " + ar_title.get(index_dst) + "\n"
                                            + "Jarak Rute : " + MyConverting.meterToKM(distance) + "\n"
                                            + "Durasi Rute: " + MyConverting.beautyTime(duration) + "\n\n";
                                    String oldLogAllPath = txtLogAllPath.getText().toString();
                                    txtLogAllPath.setText(oldLogAllPath + textRowPath);
                                    scrollDown(scLogAllPath);

                                    drawSteps(context, mMap, jLegs, true, index_ori, index_dst);

                                    /**kalau data_from_database false gak perlu di isi karena dari DB udah ada*/

                                }
                                /**selesai*/

                            }else {
                                btnResetPath.setVisibility(View.VISIBLE);
                                btnAllPath.setVisibility(View.GONE);
                                btnBestPath.setVisibility(View.VISIBLE);
                                isProgressBarShow(false);
                                Toast.makeText(context,"Sorry, data not found !",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {}
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage(), TAG);
                Log.d(TAG, "FAIL!");
                try {
                    isProgressBarShow(false);
                    Toast.makeText(context,"Request error !",Toast.LENGTH_SHORT).show();
                }catch (Exception a){}
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(120000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsonObjectRequest);

    }

    private void drawSteps(Context context, GoogleMap mMap, JSONArray jLegs, boolean isBadRoutes, int index_ori, int index_dst) {
        try {
            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
            JSONArray jSteps = null;
            List path = new ArrayList<HashMap<String, String>>();
            for(int j=0;j<jLegs.length();j++){
                jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");
                /** Traversing all steps */
                for(int k=0;k<jSteps.length();k++){
                    String polyline = "";
                    polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                    List<LatLng> list = decodePoly(polyline);
                    /** Traversing all points */
                    for(int l=0;l<list.size();l++){
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                        hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                        path.add(hm);
                    }
                }
                routes.add(path);
            }
            nowShowDraw(context, mMap, routes, isBadRoutes, index_ori, index_dst);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    private void nowShowDraw(Context context, GoogleMap mMap, List<List<HashMap<String, String>>> result, boolean isBadRoutes, int index_ori, int index_dst) {
        for(int i=0;i<result.size();i++){
            points = new ArrayList<LatLng>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for(int j=0;j<path.size();j++){
                HashMap<String,String> point = path.get(j);
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);
            }
            // Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            //Random rnd = new Random();
            //lineOptions.color(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));

            /**
             * isBadRoutes = true
             * artinya pencarian rute dari json request untuk semua alternative rute
             *
             * isBadRoutes = false
             * artinya pencarian rute dari data array untuk satu rute terabaik
             * */
            if (isBadRoutes) {
                lineOptions.color(ContextCompat.getColor(context, R.color.colorRed));
                /**
                 * load other of all alternative/bad routes
                 * validation last index from array
                 * */
                Log.d(TAG, "Dari " + String.valueOf(index_ori) + " ke " + String.valueOf(index_dst) + ". Size " + String.valueOf(ar_title.size()));
                if (index_dst+1 < ar_title.size()) {

                    try {
                        btnResetPath.setVisibility(View.VISIBLE);
                        btnAllPath.setVisibility(View.GONE);
                        btnBestPath.setVisibility(View.VISIBLE);
                        Thread.sleep(2000);
                        Log.d(TAG, String.valueOf(index_dst+1));
                        isProgressBarShow(true);
                        clickFindAllBadRoutes(context, mMap, index_ori, index_dst+1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }else {
                    btnResetPath.setVisibility(View.VISIBLE);
                    btnAllPath.setVisibility(View.GONE);
                    btnBestPath.setVisibility(View.VISIBLE);
                    isProgressBarShow(false);
                    Toasty.success(context, "Pencarian semua loaksi rute berhasil.", Toast.LENGTH_SHORT).show();
                }
            }else {
                lineOptions.color(ContextCompat.getColor(context, R.color.colorGreenSeaDark));
            }
        }
        // Drawing polyline in the Google Map for the i-th route
        mMap.addPolyline(lineOptions).setWidth(5);

    }


}
