package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firmanpro.pwoptimization.adapter.CustomerAdapter;
import com.firmanpro.pwoptimization.config.ApiAddress;
import com.firmanpro.pwoptimization.config.DataStatic;
import com.firmanpro.pwoptimization.entity.ApiCustomersEntity;
import com.firmanpro.pwoptimization.entity.CustomerEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.sqlite.repo.CustomerRepo;
import com.firmanpro.pwoptimization.sqlite.table.CustomerTable;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 10/31/17.
 */

public class CustomerService {

    private String TAG = "cust_ser";
    private CustomerRepo customerRepo;
    private CustomerTable customerTable;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private CustomerEntity customerEntity;
    private LatLng currentlocation;

    private JsonObjectRequest jsonObjectRequest;
    private Gson gsonRes;
    private ApiCustomersEntity apiCustomersEntity = new ApiCustomersEntity();

    private BitmapDescriptor bitmapDescriptor = null;

    private EnumProHelper enumProHelper = new EnumProHelper();

    public void createCustomersServer(final Context context, final RelativeLayout reLay_loading, boolean isOnline, int typeDatabase){
        if (isOnline) {
            Log.d("cust_ser", "masuk cus");
            jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    ApiAddress.api_import_customers,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("cust_ser", response.toString());
                            gsonRes = new GsonBuilder().create();
                            apiCustomersEntity = gsonRes.fromJson(response.toString(), ApiCustomersEntity.class);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.getMessage());
                }
            });
            jsonObjectRequest.setRetryPolicy(
                    new DefaultRetryPolicy(
                            60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT //after custome
                    )
            );
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsonObjectRequest);
            queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
                @Override
                public void onRequestFinished(Request<String> request) {
                    setDataCustomer(context, apiCustomersEntity);
                    Log.d("gonam", "done customer");
                }
            });
        }else{
            gsonRes = new GsonBuilder().create();
            if (typeDatabase == 1){
                apiCustomersEntity = gsonRes.fromJson(DataStatic.import_tb_customers, ApiCustomersEntity.class);
            }else if (typeDatabase == 2){
                apiCustomersEntity = gsonRes.fromJson(DataStatic.import_tb_customers_2, ApiCustomersEntity.class);
            }
            setDataCustomer(context, apiCustomersEntity);
        }
    }

    private void setDataCustomer(Context context, ApiCustomersEntity apiCustomersEntity) {
        for (int i = 0; i < i + 1; i++) {
            try {
                if (apiCustomersEntity.getImport_tb_customers().get(i) != null) {
                    saveCustomer(context,
                            apiCustomersEntity.getImport_tb_customers().get(i).getCustomerID(),
                            apiCustomersEntity.getImport_tb_customers().get(i).getCustomerProvince(),
                            Double.valueOf(apiCustomersEntity.getImport_tb_customers().get(i).getCustomerLat()),
                            Double.valueOf(apiCustomersEntity.getImport_tb_customers().get(i).getCustomerLng())
                    );
                } else {
                    break;
                }
            } catch (Exception e) {
                break;
            }
        }
        Toasty.success(context, "Customer - Database Server berhasil disimpan.", Toast.LENGTH_SHORT, true).show();
    }


    public void saveCustomer(Context context, String customerID, String customerProvince, Double customerLat, Double customerLng){
        customerRepo = new CustomerRepo(context);
        customerTable = new CustomerTable();
        customerTable.customerID = customerID;
        customerTable.customerProvince = customerProvince;
        customerTable.customerLat = customerLat;
        customerTable.customerLng = customerLng;
        customerRepo.insert(customerTable);
    }

    public void updateCustomer(Context context, String customerID, String customerProvince, Double customerLat, Double customerLng){
        customerRepo = new CustomerRepo(context);
        customerTable = new CustomerTable();
        customerTable.customerID = customerID;
        customerTable.customerProvince = customerProvince;
        customerTable.customerLat = customerLat;
        customerTable.customerLng = customerLng;
        customerRepo.update(customerTable);
        Toasty.success(context,"Data berhasil diubah", Toast.LENGTH_SHORT).show();
    }

    public boolean getCustomers(Context context, List<CustomerEntity> customerLists, CustomerAdapter customerAdapter){
        customerLists.clear();

        customerRepo = new CustomerRepo(context);
        customerTable = new CustomerTable();
        theList = customerRepo.getAllData();

        if (theList.size()>0) {


            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                printThisLog(gen, customerTable, a);

                customerEntity = new CustomerEntity();
                customerEntity.setCustomerID(gen.get(customerTable.key_customerID));
                customerEntity.setCustomerProvince(gen.get(customerTable.key_customerProvince));
                customerEntity.setCustomerLat(Double.valueOf(gen.get(customerTable.key_customerLat)));
                customerEntity.setCustomerLng(Double.valueOf(gen.get(customerTable.key_customerLng)));

                /*Log.d("copascus",
                        gen.get(customerTable.key_customerID)
                        + " KOPI " + gen.get(customerTable.key_customerProvince)
                        + " KOPI " + gen.get(customerTable.key_customerLat)
                        + " KOPI " + gen.get(customerTable.key_customerLng)
                );*/

                customerLists.add(customerEntity);
                customerAdapter.notifyDataSetChanged();

            }
            return true;
        }else {
            Toasty.error(context,"Mohon import/reset database.",Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public void getCustomersMaps(Context context, GoogleMap mMap, String getValCustomersID){
        /**
         * getValCustomersID
         * fungsinya untuk cek ID customer yang di klik null atau gak null
         * kalau null berarti munculin maps biasa
         * kalau gak null berarti select for update customer + buat marker warnanya orange
         * untuk bedain sama marker yang gak di select
         * */

        customerRepo = new CustomerRepo(context);
        customerTable = new CustomerTable();
        theList = customerRepo.getAllData();

        if (theList.size()>0) {
            for (int a = 0; a < theList.size(); a++) {
                gen = theList.get(a);
                printThisLog(gen, customerTable, a);
                currentlocation = new LatLng(
                        Double.valueOf(gen.get(customerTable.key_customerLat)),
                        Double.valueOf(gen.get(customerTable.key_customerLng))
                );
                if (getValCustomersID!=null) {
                    if (gen.get(customerTable.key_customerID).equals(getValCustomersID)){
                        bitmapDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
                    }else {
                        bitmapDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                    }
                }else {
                    bitmapDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                }
                if (a == 0){
                    mMap.addMarker(new MarkerOptions()
                            .position(getGudangLatLng(context))
                            .title(enumProHelper.keyGudang)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(currentlocation));
                }
                mMap.addMarker(new MarkerOptions()
                        .position(currentlocation)
                        .title(gen.get(customerTable.key_customerID))
                        .icon(bitmapDescriptor));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentlocation));
                if (a+1==theList.size()) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentlocation, 9));
                }
            }
        }

    }

    private LatLng getGudangLatLng(Context context) {
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_warehouse_lat) != null
                && MySession.getSessionGlobal(MySession.key_session_warehouse_lng) != null
                && !MySession.getSessionGlobal(MySession.key_session_warehouse_lat).equals("")
                && !MySession.getSessionGlobal(MySession.key_session_warehouse_lng).equals("")){
            return new LatLng(
                    Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lat)),
                    Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lng)));
        }
        return null;
    }

    private void printThisLog(HashMap<String, String> gen, CustomerTable customerTable, int a) {
        Log.d(TAG+String.valueOf(a), gen.get(customerTable.key_customerID));
        Log.d(TAG+String.valueOf(a), gen.get(customerTable.key_customerLat));
        Log.d(TAG+String.valueOf(a), gen.get(customerTable.key_customerLng));
    }

    public void deleteAllCustomer(Context context){
        customerRepo = new CustomerRepo(context);
        customerTable = new CustomerTable();
        customerRepo.deleteAllData();
    }

}
