package com.firmanpro.pwoptimization.service;

import android.content.Context;

import com.firmanpro.pwoptimization.adapter.GreedyAdapter;
import com.firmanpro.pwoptimization.centerformula.KnapsackGreedy;
import com.firmanpro.pwoptimization.entity.GreedyEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.sqlite.repo.OrderedRepo;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by firmanmac on 10/29/17.
 */

public class GreedyService {

    private String TAG = "greedy_ser";
    private OrderedRepo orderedRepo;
    private OrderedTable orderedTable;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private KnapsackGreedy knapsackGreedy = new KnapsackGreedy();
    private EnumProHelper enumProHelper = new EnumProHelper();

    private FinalRoutesService finalRoutesService = new FinalRoutesService();
    private String custIDForOptimization = "";

    public boolean loadGreedyByMode(Context context, List<GreedyEntity> greedyList, GreedyAdapter greedyAdapter, String modeGreedy, String custID){

        /**kalau shortest routes sudah digenerate*/
        //custIDForOptimization = finalRoutesService.prepareOptimizeType3(context, optimizationType);

        if (modeGreedy.equals(enumProHelper.greedyByProfit)) {
            /**clean data formula*/
            knapsackGreedy.cleanDataBarang();
            /**get data dari SQLite*/
            orderedRepo = new OrderedRepo(context);
            orderedTable = new OrderedTable();
            theList = orderedRepo.getAllDataByStatus(enumProHelper.statusTersedia, custID);
            if (theList.size()>0) {
                for (int a = 0; a < theList.size(); a++) {
                    gen = theList.get(a);
                    /**save data ke formula*/
                    knapsackGreedy.saveDataBarang(
                            a, gen.get(orderedTable.key_orderID),
                            gen.get(orderedTable.key_customerID),
                            Integer.valueOf(gen.get(orderedTable.key_productWeight)),
                            Integer.valueOf(gen.get(orderedTable.key_productProfit))
                    );
                }
                knapsackGreedy.processKGProfit(context, greedyList, greedyAdapter, true, false);
                return true;
            }
        }else if (modeGreedy.equals(enumProHelper.greedyByWeight)) {
            /**clean data formula*/
            knapsackGreedy.cleanDataBarang();
            /**get data dari SQLite*/
            orderedRepo = new OrderedRepo(context);
            orderedTable = new OrderedTable();
            theList = orderedRepo.getAllDataByStatus(enumProHelper.statusTersedia, custID);
            if (theList.size()>0) {
                for (int a = 0; a < theList.size(); a++) {
                    gen = theList.get(a);
                    /**save data ke formula*/
                    knapsackGreedy.saveDataBarang(
                            a, gen.get(orderedTable.key_orderID),
                            gen.get(orderedTable.key_customerID),
                            Integer.valueOf(gen.get(orderedTable.key_productWeight)),
                            Integer.valueOf(gen.get(orderedTable.key_productProfit))
                    );
                }
                knapsackGreedy.processKGProfit(context, greedyList, greedyAdapter, false, false);
                knapsackGreedy.processKGBerat(context, greedyList, greedyAdapter, true, false);
                return true;
            }
        }else if (modeGreedy.equals(enumProHelper.greedyByDensity)) {
            /**clean data formula*/
            knapsackGreedy.cleanDataBarang();
            /**get data dari SQLite*/
            orderedRepo = new OrderedRepo(context);
            orderedTable = new OrderedTable();
            theList = orderedRepo.getAllDataByStatus(enumProHelper.statusTersedia, custID);
            if (theList.size()>0) {
                for (int a = 0; a < theList.size(); a++) {
                    gen = theList.get(a);
                    /**save data ke formula*/
                    knapsackGreedy.saveDataBarang(
                            a, gen.get(orderedTable.key_orderID),
                            gen.get(orderedTable.key_customerID),
                            Integer.valueOf(gen.get(orderedTable.key_productWeight)),
                            Integer.valueOf(gen.get(orderedTable.key_productProfit))
                    );
                }
                knapsackGreedy.processKGProfit(context, greedyList, greedyAdapter, false, false);
                knapsackGreedy.processKGBerat(context, greedyList, greedyAdapter, false, false);
                knapsackGreedy.processKGDensitas(context, greedyList, greedyAdapter, true, false);
                return true;
            }
        }
        return false;
    }



}
