package com.firmanpro.pwoptimization.service;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.firmanpro.pwoptimization.activity.ParameterActivity;
import com.firmanpro.pwoptimization.helper.MySession;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 10/29/17.
 */

public class MainService {

    public boolean checkTruckMaxCapacity(Context context){
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity)==null){
            Toasty.error(context,
                    "Maaf, mohon isi parameter maksimal kapasitas transportasi pengiriman.",
                    Toast.LENGTH_SHORT, true).show();
            context.startActivity(new Intent(context, ParameterActivity.class));
        }else {
            return true;
        }
        return false;
    }

    public long getTruckMaxCapacity(Context context){
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity)==null){
            Toasty.error(context,
                    "Maaf, mohon isi parameter maksimal kapasitas transportasi pengiriman.",
                    Toast.LENGTH_SHORT, true).show();
        }else {
            return Long.valueOf(MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity));
        }
        return 0;
    }


}
