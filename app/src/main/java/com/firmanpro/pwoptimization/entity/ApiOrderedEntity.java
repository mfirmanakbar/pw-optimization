package com.firmanpro.pwoptimization.entity;

import java.util.List;

/**
 * Created by firmanmac on 11/10/17.
 */

public class ApiOrderedEntity {


    private List<ImportTbOrderedBean> import_tb_ordered;

    public List<ImportTbOrderedBean> getImport_tb_ordered() {
        return import_tb_ordered;
    }

    public void setImport_tb_ordered(List<ImportTbOrderedBean> import_tb_ordered) {
        this.import_tb_ordered = import_tb_ordered;
    }

    public static class ImportTbOrderedBean {

        private String orderID;
        private String productName;
        private String productStatus;
        private String customerID;
        private double productWeight;
        private double productProfit;

        public String getOrderID() {
            return orderID;
        }

        public void setOrderID(String orderID) {
            this.orderID = orderID;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductStatus() {
            return productStatus;
        }

        public void setProductStatus(String productStatus) {
            this.productStatus = productStatus;
        }

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public double getProductWeight() {
            return productWeight;
        }

        public void setProductWeight(double productWeight) {
            this.productWeight = productWeight;
        }

        public double getProductProfit() {
            return productProfit;
        }

        public void setProductProfit(double productProfit) {
            this.productProfit = productProfit;
        }
    }
}
