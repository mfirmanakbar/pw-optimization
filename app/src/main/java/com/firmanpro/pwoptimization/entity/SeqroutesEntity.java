package com.firmanpro.pwoptimization.entity;

/**
 * Created by firmanmac on 12/8/17.
 */

public class SeqroutesEntity {

    public int idSeq, isGoodWeight;
    public long totalWeight;
    public String customersID, statusDss;

    public SeqroutesEntity() {
    }

    public int getIdSeq() {
        return idSeq;
    }

    public void setIdSeq(int idSeq) {
        this.idSeq = idSeq;
    }

    public int getIsGoodWeight() {
        return isGoodWeight;
    }

    public void setIsGoodWeight(int isGoodWeight) {
        this.isGoodWeight = isGoodWeight;
    }

    public long getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(long totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getCustomersID() {
        return customersID;
    }

    public void setCustomersID(String customersID) {
        this.customersID = customersID;
    }

    public String getStatusDss() {
        return statusDss;
    }

    public void setStatusDss(String statusDss) {
        this.statusDss = statusDss;
    }
}
