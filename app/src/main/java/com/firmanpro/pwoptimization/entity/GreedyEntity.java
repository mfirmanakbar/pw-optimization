package com.firmanpro.pwoptimization.entity;

/**
 * Created by firmanmac on 10/29/17.
 */

public class GreedyEntity {

    private int dataPosition, weight, profit;
    private String orderID, statusGreedy, custID;
    private Double density;

    public GreedyEntity() {
    }

    public String getCustID() {
        return custID;
    }

    public void setCustID(String custID) {
        this.custID = custID;
    }

    public String getStatusGreedy() {
        return statusGreedy;
    }

    public void setStatusGreedy(String statusGreedy) {
        this.statusGreedy = statusGreedy;
    }

    public int getDataPosition() {
        return dataPosition;
    }

    public void setDataPosition(int dataPosition) {
        this.dataPosition = dataPosition;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public Double getDensity() {
        return density;
    }

    public void setDensity(Double density) {
        this.density = density;
    }
}
