package com.firmanpro.pwoptimization.entity;

/**
 * Created by firmanmac on 10/29/17.
 */

public class OrderedEntity {

    private String orderID, productName, productStatus, customerID;
    private int productWeight, productProfit;

    public OrderedEntity() {
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public int getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(int productWeight) {
        this.productWeight = productWeight;
    }

    public int getProductProfit() {
        return productProfit;
    }

    public void setProductProfit(int productProfit) {
        this.productProfit = productProfit;
    }
}
