package com.firmanpro.pwoptimization.entity;

/**
 * Created by firmanmac on 11/1/17.
 */

public class GreedyresultEntity {

    private String grorderID;
    private int grWeight, grProfit;
    private boolean grTakeProfit, grTakeWeight, grTakeDensity;

    public GreedyresultEntity() {
    }

    public String getGrorderID() {
        return grorderID;
    }

    public void setGrorderID(String grorderID) {
        this.grorderID = grorderID;
    }

    public int getGrWeight() {
        return grWeight;
    }

    public void setGrWeight(int grWeight) {
        this.grWeight = grWeight;
    }

    public int getGrProfit() {
        return grProfit;
    }

    public void setGrProfit(int grProfit) {
        this.grProfit = grProfit;
    }

    public boolean isGrTakeProfit() {
        return grTakeProfit;
    }

    public void setGrTakeProfit(boolean grTakeProfit) {
        this.grTakeProfit = grTakeProfit;
    }

    public boolean isGrTakeWeight() {
        return grTakeWeight;
    }

    public void setGrTakeWeight(boolean grTakeWeight) {
        this.grTakeWeight = grTakeWeight;
    }

    public boolean isGrTakeDensity() {
        return grTakeDensity;
    }

    public void setGrTakeDensity(boolean grTakeDensity) {
        this.grTakeDensity = grTakeDensity;
    }
}
