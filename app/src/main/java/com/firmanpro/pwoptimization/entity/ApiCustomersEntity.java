package com.firmanpro.pwoptimization.entity;

import java.util.List;

/**
 * Created by firmanmac on 11/10/17.
 */

public class ApiCustomersEntity {


    private List<ImportTbCustomersBean> import_tb_customers;

    public List<ImportTbCustomersBean> getImport_tb_customers() {
        return import_tb_customers;
    }

    public void setImport_tb_customers(List<ImportTbCustomersBean> import_tb_customers) {
        this.import_tb_customers = import_tb_customers;
    }

    public static class ImportTbCustomersBean {
        /**
         * customerID : CUS1
         * customerProvince : JKT
         * customerLat : -6.228576499999999
         * customerLng : 106.74885100000006
         */

        private String customerID;
        private String customerProvince;
        private String customerLat;
        private String customerLng;

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public String getCustomerProvince() {
            return customerProvince;
        }

        public void setCustomerProvince(String customerProvince) {
            this.customerProvince = customerProvince;
        }

        public String getCustomerLat() {
            return customerLat;
        }

        public void setCustomerLat(String customerLat) {
            this.customerLat = customerLat;
        }

        public String getCustomerLng() {
            return customerLng;
        }

        public void setCustomerLng(String customerLng) {
            this.customerLng = customerLng;
        }
    }
}
