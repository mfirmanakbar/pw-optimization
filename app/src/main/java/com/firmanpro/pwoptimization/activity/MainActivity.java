package com.firmanpro.pwoptimization.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.service.MainService;
import com.firmanpro.pwoptimization.sqlite.repo.LogRoutesRepo;

public class MainActivity extends AppCompatActivity {

    private MainService mainService = new MainService();
    private Button btnMainAdmin, btnMainCustomer, btnMainOredered, btnMainShipping,
            btnMainStatistics, btnMainHelp;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private AlertDialog alertDialog;

    private Intent intent;
    private LogRoutesRepo logRoutesRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**hiding action bat but title bar keep show*/
        getSupportActionBar().hide();

        MySession.beginInitialization(MainActivity.this);

        btnMainAdmin = (Button)findViewById(R.id.btnMainAdmin);
        btnMainCustomer = (Button)findViewById(R.id.btnMainCustomer);
        btnMainOredered = (Button)findViewById(R.id.btnMainOredered);
        btnMainShipping = (Button)findViewById(R.id.btnMainShipping);
        btnMainStatistics = (Button)findViewById(R.id.btnMainStatistics);
        btnMainHelp = (Button)findViewById(R.id.btnMainHelp);

    }


    @Override
    protected void onStart() {
        super.onStart();

        btnMainAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, AdminActivity.class);
                startActivity(intent);
            }
        });

        btnMainCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupCustomerViewType();
            }
        });

        btnMainOredered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, OrderedActivity.class);
                startActivity(intent);
            }
        });

        btnMainShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainService.checkTruckMaxCapacity(MainActivity.this)) {
                    intent = new Intent(MainActivity.this, ShippingActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnMainStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, StatisticsActivity.class);
                startActivity(intent);
            }
        });

        btnMainHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
            }
        });

    }


    private void popupCustomerViewType() {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_menu_customer, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        Button btnPopMenuCustList = (Button)dialogView.findViewById(R.id.btnPopMenuCustList);
        Button btnPopMenuCustSingle = (Button)dialogView.findViewById(R.id.btnPopMenuCustSingle);
        Button btnPopMenuCustClose = (Button)dialogView.findViewById(R.id.btnPopMenuCustClose);
        Button btnPopMenuCustShortest = (Button)dialogView.findViewById(R.id.btnPopMenuCustShortest);


        btnPopMenuCustList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, CustomerActivity.class);
                startActivity(intent);
                alertDialog.dismiss();
            }
        });

        btnPopMenuCustSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, SinglerouteActivity.class);
                startActivity(intent);
                alertDialog.dismiss();
            }
        });

        btnPopMenuCustShortest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callIntentShippingRoutes(alertDialog);
            }
        });

        btnPopMenuCustClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }

    private void callIntentShippingRoutes(AlertDialog alertDialog) {
        intent = new Intent(MainActivity.this, ShippingroutesActivity.class);
        intent.putExtra(enumProHelper.keyIsMenuShipping, enumProHelper.keyNo);
        intent.putExtra(enumProHelper.keyShippingID, enumProHelper.keyShortestRoutesID);
        startActivity(intent);
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }




}
