package com.firmanpro.pwoptimization.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.service.StatisticsService;
import com.github.mikephil.charting.charts.LineChart;

public class StatisticsActivity extends AppCompatActivity {

    private TextView txtSumCustomers, txtSumPackingOrder, txtSumWeights, txtSumProfits, txtSumGoodWeight,
            txtSumBadWeight, txtSumShipping, txtSumDistance, txtSumDuration;
    private LineChart chartStatistics;
    private StatisticsService statisticsService = new StatisticsService();
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        txtSumCustomers = (TextView)findViewById(R.id.txtSumCustomers);
        txtSumPackingOrder = (TextView)findViewById(R.id.txtSumPackingOrder);
        txtSumWeights = (TextView)findViewById(R.id.txtSumWeights);
        txtSumProfits = (TextView)findViewById(R.id.txtSumProfits);
        txtSumGoodWeight = (TextView)findViewById(R.id.txtSumGoodWeight);
        txtSumBadWeight = (TextView)findViewById(R.id.txtSumBadWeight);
        txtSumShipping = (TextView)findViewById(R.id.txtSumShipping);
        txtSumDistance = (TextView)findViewById(R.id.txtSumDistance);
        txtSumDuration = (TextView)findViewById(R.id.txtSumDuration);
        chartStatistics = (LineChart)findViewById(R.id.chartStatistics);

    }

    @Override
    protected void onStart() {
        super.onStart();

        statisticsService.getSumAllResult(StatisticsActivity.this, txtSumCustomers, txtSumPackingOrder, txtSumWeights, txtSumProfits,
                txtSumGoodWeight, txtSumBadWeight, txtSumShipping, txtSumDistance, txtSumDuration);

        statisticsService.getChartResult(StatisticsActivity.this, 0, chartStatistics);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_statistics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
            case R.id.action_chart_type:
                popupChartType();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void popupChartType() {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsActivity.this);
        LayoutInflater inflater = (LayoutInflater) StatisticsActivity.this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_chart_type, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        Button btnPopChartByItem = (Button)dialogView.findViewById(R.id.btnPopChartByItem);
        Button btnPopChartByWeight = (Button)dialogView.findViewById(R.id.btnPopChartByWeight);
        Button btnPopChartByProfit = (Button)dialogView.findViewById(R.id.btnPopChartByProfit);
        Button btnPopChartByDistance = (Button)dialogView.findViewById(R.id.btnPopChartByDistance);
        Button btnPopChartClose = (Button)dialogView.findViewById(R.id.btnPopChartClose);

        btnPopChartByItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statisticsService.getChartResult(StatisticsActivity.this, 0, chartStatistics);
                alertDialog.dismiss();
            }
        });

        btnPopChartByWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statisticsService.getChartResult(StatisticsActivity.this, 1, chartStatistics);
                alertDialog.dismiss();
            }
        });

        btnPopChartByProfit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statisticsService.getChartResult(StatisticsActivity.this, 2, chartStatistics);
                alertDialog.dismiss();
            }
        });

        btnPopChartByDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statisticsService.getChartResult(StatisticsActivity.this, 3, chartStatistics);
                alertDialog.dismiss();
            }
        });


        btnPopChartClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }


}
