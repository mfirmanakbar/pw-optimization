package com.firmanpro.pwoptimization.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.adapter.OrderedAdapter;
import com.firmanpro.pwoptimization.entity.OrderedEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.service.ShippingpackingService;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class ShippingpackingActivity extends AppCompatActivity {

    private ShippingpackingService shippingpackingService = new ShippingpackingService();
    private String TAG = "ord_act";

    private RecyclerView rv_ordered;
    private RecyclerView.LayoutManager lm_ordered;
    private List<OrderedEntity> orderList = new ArrayList<OrderedEntity>();
    private OrderedAdapter orderedAdapter;

    private Intent intent;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private String shippingIDExtra = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shippingpacking);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        intent = getIntent();
        if (intent.getStringExtra(enumProHelper.keyShippingID) != null){
            shippingIDExtra = intent.getStringExtra(enumProHelper.keyShippingID);
            Toasty.info(getApplicationContext(), "ID SHIPPING : " + shippingIDExtra, Toast.LENGTH_SHORT, true).show();
        }else {
            Toasty.error(getApplicationContext(), "Maaf, parameter type view tidak terpilih.", Toast.LENGTH_SHORT, true).show();
            finish();
        }

        /**inisialisasi RecyclerView*/
        rv_ordered = (RecyclerView)findViewById(R.id.rv_ordered);
        lm_ordered = new GridLayoutManager(this, 2); /**kolom tampilan item*/
        rv_ordered.setLayoutManager(lm_ordered);
        rv_ordered.setItemAnimator(new DefaultItemAnimator());
        orderedAdapter = new OrderedAdapter(ShippingpackingActivity.this, orderList);
        rv_ordered.setAdapter(orderedAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        shippingpackingService.getOrdered(ShippingpackingActivity.this, orderList, orderedAdapter, shippingIDExtra);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
