package com.firmanpro.pwoptimization.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.adapter.SeqroutesAdapter;
import com.firmanpro.pwoptimization.adapter.ShippingAdapter;
import com.firmanpro.pwoptimization.entity.SeqroutesEntity;
import com.firmanpro.pwoptimization.entity.ShippingEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.service.SeqRoutesService;
import com.firmanpro.pwoptimization.service.ShippingService;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class ShippingActivity extends AppCompatActivity {

    private ShippingService shippingService = new ShippingService();

    private RecyclerView rv_shipping;
    private RecyclerView.LayoutManager lm_shipping;
    private List<ShippingEntity> shippingList = new ArrayList<ShippingEntity>();
    private ShippingAdapter shippingAdapter;

    private RecyclerView rv_seqroutes;
    private RecyclerView.LayoutManager lm_seqroutes;
    private List<SeqroutesEntity> seqroutesList = new ArrayList<SeqroutesEntity>();
    private SeqroutesAdapter seqroutesAdapter;

    private SeqRoutesService seqRoutesService;
    private TextView txtGoodWeights, txtBadWeights;
    private AlertDialog alertDialog;

    private EnumProHelper enumProHelper = new EnumProHelper();

    public static String distanceResultFind = null;
    private TextView globalValueTxtDistance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        txtGoodWeights = (TextView)findViewById(R.id.txtGoodWeights);
        txtBadWeights = (TextView)findViewById(R.id.txtBadWeights);

        //data shipping
        rv_shipping = (RecyclerView)findViewById(R.id.rv_shipping);
        lm_shipping = new GridLayoutManager(this, 1); /**kolom tampilan item*/
        rv_shipping.setLayoutManager(lm_shipping);
        rv_shipping.setItemAnimator(new DefaultItemAnimator());
        shippingAdapter = new ShippingAdapter(ShippingActivity.this, shippingList);
        rv_shipping.setAdapter(shippingAdapter);

        //data seq routes
        rv_seqroutes = (RecyclerView)findViewById(R.id.rv_seqroutes);
        lm_seqroutes = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_seqroutes.setLayoutManager(lm_seqroutes);
        rv_seqroutes.setItemAnimator(new DefaultItemAnimator());
        seqroutesAdapter = new SeqroutesAdapter(ShippingActivity.this, seqroutesList);
        rv_seqroutes.setAdapter(seqroutesAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

        seqRoutesService = new SeqRoutesService();
        seqRoutesService.getAllData(ShippingActivity.this, txtGoodWeights, txtBadWeights);

        //get set data shipping
        shippingList.clear();
        shippingAdapter.notifyDataSetChanged();
        shippingService.getShipping(ShippingActivity.this, shippingList, shippingAdapter);

        //get set data seq routes
        seqroutesList.clear();
        seqroutesAdapter.notifyDataSetChanged();
        shippingService.getSeqRoutes(ShippingActivity.this, seqroutesList, seqroutesAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        onStart();
        if (distanceResultFind != null){
            if (alertDialog != null && globalValueTxtDistance != null){
                globalValueTxtDistance.setText(MyConverting.meterToKM(Integer.valueOf(distanceResultFind)));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_shipping, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
            case R.id.action_combination:
                popupCombination();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void popupCombination() {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(ShippingActivity.this);
        LayoutInflater inflater = (LayoutInflater) ShippingActivity.this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_shipping_combination, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        TextView txtBeforeShipping = (TextView)dialogView.findViewById(R.id.txtBeforeShipping);
        TextView txtBeforeDistance = (TextView)dialogView.findViewById(R.id.txtBeforeDistance);
        TextView txtAfterShipping = (TextView)dialogView.findViewById(R.id.txtAfterShipping);
        final TextView txtAfterDistance = (TextView)dialogView.findViewById(R.id.txtAfterDistance);
        final TextView txtCombinationCustID = (TextView)dialogView.findViewById(R.id.txtCombinationCustID);
        TextView txtCombinationWeight = (TextView)dialogView.findViewById(R.id.txtCombinationWeight);
        TextView txtCombinationProfit = (TextView)dialogView.findViewById(R.id.txtCombinationProfit);
        TextView txtFindDistance = (TextView)dialogView.findViewById(R.id.txtFindDistance);
        Button btnCombineOke = (Button)dialogView.findViewById(R.id.btnCombineOke);
        Button btnCombineCancel = (Button)dialogView.findViewById(R.id.btnCombineCancel);

        globalValueTxtDistance = txtAfterDistance;

        shippingService.getCombination(ShippingActivity.this, txtBeforeShipping, txtAfterShipping, txtBeforeDistance,
                txtAfterDistance, txtCombinationCustID, txtCombinationWeight, txtCombinationProfit, txtFindDistance);

        if (txtCombinationCustID.getText().toString().equals(getString(R.string.nodata))){
            Toasty.warning(ShippingActivity.this,"Tidak ada data yang dapat dikombinasikan.", Toast.LENGTH_SHORT).show();
            alertDialog.dismiss();
        }

        btnCombineOke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!txtCombinationCustID.getText().toString().equals(getString(R.string.nodata))){
                    shippingService.combineShipping(ShippingActivity.this, txtCombinationCustID.getText().toString(), alertDialog);
                    Intent op = new Intent(ShippingActivity.this, ShippingActivity.class);
                    startActivity(op);
                    finish();
                    //onStart();
                }else {
                    Toasty.warning(ShippingActivity.this,"Tidak ada data yang dapat dikombinasikan.", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                }
            }
        });

        btnCombineCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }


}
