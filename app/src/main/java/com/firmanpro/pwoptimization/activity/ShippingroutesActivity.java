package com.firmanpro.pwoptimization.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.service.ShippingroutesService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import es.dmoral.toasty.Toasty;

public class ShippingroutesActivity extends FragmentActivity implements OnMapReadyCallback {

    private Intent intent;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private String shippingIDExtra = "";

    private GoogleMap mMap;

    private TextView txtLogAllPath, txtLogBestPath, txtLogBestFinal;
    private Button btnResetPath, btnAllPath, btnBestPath;
    private ScrollView scLogAllPath, scLogBestPath;
    private ProgressBar loadingBars;

    private ShippingroutesService shippingroutesService = new ShippingroutesService();

    private boolean isMenuShipping = true;

    private boolean modeFindDistance = false;
    private String shippingIDForFindDistance = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shippingroutes);

        MySession.beginInitialization(ShippingroutesActivity.this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_shipping);
        mapFragment.getMapAsync(this);

        loadingBars = (ProgressBar)findViewById(R.id.loadingBars);
        txtLogAllPath = (TextView)findViewById(R.id.txtLogAllPath);
        txtLogBestPath = (TextView)findViewById(R.id.txtLogBestPath);
        txtLogBestFinal = (TextView)findViewById(R.id.txtLogBestFinal);
        btnResetPath = (Button)findViewById(R.id.btnResetPath);
        btnAllPath = (Button)findViewById(R.id.btnAllPath);
        btnBestPath = (Button)findViewById(R.id.btnBestPath);
        scLogAllPath = (ScrollView)findViewById(R.id.scLogAllPath);
        scLogBestPath = (ScrollView)findViewById(R.id.scLogBestPath);

    }

    @Override
    protected void onStart() {
        super.onStart();

        shippingroutesService.theConstructors(loadingBars, scLogAllPath, scLogBestPath,
                txtLogAllPath, txtLogBestPath, btnResetPath, btnAllPath, btnBestPath, txtLogBestFinal);

        intent = getIntent();
        if (intent.getStringExtra(enumProHelper.keyModeFindDistance) == null) {
            modeFindDistance = false;
            if (intent.getStringExtra(enumProHelper.keyShippingID) != null) {
                shippingIDExtra = intent.getStringExtra(enumProHelper.keyShippingID);
                Toasty.info(getApplicationContext(), "ID SHIPPING : " + shippingIDExtra, Toast.LENGTH_SHORT, true).show();
                if (intent.getStringExtra(enumProHelper.keyIsMenuShipping) != null) {
                    if (intent.getStringExtra(enumProHelper.keyIsMenuShipping).equals(enumProHelper.keyNo)) {
                        isMenuShipping = false;
                        Toasty.info(getApplicationContext(), "Generate Shortest Routes", Toast.LENGTH_SHORT, true).show();
                    }
                }
            } else {
                Toasty.error(getApplicationContext(), "Maaf, parameter tidak diketahui.", Toast.LENGTH_SHORT, true).show();
                finish();
            }
        }else {
            if (intent.getStringExtra(enumProHelper.keyShippingIDFindDistance) != null) {
                modeFindDistance = true;
                shippingIDForFindDistance = intent.getStringExtra(enumProHelper.keyShippingIDFindDistance);
                Toasty.info(getApplicationContext(),"Pencarian CustID : " + shippingIDForFindDistance, Toast.LENGTH_SHORT).show();
            }
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        shippingroutesService.getCustomersLocation(ShippingroutesActivity.this, shippingIDExtra, mMap, isMenuShipping, modeFindDistance, shippingIDForFindDistance);
        shippingroutesService.clickButton(ShippingroutesActivity.this, btnResetPath, btnAllPath, btnBestPath, mMap, isMenuShipping, modeFindDistance);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
