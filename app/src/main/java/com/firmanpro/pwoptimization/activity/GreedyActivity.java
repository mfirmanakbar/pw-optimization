package com.firmanpro.pwoptimization.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.adapter.GreedyAdapter;
import com.firmanpro.pwoptimization.centerformula.KnapsackGreedy;
import com.firmanpro.pwoptimization.entity.GreedyEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.service.GreedyService;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class GreedyActivity extends AppCompatActivity {

    private final String TAG = "gre_act";
    private GreedyService greedyService = new GreedyService();
    private EnumProHelper enumProHelper = new EnumProHelper();
    private KnapsackGreedy knapsackGreedy = new KnapsackGreedy();
    private String modeGreedy, custID;
    private Intent intent;

    private RecyclerView rv_greedy;
    private RecyclerView.LayoutManager lm_greedy;
    private List<GreedyEntity> greedyList = new ArrayList<GreedyEntity>();
    private GreedyAdapter greedyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greedy);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        /**inisialisasi RecyclerView*/
        rv_greedy = (RecyclerView)findViewById(R.id.rv_greedy);
        lm_greedy = new GridLayoutManager(this, 1); /**kolom tampilan item*/
        rv_greedy.setLayoutManager(lm_greedy);
        rv_greedy.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    protected void onStart() {
        super.onStart();

        /**get data extra intent*/
        modeGreedy = "";
        intent = getIntent();
        if (intent.getStringExtra(enumProHelper.keyModeGreedy) != null
                && intent.getStringExtra(enumProHelper.keyCustID) != null){
            modeGreedy = intent.getStringExtra(enumProHelper.keyModeGreedy);
            custID = intent.getStringExtra(enumProHelper.keyCustID);
            knapsackGreedy.modeGreedy = modeGreedy;

            Log.d(TAG, modeGreedy);
            Log.d(TAG, custID);
        }else {
            Toasty.error(getApplicationContext(),"Periksa kembali parameter : Mode Greedy dan Customer ID.", Toast.LENGTH_SHORT).show();
        }

        /**set data adapter RecyclerView*/
        greedyAdapter = new GreedyAdapter(GreedyActivity.this, greedyList, modeGreedy);
        rv_greedy.setAdapter(greedyAdapter);

        if (!modeGreedy.trim().equals("") && !custID.equals("")){
            /**set toolbar title*/
            if (modeGreedy.equals(enumProHelper.greedyByProfit)) {
                setCustomTitle("Max Profit");
            }else if (modeGreedy.equals(enumProHelper.greedyByWeight)) {
                setCustomTitle("Min Weight");
            }else if (modeGreedy.equals(enumProHelper.greedyByDensity)) {
                setCustomTitle("Max Density");
            }
            greedyService.loadGreedyByMode(GreedyActivity.this, greedyList, greedyAdapter, modeGreedy, custID);
        }

    }

    public void setCustomTitle(String title) {
        GreedyActivity.this.getSupportActionBar().setTitle("Sort by " + title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
