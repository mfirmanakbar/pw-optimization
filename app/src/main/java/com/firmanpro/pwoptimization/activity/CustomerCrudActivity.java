package com.firmanpro.pwoptimization.activity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.service.CustomerService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class CustomerCrudActivity extends FragmentActivity implements OnMapReadyCallback {

    private final String TAG = "cusCrud";
    private GoogleMap mMap;
    private Button btnCusCrudSaveOrUpdate, btnCusCrudDelete;
    private Intent intent;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private CustomerService customerService = new CustomerService();
    private Marker markerName = null;
    private TextView txtCusCrudProvince;
    private boolean isAddNewCustomer = false;
    private String getValCustomersID = null, getValCustomersProvince = null;

    private Geocoder geocoder = null;
    private List<Address> addresses = new ArrayList<>();
    private String addressLine = "";
    private String[] addressLineSplit = null;
    private View markerIcon;

    private LinearLayout hs_1;
    private ImageView hs_2;
    private TextView hs_3;

    private String finalProvince = "";
    private double finalLat = 0.0;
    private double finalLng = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_crud);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_customer_crud);
        mapFragment.getMapAsync(this);

        hs_1 = (LinearLayout)findViewById(R.id.hs_1);
        hs_2 = (ImageView) findViewById(R.id.hs_2);
        hs_3 = (TextView) findViewById(R.id.hs_3);

        btnCusCrudSaveOrUpdate = (Button) findViewById(R.id.btnCusCrudSaveOrUpdate);
        btnCusCrudDelete = (Button) findViewById(R.id.btnCusCrudDelete);
        txtCusCrudProvince = (TextView) findViewById(R.id.txtCusCrudProvince);
        if (txtCusCrudProvince.getVisibility()==View.VISIBLE){
            txtCusCrudProvince.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        intent = getIntent();
        if (intent.getStringExtra(enumProHelper.keyIsAddNewCustomer) != null){
            if (intent.getStringExtra(enumProHelper.keyIsAddNewCustomer).equals("true")){
                getValCustomersID = null;
                getValCustomersProvince = null;
                btnCusCrudSaveOrUpdate.setText("Save");
                isAddNewCustomer = true;
                hs_1.setVisibility(View.GONE);
                hs_2.setVisibility(View.GONE);
                hs_3.setVisibility(View.GONE);

                if (btnCusCrudDelete.getVisibility()== View.VISIBLE)
                    btnCusCrudDelete.setVisibility(View.GONE);
            }else {
                getValCustomersID = intent.getStringExtra(enumProHelper.keyCustomersID);
                getValCustomersProvince = intent.getStringExtra(enumProHelper.keyCustomersProvince);

                isAddNewCustomer = false;
                hs_1.setVisibility(View.VISIBLE);
                hs_2.setVisibility(View.VISIBLE);
                hs_3.setVisibility(View.VISIBLE);
                btnCusCrudSaveOrUpdate.setText("Update");

                /*if (getValCustomersProvince != null) {
                    txtCusCrudProvince.setVisibility(View.VISIBLE);
                    txtCusCrudProvince.setText(getValCustomersProvince);
                }*/

                if (btnCusCrudDelete.getVisibility() == View.GONE)
                    btnCusCrudDelete.setVisibility(View.VISIBLE);
            }
        }else {
            finish();
        }
        btnCusCrudSaveOrUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveOrUpdate();
            }
        });
    }

    private void saveOrUpdate() {
        if (!isAddNewCustomer){
            /**update data*/
            if (txtCusCrudProvince.getVisibility()==View.VISIBLE) {
                if (!txtCusCrudProvince.equals(getString(R.string.provinsi_tidak_ditemukan))) {
                    finalProvince = txtCusCrudProvince.getText().toString();
                    customerService.updateCustomer(CustomerCrudActivity.this, getValCustomersID, finalProvince, finalLat, finalLng);
                    finish();
                } else {
                    Toasty.error(getApplicationContext(), getString(R.string.provinsi_tidak_ditemukan), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        customerService.getCustomersMaps(CustomerCrudActivity.this, mMap, getValCustomersID);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                if (txtCusCrudProvince.getVisibility()==View.GONE){
                    txtCusCrudProvince.setVisibility(View.VISIBLE);
                }
                if (markerName!=null){
                    markerName.remove();
                }
                markerName = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                getAddress(latLng.latitude, latLng.longitude);
            }
        });

    }

    private void getAddress(double latitude, double longitude) {
        finalLat = latitude;
        finalLng = longitude;
        Log.d("LXLX", "Lat: " + String.valueOf(latitude) + ". Lng: " + String.valueOf(longitude));

        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if(addresses != null) {
                addressLine = addresses.get(0).getAddressLine(0);
                Log.d(TAG, addressLine);
                addressLineSplit = addressLine.toString().trim().split(",");
                if (addressLineSplit.length > 3){
                    txtCusCrudProvince.setText(addressLineSplit[3]);
                }
            }
            else{
                txtCusCrudProvince.setText(getString(R.string.provinsi_tidak_ditemukan));
            }
        } catch (IOException e) {
            e.printStackTrace();
            txtCusCrudProvince.setText(getString(R.string.provinsi_tidak_ditemukan));
        }


    }


}
