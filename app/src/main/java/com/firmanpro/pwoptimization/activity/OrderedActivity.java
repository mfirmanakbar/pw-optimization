package com.firmanpro.pwoptimization.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.adapter.OrderedAdapter;
import com.firmanpro.pwoptimization.entity.OrderedEntity;
import com.firmanpro.pwoptimization.service.OrderedService;

import java.util.ArrayList;
import java.util.List;

public class OrderedActivity extends AppCompatActivity {

    private OrderedService orderedService = new OrderedService();
    private String TAG = "ord_act";

    private RecyclerView rv_ordered;
    private RecyclerView.LayoutManager lm_ordered;
    private List<OrderedEntity> orderList = new ArrayList<OrderedEntity>();
    private OrderedAdapter orderedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordered);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        /**inisialisasi RecyclerView*/
        rv_ordered = (RecyclerView)findViewById(R.id.rv_ordered);
        lm_ordered = new GridLayoutManager(this, 2); /**kolom tampilan item*/
        rv_ordered.setLayoutManager(lm_ordered);
        rv_ordered.setItemAnimator(new DefaultItemAnimator());
        orderedAdapter = new OrderedAdapter(OrderedActivity.this, orderList);
        rv_ordered.setAdapter(orderedAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        orderedService.getOrdered(OrderedActivity.this, orderList, orderedAdapter);
        /*if (!orderedService.getOrdered(OrderedActivity.this, orderList, orderedAdapter))
            finish();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
