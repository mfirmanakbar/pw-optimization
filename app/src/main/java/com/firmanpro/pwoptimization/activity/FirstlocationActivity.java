package com.firmanpro.pwoptimization.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.helper.MySession;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import es.dmoral.toasty.Toasty;

public class FirstlocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Double my_latitude, my_longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstlocation);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_first_location);
        mapFragment.getMapAsync(this);

        MySession.beginInitialization(FirstlocationActivity.this);


    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        if (MySession.getSessionGlobal(MySession.key_session_warehouse_lat) != null
                && MySession.getSessionGlobal(MySession.key_session_warehouse_lng) != null
                && !MySession.getSessionGlobal(MySession.key_session_warehouse_lat).equals("")
                && !MySession.getSessionGlobal(MySession.key_session_warehouse_lng).equals("")){
            LatLng locationBefore = new LatLng(
                    Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lat)),
                    Double.valueOf(MySession.getSessionGlobal(MySession.key_session_warehouse_lng)));
            mMap.addMarker(new MarkerOptions()
                    .position(locationBefore)
                    .title("First Location of Shipping"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationBefore, 15.0f));
        }

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng point) {
                mMap.clear();
                mMap = googleMap;
                mMap.addMarker(new MarkerOptions()
                        .position(point)
                        .title("First Location of Shipping")
                        .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 15.0f));
                my_latitude = point.latitude;
                my_longitude = point.longitude;
                MySession.clearSessionGlobal(MySession.key_session_warehouse_lat);
                MySession.clearSessionGlobal(MySession.key_session_warehouse_lng);
                MySession.setSessionGlobal(MySession.key_session_warehouse_lat, String.valueOf(my_latitude));
                MySession.setSessionGlobal(MySession.key_session_warehouse_lng, String.valueOf(my_longitude));
                Toasty.success(getApplicationContext(),"Lokasi awal berhasil disimpan.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}
