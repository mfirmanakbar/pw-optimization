package com.firmanpro.pwoptimization.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.service.AdminService;

import es.dmoral.toasty.Toasty;

public class AdminActivity extends AppCompatActivity {

    private Button btnAdminParamter, btnAdminImportOfflineA, btnAdminImportOfflineB, btnAdminImportDataServer,
            btnAdminDeleteData, btnAdminResetStatusOrdered, btnAdminResetStatusSeqRoutes, btnAdminResetStatusShipping;
    private Intent intent;
    private AdminService adminService = new AdminService();
    private RelativeLayout reLay_loading;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        reLay_loading = (RelativeLayout)findViewById(R.id.reLay_loading);
        btnAdminParamter = (Button)findViewById(R.id.btnAdminParamter);
        btnAdminImportOfflineA = (Button)findViewById(R.id.btnAdminImportOfflineA);
        btnAdminImportOfflineB = (Button)findViewById(R.id.btnAdminImportOfflineB);
        btnAdminImportDataServer = (Button)findViewById(R.id.btnAdminImportDataServer);
        btnAdminDeleteData = (Button)findViewById(R.id.btnAdminDeleteData);
        btnAdminResetStatusOrdered = (Button)findViewById(R.id.btnAdminResetStatusOrdered);
        btnAdminResetStatusSeqRoutes = (Button)findViewById(R.id.btnAdminResetStatusSeqRoutes);
        btnAdminResetStatusShipping = (Button)findViewById(R.id.btnAdminResetStatusShipping);

        MySession.beginInitialization(AdminActivity.this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        btnAdminParamter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(AdminActivity.this, ParameterActivity.class);
                startActivity(intent);
            }
        });

        btnAdminImportOfflineA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminService.resetToImportDummy(AdminActivity.this, reLay_loading, 1);
            }
        });

        btnAdminImportOfflineB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminService.resetToImportDummy(AdminActivity.this, reLay_loading, 2);
            }
        });

        btnAdminImportDataServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MySession.getSessionGlobal(MySession.key_session_api_customer_import)==null){
                    Toasty.error(getApplicationContext(),"Customer - Parameter Link belum diisi", Toast.LENGTH_SHORT).show();
                }else if (MySession.getSessionGlobal(MySession.key_session_api_ordered_import)==null){
                    Toasty.error(getApplicationContext(),"Ordered - Parameter Link belum diisi", Toast.LENGTH_SHORT).show();
                }else {
                    adminService.resetToImportServer(AdminActivity.this, reLay_loading);
                }
            }
        });

        btnAdminDeleteData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupDeleteDatabase();
            }
        });

        btnAdminResetStatusOrdered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupResetStatus("Confirm", "Reset All Ordered Status ?", 1);
            }
        });

        btnAdminResetStatusSeqRoutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupResetStatus("Confirm", "Reset All Sequence Routes Status ?", 2);
            }
        });

        btnAdminResetStatusShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupResetStatus("Confirm", "Reset All Shipping Status ?", 3);
            }
        });

    }

    private void popupResetStatus(String title, String msg, final int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminActivity.this, R.style.MyDialogTheme);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (type == 1) {
                    adminService.resetStatusOrdered(AdminActivity.this);
                }else if (type == 2) {
                    adminService.resetStatusSeqRoutes(AdminActivity.this);
                }else if (type == 3) {
                    adminService.resetStatusShipping(AdminActivity.this);
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void popupDeleteDatabase() {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminActivity.this);
        LayoutInflater inflater = (LayoutInflater) AdminActivity.this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_delete_database, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        final CheckBox cbPopupDeleteDBShipping = (CheckBox) dialogView.findViewById(R.id.cbPopupDeleteDBShipping);
        final CheckBox cbPopupDeleteDBRoutes = (CheckBox) dialogView.findViewById(R.id.cbPopupDeleteDBRoutes);
        final CheckBox cbPopupDeleteDBLegs = (CheckBox) dialogView.findViewById(R.id.cbPopupDeleteDBLegs);
        Button btnPopupDeleteDBCancel = (Button)dialogView.findViewById(R.id.btnPopupDeleteDBCancel);
        Button btnPopupDeleteDBYes = (Button)dialogView.findViewById(R.id.btnPopupDeleteDBYes);

        btnPopupDeleteDBYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adminService.deleteDatabase(AdminActivity.this, reLay_loading,
                        cbPopupDeleteDBShipping.isChecked(),
                        cbPopupDeleteDBRoutes.isChecked(),
                        cbPopupDeleteDBLegs.isChecked()
                );
                alertDialog.dismiss();
            }
        });
        btnPopupDeleteDBCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
