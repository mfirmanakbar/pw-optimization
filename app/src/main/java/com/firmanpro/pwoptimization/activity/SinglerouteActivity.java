package com.firmanpro.pwoptimization.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.adapter.CustomerAdapter;
import com.firmanpro.pwoptimization.entity.CustomerEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.service.CustomerService;
import com.firmanpro.pwoptimization.service.SinglerouteService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class SinglerouteActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private CustomerService customerService = new CustomerService();
    private LinearLayout linLaySRDepartue, linLaySRDestination;
    private AlertDialog alertDialog;
    private TextView txtPopupSRdeparture, txtPopupSRdestination, txtPopupSRDistance, txtPopupSRDuration, txtSrLog, txtSrTdis, txtSrTdur;
    private String cust_origin, cust_dest;
    public static LatLng originX, destX;
    private Button btnPopupSRfindRoute;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private ScrollView scSrLog;

    private RecyclerView rv_customer;
    private RecyclerView.LayoutManager lm_customer;
    private List<CustomerEntity> customerLists = new ArrayList<CustomerEntity>();
    private CustomerAdapter customerAdapter;
    private SinglerouteService singlerouteService = new SinglerouteService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singleroute);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_singleroute);
        mapFragment.getMapAsync(this);

        linLaySRDepartue = (LinearLayout)findViewById(R.id.linLaySRDepartue);
        linLaySRDestination = (LinearLayout)findViewById(R.id.linLaySRDestination);
        txtPopupSRdeparture = (TextView)findViewById(R.id.txtPopupSRdeparture);
        txtPopupSRdestination = (TextView)findViewById(R.id.txtPopupSRdestination);
        txtPopupSRDistance = (TextView)findViewById(R.id.txtPopupSRDistance);
        txtPopupSRDuration = (TextView)findViewById(R.id.txtPopupSRDuration);
        btnPopupSRfindRoute = (Button)findViewById(R.id.btnPopupSRfindRoute);
        txtSrLog = (TextView)findViewById(R.id.txtSrLog);
        txtSrTdis = (TextView)findViewById(R.id.txtSrTdis);
        txtSrTdur = (TextView)findViewById(R.id.txtSrTdur);
        scSrLog = (ScrollView)findViewById(R.id.scSrLog);

    }

    @Override
    protected void onStart() {
        super.onStart();

        linLaySRDepartue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupCustomerList(true, txtPopupSRdeparture);
            }
        });
        linLaySRDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupCustomerList(false, txtPopupSRdestination);
            }
        });
        btnPopupSRfindRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMap != null){
                    cust_origin = txtPopupSRdeparture.getText().toString();
                    cust_dest = txtPopupSRdestination.getText().toString();
                    if (cust_origin.length() > 1 && cust_dest.length() > 1){
                        if (!cust_origin.equals(cust_dest)){
                            singlerouteService.clickFindRoutes(SinglerouteActivity.this, mMap, cust_origin, cust_dest,
                                    txtPopupSRDistance, txtPopupSRDuration, txtSrLog, txtSrTdis, txtSrTdur, scSrLog);
                        }else {
                            Toasty.warning(getApplicationContext(),"Customers tidak valid", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toasty.warning(getApplicationContext(),"Mohon pilih customers", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void popupCustomerList(boolean isThatDeparture, TextView txtView) {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(SinglerouteActivity.this);
        LayoutInflater inflater = (LayoutInflater) SinglerouteActivity.this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_single_route, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        Button btnPopupSRClose = (Button)dialogView.findViewById(R.id.btnPopupSRClose);
        TextView txtPopupSRLatLngGudang = (TextView)dialogView.findViewById(R.id.txtPopupSRLatLngGudang);

        /**inisialisasi RecyclerView*/
        rv_customer = (RecyclerView)dialogView.findViewById(R.id.rv_customer);
        lm_customer = new GridLayoutManager(this, 1); /**kolom tampilan item*/
        rv_customer.setLayoutManager(lm_customer);
        rv_customer.setItemAnimator(new DefaultItemAnimator());
        customerAdapter = new CustomerAdapter(SinglerouteActivity.this, customerLists,
                true, txtView, alertDialog);
        rv_customer.setAdapter(customerAdapter);

        customerService.getCustomers(SinglerouteActivity.this, customerLists, customerAdapter);

        txtPopupSRLatLngGudang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtPopupSRdeparture.setText(enumProHelper.keyGudang);
                alertDialog.dismiss();
            }
        });

        btnPopupSRClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        customerService.getCustomersMaps(SinglerouteActivity.this, mMap, null);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    }
}
