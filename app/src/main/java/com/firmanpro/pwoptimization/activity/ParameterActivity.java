package com.firmanpro.pwoptimization.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.config.ApiAddress;
import com.firmanpro.pwoptimization.helper.GuiHelper;
import com.firmanpro.pwoptimization.helper.MySession;

import es.dmoral.toasty.Toasty;

public class ParameterActivity extends AppCompatActivity {

    private EditText txtParamLinkCustomer, txtParamLinkOrdered, txtParamMaxCapacity, txtParamFirstLat, txtParamFirstLng;
    private Button btnParamUpdate, btnOpenMaps, btnWarehouseA, btnWarehouseB;
    private GuiHelper guiHelper = new GuiHelper();
    private String maxCapacityTruck, warehouseALat, warehouseBLat, warehouseALng, warehouseBLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parameter);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        MySession.beginInitialization(ParameterActivity.this);
        guiHelper.hideMyKeyBoard(getWindow());

        txtParamLinkCustomer = (EditText)findViewById(R.id.txtParamLinkCustomer);
        txtParamLinkOrdered = (EditText)findViewById(R.id.txtParamLinkOrdered);
        txtParamMaxCapacity = (EditText)findViewById(R.id.txtParamMaxCapacity);
        txtParamFirstLat = (EditText)findViewById(R.id.txtParamFirstLat);
        txtParamFirstLng = (EditText)findViewById(R.id.txtParamFirstLng);
        btnParamUpdate = (Button)findViewById(R.id.btnParamUpdate);
        btnOpenMaps = (Button)findViewById(R.id.btnOpenMaps);
        btnWarehouseA = (Button)findViewById(R.id.btnWarehouseA);
        btnWarehouseB = (Button)findViewById(R.id.btnWarehouseB);

        if (MySession.getSessionGlobal(MySession.key_session_api_customer_import)==null)
            MySession.setSessionGlobal(MySession.key_session_api_customer_import, ApiAddress.api_import_customers);

        if (MySession.getSessionGlobal(MySession.key_session_api_ordered_import)==null)
            MySession.setSessionGlobal(MySession.key_session_api_ordered_import, ApiAddress.api_import_ordered);

        txtParamLinkCustomer.setText(MySession.getSessionGlobal(MySession.key_session_api_customer_import));
        txtParamLinkOrdered.setText(MySession.getSessionGlobal(MySession.key_session_api_ordered_import));

        warehouseALat = "-6.046250729029517";
        warehouseALng = "106.54679283499718";
        warehouseBLat = "-6.739981672402339";
        warehouseBLng = "106.57735124230385";

    }

    @Override
    protected void onStart() {
        super.onStart();

        btnWarehouseA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySession.setSessionGlobal(MySession.key_session_warehouse_lat, warehouseALat);
                MySession.setSessionGlobal(MySession.key_session_warehouse_lng, warehouseALng);
                setLatLngInTextView();
            }
        });

        btnWarehouseB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySession.setSessionGlobal(MySession.key_session_warehouse_lat, warehouseBLat);
                MySession.setSessionGlobal(MySession.key_session_warehouse_lng, warehouseBLng);
                setLatLngInTextView();
            }
        });

        //Log.d("teslatng1", MySession.getSessionGlobal(MySession.key_session_warehouse_lat));
        //Log.d("teslatng2", MySession.getSessionGlobal(MySession.key_session_warehouse_lng));

        if (MySession.getSessionGlobal(MySession.key_session_warehouse_lat) == null
                && MySession.getSessionGlobal(MySession.key_session_warehouse_lng) == null){
            /**custome default lat lng warehouse*/
            MySession.setSessionGlobal(MySession.key_session_warehouse_lat, "-6.046250729029517");
            MySession.setSessionGlobal(MySession.key_session_warehouse_lng, "106.54679283499718");
        }

        setLatLngInTextView();

        maxCapacityTruck = MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity);
        txtParamMaxCapacity.setText(maxCapacityTruck);
        btnParamUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySession.clearSessionGlobal(MySession.key_session_truck_maxcapacity);
                MySession.clearSessionGlobal(MySession.key_session_api_customer_import);
                MySession.clearSessionGlobal(MySession.key_session_api_ordered_import);
                MySession.clearSessionGlobal(MySession.key_session_warehouse_lat);
                MySession.clearSessionGlobal(MySession.key_session_warehouse_lng);
                MySession.setSessionGlobal(MySession.key_session_truck_maxcapacity,txtParamMaxCapacity.getText().toString());
                MySession.setSessionGlobal(MySession.key_session_api_customer_import,txtParamLinkCustomer.getText().toString());
                MySession.setSessionGlobal(MySession.key_session_api_ordered_import,txtParamLinkOrdered.getText().toString());
                MySession.setSessionGlobal(MySession.key_session_warehouse_lat,txtParamFirstLat.getText().toString());
                MySession.setSessionGlobal(MySession.key_session_warehouse_lng,txtParamFirstLng.getText().toString());

                Toasty.success(getApplicationContext(), "Paramter berhasil diubah.", Toast.LENGTH_SHORT, true).show();
                finish();
            }
        });
        btnOpenMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ParameterActivity.this, FirstlocationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setLatLngInTextView() {
        if (MySession.getSessionGlobal(MySession.key_session_warehouse_lat) != null)
            txtParamFirstLat.setText(MySession.getSessionGlobal(MySession.key_session_warehouse_lat));

        if (MySession.getSessionGlobal(MySession.key_session_warehouse_lng) != null)
            txtParamFirstLng.setText(MySession.getSessionGlobal(MySession.key_session_warehouse_lng));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
