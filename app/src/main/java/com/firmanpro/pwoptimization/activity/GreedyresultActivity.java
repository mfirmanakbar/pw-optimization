package com.firmanpro.pwoptimization.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.adapter.GreedyresultAdapter;
import com.firmanpro.pwoptimization.entity.GreedyresultEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.service.GreedyresultService;

import java.util.ArrayList;
import java.util.List;

public class GreedyresultActivity extends AppCompatActivity {

    private GreedyresultService greedyresultService = new GreedyresultService();
    private TextView txtTotalWeight1, txtTotalWeight2, txtTotalWeight3, txtTotalProfit1,
            txtTotalProfit2, txtTotalProfit3, txtTotalItem1, txtTotalItem2, txtTotalItem3;
    private EnumProHelper enumProHelper = new EnumProHelper();

    private RecyclerView rv_greedyresult;
    private RecyclerView.LayoutManager lm_greedyresult;
    private List<GreedyresultEntity> greedyresultList = new ArrayList<GreedyresultEntity>();
    private GreedyresultAdapter greedyresultAdapter;

    private AlertDialog alertDialog;

    private Intent intent;
    //private int optimizationType = 0;

    private String custID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greedyresult);

        /**enabling back*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        txtTotalWeight1 = (TextView)findViewById(R.id.txtTotalWeight1);
        txtTotalWeight2 = (TextView)findViewById(R.id.txtTotalWeight2);
        txtTotalWeight3 = (TextView)findViewById(R.id.txtTotalWeight3);
        txtTotalProfit1 = (TextView)findViewById(R.id.txtTotalProfit1);
        txtTotalProfit2 = (TextView)findViewById(R.id.txtTotalProfit2);
        txtTotalProfit3 = (TextView)findViewById(R.id.txtTotalProfit3);
        txtTotalItem1 = (TextView)findViewById(R.id.txtTotalItem1);
        txtTotalItem2 = (TextView)findViewById(R.id.txtTotalItem2);
        txtTotalItem3 = (TextView)findViewById(R.id.txtTotalItem3);

        rv_greedyresult = (RecyclerView)findViewById(R.id.rv_greedyresult);
        lm_greedyresult = new GridLayoutManager(this, 1); /**kolom tampilan item*/
        rv_greedyresult.setLayoutManager(lm_greedyresult);
        rv_greedyresult.setItemAnimator(new DefaultItemAnimator());
        greedyresultAdapter = new GreedyresultAdapter(GreedyresultActivity.this, greedyresultList);
        rv_greedyresult.setAdapter(greedyresultAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

        intent = getIntent();
        if (intent.getStringExtra(enumProHelper.keyCustID) != null){
            custID = intent.getStringExtra(enumProHelper.keyCustID);
            greedyresultService.loadDataDelivery(GreedyresultActivity.this, greedyresultList, greedyresultAdapter,
                    txtTotalWeight1, txtTotalWeight2, txtTotalWeight3, txtTotalProfit1, txtTotalProfit2, txtTotalProfit3,
                    custID, txtTotalItem1, txtTotalItem2, txtTotalItem3);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_greedyresult, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_shipping:
                popupListShipping();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void popupListShipping() {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(GreedyresultActivity.this);
        LayoutInflater inflater = (LayoutInflater) GreedyresultActivity.this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_type_greedy, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        Button btnTypeResultProfit = (Button)dialogView.findViewById(R.id.btnTypeResultProfit);
        Button btnTypeResultWeight = (Button)dialogView.findViewById(R.id.btnTypeResultWeight);
        Button btnTypeResultDensity = (Button)dialogView.findViewById(R.id.btnTypeResultDensity);
        Button btnTypeResultClose = (Button)dialogView.findViewById(R.id.btnTypeResultClose);

        btnTypeResultProfit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                greedyresultService.saveShipping(GreedyresultActivity.this, enumProHelper.greedyByProfit);
                alertDialog.dismiss();
                finish();
            }
        });
        btnTypeResultWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                greedyresultService.saveShipping(GreedyresultActivity.this, enumProHelper.greedyByWeight);
                alertDialog.dismiss();
                finish();
            }
        });
        btnTypeResultDensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                greedyresultService.saveShipping(GreedyresultActivity.this, enumProHelper.greedyByDensity);
                alertDialog.dismiss();
                finish();
            }
        });
        btnTypeResultClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }

}
