package com.firmanpro.pwoptimization.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.adapter.CustomerAdapter;
import com.firmanpro.pwoptimization.entity.CustomerEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.service.CustomerService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class CustomerActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private String TAG = "cust_act";
    private CustomerService customerService = new CustomerService();

    private RecyclerView rv_customer;
    private RecyclerView.LayoutManager lm_customer;
    private List<CustomerEntity> customerLists = new ArrayList<CustomerEntity>();
    private CustomerAdapter customerAdapter;
    private LatLng currentlocation;

    private Button btnCustAdd;

    private Intent intent;
    private EnumProHelper enumProHelper = new EnumProHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_customer);
        mapFragment.getMapAsync(this);

        btnCustAdd = (Button)findViewById(R.id.btnCustAdd);

        /**inisialisasi RecyclerView*/
        rv_customer = (RecyclerView)findViewById(R.id.rv_customer);
        lm_customer = new GridLayoutManager(this, 1); /**kolom tampilan item*/
        rv_customer.setLayoutManager(lm_customer);
        rv_customer.setItemAnimator(new DefaultItemAnimator());
        customerAdapter = new CustomerAdapter(CustomerActivity.this, customerLists,
                false, null, null);
        rv_customer.setAdapter(customerAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        btnCustAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(CustomerActivity.this, CustomerCrudActivity.class);
                intent.putExtra(enumProHelper.keyIsAddNewCustomer,"true");
                startActivity(intent);
            }
        });
        customerService.getCustomers(CustomerActivity.this, customerLists, customerAdapter);
        if (mMap != null) {
            onMapReady(mMap);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        customerService.getCustomers(CustomerActivity.this, customerLists, customerAdapter);
        if (mMap != null) {
            onMapReady(mMap);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        customerService.getCustomersMaps(CustomerActivity.this, mMap, null);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

}
