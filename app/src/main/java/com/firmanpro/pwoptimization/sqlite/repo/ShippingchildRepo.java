package com.firmanpro.pwoptimization.sqlite.repo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.sqlite.DBHelper;
import com.firmanpro.pwoptimization.sqlite.table.CustomerTable;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingChildTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 11/2/17.
 */

public class ShippingchildRepo {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private ContentValues values;
    private long general_Id;
    private Cursor cursor;
    private String selectQuery;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private ArrayList<String> ar_orderid = new ArrayList<>();
    private EnumProHelper enumProHelper = new EnumProHelper();

    public ShippingchildRepo(Context context) { this.dbHelper = new DBHelper(context); }

    public int insert(ShippingChildTable shippingChildTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(ShippingChildTable.key_shippingID, shippingChildTable.shippingID);
        values.put(ShippingChildTable.key_orderID, shippingChildTable.orderID);
        /**Inserting Row*/
        general_Id = db.insert(ShippingChildTable.key_tb_shippingchild, null, values);
        db.close();
        return (int) general_Id;
    }

    public ArrayList<HashMap<String, String>> getAllData(String shippingIDExtra) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + ShippingChildTable.key_tb_shippingchild;
        if (shippingIDExtra!=null){
            selectQuery =  "SELECT a.shippingChildID, a.shippingID, a.orderID, " +
                    "b.productName, b.productStatus, b.customerID, b.productWeight, b.productProfit, " +
                    "c.customerProvince, c.customerLat, c.customerLng " +
                    "FROM (shippingChildTable a INNER JOIN orderedTable b ON a.orderID = b.orderID) " +
                    "INNER JOIN customerTable c ON b.customerID = c.customerID " +
                    "WHERE " + ShippingChildTable.key_shippingID + " = '" + shippingIDExtra + "'";
        }
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(ShippingChildTable.key_shippingChildID, cursor.getString(cursor.getColumnIndex(ShippingChildTable.key_shippingChildID)));
                gen.put(ShippingChildTable.key_shippingID, cursor.getString(cursor.getColumnIndex(ShippingChildTable.key_shippingID)));
                gen.put(ShippingChildTable.key_orderID, cursor.getString(cursor.getColumnIndex(ShippingChildTable.key_orderID)));
                gen.put(OrderedTable.key_productName, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productName)));
                gen.put(OrderedTable.key_productWeight, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productWeight)));
                gen.put(OrderedTable.key_productProfit, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productProfit)));
                gen.put(OrderedTable.key_customerID, cursor.getString(cursor.getColumnIndex(OrderedTable.key_customerID)));
                gen.put(OrderedTable.key_productStatus, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productStatus)));
                gen.put(CustomerTable.key_customerProvince, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerProvince)));
                gen.put(CustomerTable.key_customerLat, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerLat)));
                gen.put(CustomerTable.key_customerLng, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerLng)));
                theList.add(gen);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;
    }

    public String getCustIDByShippingID(String shippingID) {
        db = dbHelper.getReadableDatabase();
        selectQuery = "SELECT a.shippingID, a.orderID, b.orderID, b.customerID FROM shippingChildTable a INNER JOIN orderedTable b ON a.orderID = b.orderID "
                + " WHERE shippingID = '"+shippingID+"' LIMIT 1";
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                return cursor.getString(cursor.getColumnIndex(OrderedTable.key_customerID));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return "";
    }

    public ArrayList<String> getOrderIDByShippingID(String shippingID) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + ShippingChildTable.key_tb_shippingchild + " WHERE " + ShippingChildTable.key_shippingID + " = '"+shippingID+"' ";
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                ar_orderid.add(cursor.getString(cursor.getColumnIndex(ShippingChildTable.key_orderID)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return ar_orderid;
    }

    private int shippingIDcount = 0;
    public int countingRows(String finalShippingIDparent) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM shippingChildTable WHERE shippingID = '"+finalShippingIDparent+"' ";
        //Log.d("iambatman_q", selectQuery);
        cursor = db.rawQuery(selectQuery, null);
        shippingIDcount = cursor.getCount();
        cursor.close();
        db.close();
        return shippingIDcount;
    }

    public ArrayList<HashMap<String, String>> sumTotalWeightProfit(String finalShippingIDparent) {
        db = dbHelper.getReadableDatabase();
        selectQuery = "SELECT a.shippingID, a.orderID, b.orderID, b.productWeight, b.productProfit " +
                "FROM shippingChildTable a INNER JOIN orderedTable b ON a.orderID = b.orderID " +
                "WHERE shippingID = '" + finalShippingIDparent + "' ";
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(ShippingChildTable.key_shippingID, cursor.getString(cursor.getColumnIndex(ShippingChildTable.key_shippingID)));
                gen.put(ShippingChildTable.key_orderID, cursor.getString(cursor.getColumnIndex(ShippingChildTable.key_orderID)));
                gen.put(OrderedTable.key_productWeight, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productWeight)));
                gen.put(OrderedTable.key_productProfit, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productProfit)));
                theList.add(gen);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;
    }

    public void deleteAllData() {
        db = dbHelper.getWritableDatabase();
        db.delete(ShippingChildTable.key_tb_shippingchild, null, null);
        db.close();
    }

    public boolean isExist(String custID) {
        db = dbHelper.getReadableDatabase();
        selectQuery = "SELECT a.orderID, b.customerID " +
                "FROM shippingChildTable a INNER JOIN orderedTable b ON a.orderID = b.orderID " +
                "WHERE customerID = '" + custID + "' ";
        cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0){
            return true;
        }
        cursor.close();
        db.close();
        return false;
    }


}
