package com.firmanpro.pwoptimization.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.firmanpro.pwoptimization.sqlite.table.CustomerTable;
import com.firmanpro.pwoptimization.sqlite.table.LegsTable;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;
import com.firmanpro.pwoptimization.sqlite.table.LogRoutesTable;
import com.firmanpro.pwoptimization.sqlite.table.SeqRoutesTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingChildTable;
import com.firmanpro.pwoptimization.sqlite.table.ShippingTable;

/**
 * Created by firmanmac on 10/29/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "pwop.db";
    private String CREATE_TABLE_CUSTOMER, CREATE_TABLE_ORDERED, CREATE_TABLE_SHIPPING,
            CREATE_TABLE_SHIPPING_CHILD, CREATE_TABLE_ROUTES, CREATE_TABLE_LEGS, CREATE_TABLE_SEQ_ROUTES;

    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        /**
         * Example PK and FK
         CREATE TABLE artist(
                 artistid    INTEGER PRIMARY KEY,
                 artistname  TEXT
            );
         CREATE TABLE track(
                trackid     INTEGER,
                trackname   TEXT,
                trackartist INTEGER,
                FOREIGN KEY(trackartist) REFERENCES artist(artistid)
            );
         */

        /**Query crate table*/
        CREATE_TABLE_CUSTOMER = "CREATE TABLE " + CustomerTable.key_tb_customer + "("
                + CustomerTable.key_customerID + " TEXT PRIMARY KEY,"
                + CustomerTable.key_customerProvince + " TEXT, "
                + CustomerTable.key_customerLat + " REAL, "
                + CustomerTable.key_customerLng + " REAL )";
        CREATE_TABLE_ORDERED = "CREATE TABLE " + OrderedTable.key_tb_ordered + "("
                + OrderedTable.key_orderID + " TEXT PRIMARY KEY,"
                + OrderedTable.key_productName + " TEXT, "
                + OrderedTable.key_productStatus + " TEXT, "
                + OrderedTable.key_customerID + " TEXT, "
                + OrderedTable.key_productWeight + " INTEGER, "
                + OrderedTable.key_productProfit + " INTEGER, "
                + "FOREIGN KEY(" + OrderedTable.key_customerID + ") REFERENCES " + CustomerTable.key_tb_customer + "(" + CustomerTable.key_customerID + ") )";
        CREATE_TABLE_SHIPPING = "CREATE TABLE " + ShippingTable.key_tb_shipping + "("
                + ShippingTable.key_shippingID + " TEXT PRIMARY KEY,"
                + ShippingTable.key_maxcapacity + " INTEGER, "
                + ShippingTable.key_typeResult + " TEXT, "
                + ShippingTable.key_totalItem + " INTEGER, "
                + ShippingTable.key_totalWeigth + " INTEGER, "
                + ShippingTable.key_totalProfit + " INTEGER, "
                + ShippingTable.key_totalDistance + " INTEGER, "
                + ShippingTable.key_totalDuration+ " INTEGER, "
                + ShippingTable.key_status + " TEXT )";
        CREATE_TABLE_SHIPPING_CHILD = "CREATE TABLE " + ShippingChildTable.key_tb_shippingchild + "("
                + ShippingChildTable.key_shippingChildID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ShippingChildTable.key_shippingID + " TEXT, "
                + ShippingChildTable.key_orderID + " TEXT, "
                + "FOREIGN KEY(" + ShippingChildTable.key_shippingID + ") REFERENCES " + ShippingTable.key_tb_shipping + "(" + ShippingTable.key_shippingID + "), "
                + "FOREIGN KEY(" + ShippingChildTable.key_orderID + ") REFERENCES " + OrderedTable.key_tb_ordered + "(" + OrderedTable.key_orderID + ") )";
        CREATE_TABLE_ROUTES = "CREATE TABLE " + LogRoutesTable.key_tb_logRoutesTable + "("
                + LogRoutesTable.key_routesID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + LogRoutesTable.key_shippingID + " TEXT, "
                + LogRoutesTable.key_logAllPath + " TEXT, "
                + LogRoutesTable.key_logBestPath + " TEXT, "
                + LogRoutesTable.key_logFinalPath + " TEXT, "
                + LogRoutesTable.key_logBestRoutesSplit + " TEXT, "
                + "FOREIGN KEY(" + LogRoutesTable.key_shippingID + ") REFERENCES " + ShippingTable.key_tb_shipping + "(" + ShippingTable.key_shippingID + ") )";
        CREATE_TABLE_LEGS = "CREATE TABLE " + LegsTable.key_tb_legsTable + "("
                + LegsTable.key_legsID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + LegsTable.key_shippingID + " TEXT, "
                + LegsTable.key_legsData + " TEXT, "
                + "FOREIGN KEY(" + LogRoutesTable.key_shippingID + ") REFERENCES " + ShippingTable.key_tb_shipping + "(" + ShippingTable.key_shippingID + ") )";
        CREATE_TABLE_SEQ_ROUTES = "CREATE TABLE " + SeqRoutesTable.key_tb_seqRoutesTable + "("
                + SeqRoutesTable.key_idSeq + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SeqRoutesTable.key_customersID + " TEXT, "
                + SeqRoutesTable.key_totalWeight + " INTEGER, "
                + SeqRoutesTable.key_isGoodWeight + " INTEGER, "
                + SeqRoutesTable.key_statusDss + " TEXT, "
                + "FOREIGN KEY(" + SeqRoutesTable.key_customersID + ") REFERENCES " + CustomerTable.key_tb_customer + "(" + CustomerTable.key_customerID + ") )";

        /**Exec crate table*/
        db.execSQL(CREATE_TABLE_CUSTOMER);
        db.execSQL(CREATE_TABLE_ORDERED);
        db.execSQL(CREATE_TABLE_SHIPPING);
        db.execSQL(CREATE_TABLE_SHIPPING_CHILD);
        db.execSQL(CREATE_TABLE_ROUTES);
        db.execSQL(CREATE_TABLE_LEGS);
        db.execSQL(CREATE_TABLE_SEQ_ROUTES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CustomerTable.key_tb_customer);
        db.execSQL("DROP TABLE IF EXISTS " + OrderedTable.key_tb_ordered);
        db.execSQL("DROP TABLE IF EXISTS " + ShippingTable.key_tb_shipping);
        db.execSQL("DROP TABLE IF EXISTS " + ShippingChildTable.key_tb_shippingchild);
        db.execSQL("DROP TABLE IF EXISTS " + LogRoutesTable.key_tb_logRoutesTable);
        db.execSQL("DROP TABLE IF EXISTS " + LegsTable.key_tb_legsTable);
        db.execSQL("DROP TABLE IF EXISTS " + SeqRoutesTable.key_tb_seqRoutesTable);
        onCreate(db);
    }

}
