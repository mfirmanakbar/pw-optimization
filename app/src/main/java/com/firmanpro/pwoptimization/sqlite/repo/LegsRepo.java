package com.firmanpro.pwoptimization.sqlite.repo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.firmanpro.pwoptimization.sqlite.DBHelper;
import com.firmanpro.pwoptimization.sqlite.table.LegsTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 11/10/17.
 */

public class LegsRepo {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private ContentValues values;
    private long general_Id;
    private Cursor cursor;
    private String selectQuery;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;

    public LegsRepo(Context context) { this.dbHelper = new DBHelper(context); }

    public int insert(LegsTable legsTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(LegsTable.key_shippingID, legsTable.shippingID);
        values.put(LegsTable.key_legsData, legsTable.legsData);
        /**Inserting Row*/
        general_Id = db.insert(LegsTable.key_tb_legsTable, null, values);
        db.close();
        return (int) general_Id;
    }

    public ArrayList<HashMap<String, String>> getDataByShippingID(String shippingID) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + LegsTable.key_tb_legsTable + " WHERE " + LegsTable.key_shippingID + " = '" + shippingID + "' ";
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(LegsTable.key_legsID, cursor.getString(cursor.getColumnIndex(LegsTable.key_legsID)));
                gen.put(LegsTable.key_shippingID, cursor.getString(cursor.getColumnIndex(LegsTable.key_shippingID)));
                gen.put(LegsTable.key_legsData, cursor.getString(cursor.getColumnIndex(LegsTable.key_legsData)));
                theList.add(gen);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;
    }

    public void deleteByShippingID(String shippingID) {
        db = dbHelper.getWritableDatabase();
        db.delete(LegsTable.key_tb_legsTable, LegsTable.key_shippingID + "=?", new String[] { shippingID });
        db.close();
    }

    public void deleteAllData() {
        db = dbHelper.getWritableDatabase();
        db.delete(LegsTable.key_tb_legsTable, null, null);
        db.close();
    }

}
