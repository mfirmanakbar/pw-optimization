package com.firmanpro.pwoptimization.sqlite.table;

/**
 * Created by firmanmac on 10/30/17.
 */

public class CustomerTable {

    /**final table name*/
    public static final String key_tb_customer = "customerTable";

    /**final column name*/
    public static final String key_customerID = "customerID";
    public static final String key_customerProvince = "customerProvince";
    public static final String key_customerLat = "customerLat";
    public static final String key_customerLng = "customerLng";

    /**variable for set get data*/
    public String customerID, customerProvince;
    public Double customerLat, customerLng;

}
