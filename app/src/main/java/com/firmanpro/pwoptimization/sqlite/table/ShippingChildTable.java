package com.firmanpro.pwoptimization.sqlite.table;

/**
 * Created by firmanmac on 11/2/17.
 */

public class ShippingChildTable {

    /**final table name*/
    public static final String key_tb_shippingchild = "shippingChildTable";

    /**final column name*/
    public static final String key_shippingChildID = "shippingChildID";
    public static final String key_shippingID = "shippingID";
    public static final String key_orderID = "orderID";

    /**variable for set get data*/
    public String shippingChildID, shippingID, orderID;

}
