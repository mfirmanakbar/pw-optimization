package com.firmanpro.pwoptimization.sqlite.repo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.firmanpro.pwoptimization.sqlite.DBHelper;
import com.firmanpro.pwoptimization.sqlite.table.CustomerTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 10/31/17.
 */

public class CustomerRepo {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private ContentValues values;
    private long general_Id;
    private Cursor cursor;
    private String selectQuery;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private int totalCounting = 0;

    public CustomerRepo(Context context) { this.dbHelper = new DBHelper(context); }

    public int insert(CustomerTable customerTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(CustomerTable.key_customerID, customerTable.customerID);
        values.put(CustomerTable.key_customerProvince, customerTable.customerProvince);
        values.put(CustomerTable.key_customerLat, customerTable.customerLat);
        values.put(CustomerTable.key_customerLng, customerTable.customerLng);
        /**Inserting Row*/
        general_Id = db.insert(CustomerTable.key_tb_customer, null, values);
        db.close();
        return (int) general_Id;
    }

    public void update(CustomerTable customerTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(CustomerTable.key_customerProvince, customerTable.customerProvince);
        values.put(CustomerTable.key_customerLat, customerTable.customerLat);
        values.put(CustomerTable.key_customerLng, customerTable.customerLng);
        db.update(CustomerTable.key_tb_customer, values, CustomerTable.key_customerID + "= ?", new String[] { String.valueOf(customerTable.customerID) });
        db.close();
    }

    /**
    public void update(GeneralTable general) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(GeneralTable.key_date_created, String.valueOf(general.date_created));
        values.put(GeneralTable.key_json_name, general.json_name);
        values.put(GeneralTable.key_detail_value, general.detail_value);

        db.update(GeneralTable.key_table_general, values, GeneralTable.key_id_general + "= ?", new String[] { String.valueOf(general.id_general) });
        db.close();
    }
    */

    public ArrayList<HashMap<String, String>> getAllData() {
        db = dbHelper.getReadableDatabase();

        selectQuery =  "SELECT * FROM " + CustomerTable.key_tb_customer;

        theList = new ArrayList<HashMap<String, String>>();

        cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(CustomerTable.key_customerID, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerID)));
                gen.put(CustomerTable.key_customerProvince, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerProvince)));
                gen.put(CustomerTable.key_customerLat, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerLat)));
                gen.put(CustomerTable.key_customerLng, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerLng)));
                theList.add(gen);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return theList;

    }

    public ArrayList<HashMap<String, String>> getByCustID( String custID) {
        db = dbHelper.getReadableDatabase();

        selectQuery =  "SELECT * FROM "
                + CustomerTable.key_tb_customer + " WHERE "
                + CustomerTable.key_customerID + " = '" + custID + "' ";

        theList = new ArrayList<HashMap<String, String>>();

        cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(CustomerTable.key_customerID, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerID)));
                gen.put(CustomerTable.key_customerProvince, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerProvince)));
                gen.put(CustomerTable.key_customerLat, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerLat)));
                gen.put(CustomerTable.key_customerLng, cursor.getString(cursor.getColumnIndex(CustomerTable.key_customerLng)));
                theList.add(gen);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return theList;

    }

    public void deleteAllData() {
        db = dbHelper.getWritableDatabase();
        db.delete(CustomerTable.key_tb_customer, null, null);
        db.close();
    }

    public int countingRows() {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + CustomerTable.key_tb_customer;
        cursor = db.rawQuery(selectQuery, null);
        totalCounting = cursor.getCount();
        cursor.close();
        db.close();
        return totalCounting;
    }

}
