package com.firmanpro.pwoptimization.sqlite.repo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.firmanpro.pwoptimization.sqlite.DBHelper;
import com.firmanpro.pwoptimization.sqlite.table.SeqRoutesTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 12/7/17.
 */

public class SeqRoutesRepo {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private ContentValues values;
    private long general_Id;
    private Cursor cursor;
    private String selectQuery;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private int totalCounting = 0;

    public SeqRoutesRepo(Context context) { this.dbHelper = new DBHelper(context); }

    public void deleteAllData() {
        db = dbHelper.getWritableDatabase();
        db.delete(SeqRoutesTable.key_tb_seqRoutesTable, null, null);
        db.close();
    }

    public int insert(SeqRoutesTable seqRoutesTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(SeqRoutesTable.key_customersID, seqRoutesTable.customersID);
        values.put(SeqRoutesTable.key_totalWeight, seqRoutesTable.totalWeight);
        values.put(SeqRoutesTable.key_isGoodWeight, seqRoutesTable.isGoodWeight);
        values.put(SeqRoutesTable.key_statusDss, seqRoutesTable.statusDss);
        /**Inserting Row*/
        general_Id = db.insert(SeqRoutesTable.key_tb_seqRoutesTable, null, values);
        db.close();
        return (int) general_Id;
    }

    public ArrayList<HashMap<String, String>> getAllData() {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + SeqRoutesTable.key_tb_seqRoutesTable;
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(SeqRoutesTable.key_idSeq, cursor.getString(cursor.getColumnIndex(SeqRoutesTable.key_idSeq)));
                gen.put(SeqRoutesTable.key_customersID, cursor.getString(cursor.getColumnIndex(SeqRoutesTable.key_customersID)));
                gen.put(SeqRoutesTable.key_totalWeight, cursor.getString(cursor.getColumnIndex(SeqRoutesTable.key_totalWeight)));
                gen.put(SeqRoutesTable.key_isGoodWeight, cursor.getString(cursor.getColumnIndex(SeqRoutesTable.key_isGoodWeight)));
                gen.put(SeqRoutesTable.key_statusDss, cursor.getString(cursor.getColumnIndex(SeqRoutesTable.key_statusDss)));
                theList.add(gen);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;
    }

    public void updateStatusByCustID(String status, String custID) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        selectQuery =  "Update " + SeqRoutesTable.key_tb_seqRoutesTable+" SET "
                + SeqRoutesTable.key_statusDss + " = '" + status + "' WHERE "
                + SeqRoutesTable.key_customersID + " = '" + custID + "' ";
        db.execSQL(selectQuery);
        db.close();
    }

    public void updateStatusAllSame(String status) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        selectQuery =  "Update " + SeqRoutesTable.key_tb_seqRoutesTable+" SET "
                + SeqRoutesTable.key_statusDss + " = '" + status + "' ";
        db.execSQL(selectQuery);
        db.close();
    }

    public int countingRows(boolean isGoodWeight) {
        db = dbHelper.getReadableDatabase();
        if (isGoodWeight) {
            selectQuery = "SELECT * FROM " + SeqRoutesTable.key_tb_seqRoutesTable + " WHERE " + SeqRoutesTable.key_isGoodWeight + "='1'";
        }else {
            selectQuery = "SELECT * FROM " + SeqRoutesTable.key_tb_seqRoutesTable + " WHERE " + SeqRoutesTable.key_isGoodWeight + "='0'";
        }
        cursor = db.rawQuery(selectQuery, null);
        totalCounting = cursor.getCount();
        cursor.close();
        db.close();
        return totalCounting;
    }

}
