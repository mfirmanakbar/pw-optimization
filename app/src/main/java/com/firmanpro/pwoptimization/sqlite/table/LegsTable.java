package com.firmanpro.pwoptimization.sqlite.table;

/**
 * Created by firmanmac on 11/10/17.
 */

public class LegsTable {

    /**final table name*/
    public static final String key_tb_legsTable = "legsTable";

    /**final column name*/
    public static final String key_legsID = "legsID";
    public static final String key_shippingID = "shippingID";
    public static final String key_legsData = "legsData";

    /**variable for set get data*/
    public int legsID;
    public String shippingID, legsData;

}
