package com.firmanpro.pwoptimization.sqlite.repo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.firmanpro.pwoptimization.sqlite.DBHelper;
import com.firmanpro.pwoptimization.sqlite.table.LogRoutesTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 11/10/17.
 */

public class LogRoutesRepo {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private ContentValues values;
    private long general_Id;
    private Cursor cursor;
    private String selectQuery;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;

    public LogRoutesRepo(Context context) { this.dbHelper = new DBHelper(context); }

    public boolean isExist(String shippingID) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + LogRoutesTable.key_tb_logRoutesTable + " WHERE " + LogRoutesTable.key_shippingID + "='" + shippingID + "' ";
        cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0){
            return true;
        }
        cursor.close();
        db.close();
        return false;
    }

    public int insert(LogRoutesTable routesTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(LogRoutesTable.key_shippingID, routesTable.shippingID);
        values.put(LogRoutesTable.key_logAllPath, routesTable.logAllPath);
        values.put(LogRoutesTable.key_logBestPath, routesTable.logBestPath);
        values.put(LogRoutesTable.key_logFinalPath, routesTable.logFinalPath);
        values.put(LogRoutesTable.key_logBestRoutesSplit, routesTable.logBestRoutesSplit);
        /**Inserting Row*/
        general_Id = db.insert(LogRoutesTable.key_tb_logRoutesTable, null, values);
        db.close();
        return (int) general_Id;
    }

    public ArrayList<HashMap<String, String>> getAllData() {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + LogRoutesTable.key_tb_logRoutesTable;
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(LogRoutesTable.key_routesID, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_routesID)));
                gen.put(LogRoutesTable.key_shippingID, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_shippingID)));
                gen.put(LogRoutesTable.key_logAllPath, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logAllPath)));
                gen.put(LogRoutesTable.key_logBestPath, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logBestPath)));
                gen.put(LogRoutesTable.key_logFinalPath, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logFinalPath)));
                gen.put(LogRoutesTable.key_logBestRoutesSplit, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logBestRoutesSplit)));
                theList.add(gen);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;
    }

    public ArrayList<HashMap<String, String>> getOneData(String shippingID) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + LogRoutesTable.key_tb_logRoutesTable + " WHERE " + LogRoutesTable.key_shippingID + " = '" + shippingID + "' ";
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(LogRoutesTable.key_routesID, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_routesID)));
                gen.put(LogRoutesTable.key_shippingID, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_shippingID)));
                gen.put(LogRoutesTable.key_logAllPath, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logAllPath)));
                gen.put(LogRoutesTable.key_logBestPath, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logBestPath)));
                gen.put(LogRoutesTable.key_logFinalPath, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logFinalPath)));
                gen.put(LogRoutesTable.key_logBestRoutesSplit, cursor.getString(cursor.getColumnIndex(LogRoutesTable.key_logBestRoutesSplit)));
                theList.add(gen);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;
    }

    public void deleteByShippingID(String shippingID) {
        db = dbHelper.getWritableDatabase();
        db.delete(LogRoutesTable.key_tb_logRoutesTable, LogRoutesTable.key_shippingID + "=?", new String[] { shippingID });
        db.close();
    }

    public void deleteAllData() {
        db = dbHelper.getWritableDatabase();
        db.delete(LogRoutesTable.key_tb_logRoutesTable, null, null);
        db.close();
    }

}
