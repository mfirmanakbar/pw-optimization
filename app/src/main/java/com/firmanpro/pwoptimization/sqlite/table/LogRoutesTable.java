package com.firmanpro.pwoptimization.sqlite.table;

/**
 * Created by firmanmac on 11/10/17.
 */

public class LogRoutesTable {

    /**final table name*/
    public static final String key_tb_logRoutesTable = "logRoutesTable";

    /**final column name*/
    public static final String key_routesID = "routesID";
    public static final String key_shippingID = "shippingID";
    public static final String key_logAllPath = "logAllPath";
    public static final String key_logBestPath = "logBestPath";
    public static final String key_logFinalPath = "logFinalPath";
    public static final String key_logBestRoutesSplit = "logBestRoutesSplit";

    /**variable for set get data*/
    public String routesID, shippingID, logAllPath, logBestPath,
            logFinalPath  = "", logBestRoutesSplit = "";

}
