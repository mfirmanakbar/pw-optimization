package com.firmanpro.pwoptimization.sqlite.repo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.sqlite.DBHelper;
import com.firmanpro.pwoptimization.sqlite.table.OrderedTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 10/29/17.
 */

public class OrderedRepo {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private ContentValues values;
    private long general_Id;
    private Cursor cursor;
    private String selectQuery, strQuery;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private ArrayList<String> ar_orderid = new ArrayList<String>();
    private int totalCounting = 0;

    public OrderedRepo(Context context) {
        this.dbHelper = new DBHelper(context);
    }

    public boolean isExist(String custID) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM "
                + OrderedTable.key_tb_ordered + " WHERE "
                + OrderedTable.key_customerID + " = '" + custID
                + "' AND " + OrderedTable.key_productStatus + " = '"
                + enumProHelper.statusTersedia + "' ";
        cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount() > 0){
            return true;
        }
        cursor.close();
        db.close();
        return false;
    }

    public int insert(OrderedTable orderedTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(OrderedTable.key_orderID, orderedTable.orderID);
        values.put(OrderedTable.key_productName, orderedTable.productName);
        values.put(OrderedTable.key_productStatus, orderedTable.productStatus);
        values.put(OrderedTable.key_customerID, orderedTable.customerID);
        values.put(OrderedTable.key_productWeight, orderedTable.productWeight);
        values.put(OrderedTable.key_productProfit, orderedTable.productProfit);
        /**Inserting Row*/
        general_Id = db.insert(OrderedTable.key_tb_ordered, null, values);
        db.close();
        return (int) general_Id;
    }

    public ArrayList<HashMap<String, String>> getAllData() {
        db = dbHelper.getReadableDatabase();

        selectQuery =  "SELECT * FROM " + OrderedTable.key_tb_ordered;

        theList = new ArrayList<HashMap<String, String>>();

        cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(OrderedTable.key_orderID, cursor.getString(cursor.getColumnIndex(OrderedTable.key_orderID)));
                gen.put(OrderedTable.key_productName, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productName)));
                gen.put(OrderedTable.key_productStatus, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productStatus)));
                gen.put(OrderedTable.key_customerID, cursor.getString(cursor.getColumnIndex(OrderedTable.key_customerID)));
                gen.put(OrderedTable.key_productWeight, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productWeight)));
                gen.put(OrderedTable.key_productProfit, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productProfit)));
                theList.add(gen);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return theList;
    }

    public ArrayList<String> getOrderIDByCustID(String custID) {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + OrderedTable.key_tb_ordered + " WHERE " + OrderedTable.key_customerID + " = '"+custID+"' ";
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                ar_orderid.add(cursor.getString(cursor.getColumnIndex(OrderedTable.key_orderID)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return ar_orderid;
    }

    public ArrayList<HashMap<String, String>> getAllDataByStatus(String status, int opmtimizationType, String custIDForOptimization) {
        db = dbHelper.getReadableDatabase();

        if (opmtimizationType == 3 && !custIDForOptimization.equals("")) {
            selectQuery = "SELECT * FROM " + OrderedTable.key_tb_ordered + " WHERE " + OrderedTable.key_productStatus + " = '" + status + "' AND "
                    + OrderedTable.key_customerID + " = '" + custIDForOptimization + "' ";
        }else if (opmtimizationType == 2){
            selectQuery =  "SELECT * FROM " + OrderedTable.key_tb_ordered +" WHERE " + OrderedTable.key_productStatus + " = '" + status + "' ";
        }

        theList = new ArrayList<HashMap<String, String>>();

        cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(OrderedTable.key_orderID, cursor.getString(cursor.getColumnIndex(OrderedTable.key_orderID)));
                gen.put(OrderedTable.key_productName, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productName)));
                gen.put(OrderedTable.key_productStatus, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productStatus)));
                gen.put(OrderedTable.key_customerID, cursor.getString(cursor.getColumnIndex(OrderedTable.key_customerID)));
                gen.put(OrderedTable.key_productWeight, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productWeight)));
                gen.put(OrderedTable.key_productProfit, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productProfit)));
                theList.add(gen);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;

    }

    public ArrayList<HashMap<String, String>> getAllDataByStatus(String status, String custID) {
        db = dbHelper.getReadableDatabase();

        selectQuery = "SELECT * FROM " + OrderedTable.key_tb_ordered + " WHERE " + OrderedTable.key_productStatus + " = '" + status + "' AND "
                + OrderedTable.key_customerID + " = '" + custID + "' ";

        theList = new ArrayList<HashMap<String, String>>();

        cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(OrderedTable.key_orderID, cursor.getString(cursor.getColumnIndex(OrderedTable.key_orderID)));
                gen.put(OrderedTable.key_productName, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productName)));
                gen.put(OrderedTable.key_productStatus, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productStatus)));
                gen.put(OrderedTable.key_customerID, cursor.getString(cursor.getColumnIndex(OrderedTable.key_customerID)));
                gen.put(OrderedTable.key_productWeight, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productWeight)));
                gen.put(OrderedTable.key_productProfit, cursor.getString(cursor.getColumnIndex(OrderedTable.key_productProfit)));
                theList.add(gen);

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;

    }

    public long getSumWeight(){
        db = dbHelper.getReadableDatabase();
        selectQuery = "SELECT SUM("+OrderedTable.key_productWeight+") FROM "+OrderedTable.key_tb_ordered;
        cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            return cursor.getLong(0);
        }
        cursor.close();
        db.close();
        return 0;
    }

    public long getSumWeight(String custID){
        db = dbHelper.getReadableDatabase();

        selectQuery = "SELECT SUM("+OrderedTable.key_productWeight+") FROM "+OrderedTable.key_tb_ordered+" WHERE "+OrderedTable.key_customerID+" = '"+custID+"' ";
        Log.d("iamflash_ser", selectQuery);

        cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            Log.d("iamflash_ser", String.valueOf(cursor.getLong(0)));
            return cursor.getLong(0);
        }
        cursor.close();
        db.close();
        return 0;
    }

    public long getSumProfit(){
        db = dbHelper.getReadableDatabase();
        selectQuery = "SELECT SUM("+OrderedTable.key_productProfit+") FROM "+OrderedTable.key_tb_ordered;
        cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            return cursor.getLong(0);
        }
        cursor.close();
        db.close();
        return 0;
    }

    public long getSumProfit(String custID){
        db = dbHelper.getReadableDatabase();

        selectQuery = "SELECT SUM("+OrderedTable.key_productProfit+") FROM "+OrderedTable.key_tb_ordered+" WHERE "+OrderedTable.key_customerID+" = '"+custID+"' ";
        //Log.d("iamflash", selectQuery);

        cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            //Log.d("iamflash", String.valueOf(cursor.getLong(0)));
            return cursor.getLong(0);
        }
        cursor.close();
        db.close();
        return 0;
    }

    public void updateStatusByOrderID(OrderedTable orderedTable) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        strQuery =  "Update " + OrderedTable.key_tb_ordered +" SET " +
                OrderedTable.key_productStatus +
                " = '" + orderedTable.productStatus +
                "' WHERE " + OrderedTable.key_orderID + " = '" +orderedTable.orderID + "' ";
        db.execSQL(strQuery);
        db.close();
    }

    public void updateStatusAllData(String status) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        strQuery =  "Update " + OrderedTable.key_tb_ordered +" SET " +
                OrderedTable.key_productStatus + " = '" + status + "' ";
        db.execSQL(strQuery);
        db.close();
    }

    public void deleteAllData() {
        db = dbHelper.getWritableDatabase();
        db.delete(OrderedTable.key_tb_ordered, null, null);
        db.close();
    }

    public int countingRows() {
        db = dbHelper.getReadableDatabase();
        selectQuery =  "SELECT * FROM " + OrderedTable.key_tb_ordered;
        cursor = db.rawQuery(selectQuery, null);
        totalCounting = cursor.getCount();
        cursor.close();
        db.close();
        return totalCounting;
    }

}
