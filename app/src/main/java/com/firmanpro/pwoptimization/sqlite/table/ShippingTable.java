package com.firmanpro.pwoptimization.sqlite.table;

/**
 * Created by firmanmac on 11/2/17.
 */

public class ShippingTable {

    /**final table name*/
    public static final String key_tb_shipping = "shippingTable";

    /**final column name*/
    public static final String key_shippingID = "shippingID";
    public static final String key_maxcapacity = "maxcapacity";
    public static final String key_typeResult = "typeResult";
    public static final String key_totalItem = "totalItem";
    public static final String key_totalWeigth = "totalWeigth";
    public static final String key_totalProfit = "totalProfit";
    public static final String key_totalDistance = "totalDistance";
    public static final String key_totalDuration = "totalDuration";
    public static final String key_status = "status";

    /**
     * pada status diisi Ok dan Cancel
     * jika Ok maka pengiriman dilakukan
     * jika Cancel maka pengiriman tidak dibaca karna ini sisa data kombinasi
     * yang sudah masuk ke data pengiriman baru
     * */

    /**variable for set get data*/
    public String shippingID, typeResult, status;
    public int totalItem;
    public long maxcapacity, totalWeigth, totalProfit, totalDistance, totalDuration;

}
