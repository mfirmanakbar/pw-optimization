package com.firmanpro.pwoptimization.sqlite.table;

/**
 * Created by firmanmac on 10/29/17.
 */

public class OrderedTable {

    /**final table name*/
    public static final String key_tb_ordered = "orderedTable";

    /**final column name*/
    public static final String key_orderID = "orderID";
    public static final String key_productName = "productName";
    public static final String key_productStatus = "productStatus";
    public static final String key_customerID = "customerID";
    public static final String key_productWeight = "productWeight";
    public static final String key_productProfit = "productProfit";

    /**variable for set get data*/
    public String orderID, productName, productStatus, customerID;
    public int productWeight, productProfit;

}
