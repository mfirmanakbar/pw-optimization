package com.firmanpro.pwoptimization.sqlite.repo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.sqlite.DBHelper;
import com.firmanpro.pwoptimization.sqlite.table.ShippingTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by firmanmac on 11/2/17.
 */

public class ShippingRepo {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private ContentValues values;
    private long general_Id;
    private Cursor cursor;
    private String selectQuery;
    private ArrayList<HashMap<String, String>> theList;
    private HashMap<String, String> gen;
    private String shippingIDdefault = "SPP";
    private int shippingIDcount = 0;
    private int shippingRows = 0;
    private String finalShippingID = "";
    private EnumProHelper enumProHelper = new EnumProHelper();

    public ShippingRepo(Context context) { this.dbHelper = new DBHelper(context); }

    public int countingRows(boolean isAllStatus, String status) {
        db = dbHelper.getReadableDatabase();
        if (isAllStatus) {
            selectQuery = "SELECT * FROM " + ShippingTable.key_tb_shipping;
        }else {
            selectQuery = "SELECT * FROM " + ShippingTable.key_tb_shipping + " WHERE " + ShippingTable.key_status + " ='" + status + "' ";
        }
        cursor = db.rawQuery(selectQuery, null);
        shippingIDcount = cursor.getCount();
        cursor.close();
        db.close();
        return shippingIDcount;
    }

    public String insert(ShippingTable shippingTable) {
        shippingRows = countingRows(true, "");
        shippingRows += 1;
        finalShippingID = shippingIDdefault + String.valueOf(shippingRows);
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        values.put(ShippingTable.key_shippingID, finalShippingID);
        values.put(ShippingTable.key_maxcapacity, shippingTable.maxcapacity);
        values.put(ShippingTable.key_typeResult, shippingTable.typeResult);
        values.put(ShippingTable.key_totalItem, shippingTable.totalItem);
        values.put(ShippingTable.key_totalWeigth, shippingTable.totalWeigth);
        values.put(ShippingTable.key_totalProfit, shippingTable.totalProfit);
        values.put(ShippingTable.key_totalDistance, 0);
        values.put(ShippingTable.key_totalDuration, 0);
        values.put(ShippingTable.key_status, shippingTable.status);
        /**Inserting Row*/
        general_Id = db.insert(ShippingTable.key_tb_shipping, null, values);
        db.close();
        return finalShippingID;
    }

    public void updateTotalDistance(String shippingID, String totalDistance) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        selectQuery =  "Update shippingTable SET totalDistance = '" + totalDistance + "' WHERE shippingID = '" + shippingID + "' ";
        db.execSQL(selectQuery);
        db.close();
    }

    public void updateTotalDuration(String shippingID, String totalDuration) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        selectQuery =  "Update shippingTable SET totalDuration = '" + totalDuration + "' WHERE shippingID = '" + shippingID + "' ";
        db.execSQL(selectQuery);
        db.close();
    }

    public void updateTotal(int totalItem, long totalWeigth, long totalProfit, String shippingID) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        selectQuery =  "Update shippingTable SET totalItem = '" + totalItem + "', totalWeigth = '"
                + totalWeigth + "', totalProfit = '" + totalProfit + "' WHERE shippingID = '" + shippingID + "' ";
        db.execSQL(selectQuery);
        db.close();
    }

    public void updateStatus(String shippingID, String status) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        selectQuery =  "Update shippingTable SET "+ShippingTable.key_status+" = '" + status + "' WHERE shippingID = '" + shippingID + "' ";
        db.execSQL(selectQuery);
        db.close();
    }

    public void updateAllStatus(String status) {
        db = dbHelper.getWritableDatabase();
        values = new ContentValues();
        selectQuery =  "Update shippingTable SET "+ShippingTable.key_status+" = '" + status+"' ";
        db.execSQL(selectQuery);
        db.close();
    }

    public ArrayList<HashMap<String, String>> getAllData(boolean isAllStatus, String status) {
        db = dbHelper.getReadableDatabase();
        if (isAllStatus) {
            selectQuery = "SELECT * FROM " + ShippingTable.key_tb_shipping;
        }else {
            selectQuery = "SELECT * FROM " + ShippingTable.key_tb_shipping + " WHERE " + ShippingTable.key_status + " ='" + status + "' ";
            Log.d("superman", selectQuery);
        }
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                gen = new HashMap<String, String>();
                gen.put(ShippingTable.key_shippingID, cursor.getString(cursor.getColumnIndex(ShippingTable.key_shippingID)));
                gen.put(ShippingTable.key_maxcapacity, cursor.getString(cursor.getColumnIndex(ShippingTable.key_maxcapacity)));
                gen.put(ShippingTable.key_typeResult, cursor.getString(cursor.getColumnIndex(ShippingTable.key_typeResult)));
                gen.put(ShippingTable.key_totalItem, cursor.getString(cursor.getColumnIndex(ShippingTable.key_totalItem)));
                gen.put(ShippingTable.key_totalWeigth, cursor.getString(cursor.getColumnIndex(ShippingTable.key_totalWeigth)));
                gen.put(ShippingTable.key_totalProfit, cursor.getString(cursor.getColumnIndex(ShippingTable.key_totalProfit)));
                gen.put(ShippingTable.key_totalDistance, cursor.getString(cursor.getColumnIndex(ShippingTable.key_totalDistance)));
                gen.put(ShippingTable.key_totalDuration, cursor.getString(cursor.getColumnIndex(ShippingTable.key_totalDuration)));
                gen.put(ShippingTable.key_status, cursor.getString(cursor.getColumnIndex(ShippingTable.key_status)));
                theList.add(gen);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return theList;
    }

    public boolean isStatusOk(String shippingID) {
        db = dbHelper.getReadableDatabase();
        selectQuery = "SELECT * FROM " + ShippingTable.key_tb_shipping + " WHERE " + ShippingTable.key_shippingID + " ='" + shippingID + "' LIMIT 1";
        theList = new ArrayList<HashMap<String, String>>();
        cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getString(cursor.getColumnIndex(ShippingTable.key_status)).equals(enumProHelper.keyStatusOk)){
                    return true;
                }else {
                    return false;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return false;
    }

    public void deleteAllData() {
        db = dbHelper.getWritableDatabase();
        db.delete(ShippingTable.key_tb_shipping, null, null);
        db.close();
    }

    public void deleteByShippingID(String shippingID) {
        db = dbHelper.getWritableDatabase();
        db.delete(ShippingTable.key_tb_shipping, ShippingTable.key_shippingID + "=?", new String[] { shippingID });
        db.close();
    }

    public int getSumDistance(boolean isAllStatus, String status){
        db = dbHelper.getReadableDatabase();
        if (isAllStatus) {
            selectQuery = "SELECT SUM(" + ShippingTable.key_totalDistance + ") FROM " + ShippingTable.key_tb_shipping;
        }else {
            selectQuery = "SELECT SUM(" + ShippingTable.key_totalDistance + ") FROM " + ShippingTable.key_tb_shipping + " WHERE " + ShippingTable.key_status + " ='" + status + "' ";
        }
        cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        cursor.close();
        db.close();
        return 0;
    }

    public int getSumDuration(boolean isAllStatus, String status){
        db = dbHelper.getReadableDatabase();
        if (isAllStatus) {
            selectQuery = "SELECT SUM(" + ShippingTable.key_totalDuration+ ") FROM " + ShippingTable.key_tb_shipping;
        }else {
            selectQuery = "SELECT SUM(" + ShippingTable.key_totalDuration + ") FROM " + ShippingTable.key_tb_shipping + " WHERE " + ShippingTable.key_status + " ='" + status + "' ";
        }
        cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        cursor.close();
        db.close();
        return 0;
    }

}
