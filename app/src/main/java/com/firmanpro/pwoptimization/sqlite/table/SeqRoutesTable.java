package com.firmanpro.pwoptimization.sqlite.table;

/**
 * Created by firmanmac on 12/7/17.
 */

public class SeqRoutesTable {

    /**
     * tabel ini diisi dengan
     * urutan rute-rute
     * dimana rute yang isGoodWeight = 1
     * dapat digabungkan pengirimannya dengan yang lain
     * selama total bobot gabungan <= max capacity
     * */

    /**final table name*/
    public static final String key_tb_seqRoutesTable = "seqRoutesTable";

    /**final column name*/
    public static final String key_idSeq = "idSeq";
    public static final String key_customersID = "customerID";
    public static final String key_totalWeight = "totalWeight";
    public static final String key_isGoodWeight = "isGoodWeight";
    public static final String key_statusDss = "statusDss";

    /**variable for set get data
     * isGoodWeight = 1 berarti bagus bisa untuk gabungan
     * isGoodWeight = 0 berarti gak bagus harus di optimasi dengan knapsack problem
     * statusDss = done artinya sudah masuk ke data pengiriman
     * statusDss = "" atau kosong artinya belum dimasukan ke pengiriman
     * statusDss ini agar dapat melakukan penyimpanan data pengiriman setiap menu Shipping Dibuka hanya WHERE statusDSS = "" atau != "done"
     * */
    public int idSeq, isGoodWeight;
    public long totalWeight;
    public String customersID, statusDss;

}
