package com.firmanpro.pwoptimization.config;

/**
 * Created by firmanmac on 11/10/17.
 */

public class ApiAddress {

    /**
     * Google Sheet API
     * isi sheet dengan tab sheet name
     * isi YOUR_SPREAD_SHEET_ID dengan ID google sheet
     * https://script.google.com/macros/s/AKfycbxOLElujQcy1-ZUer1KgEvK16gkTLUqYftApjNCM_IRTL3HSuDk/exec?id=YOUR_SPREAD_SHEET_ID&sheet=Sheet1
     * nanti output linknya akan keluar
     * dan pakai itu untuk alamat JSON API
     * */

    public static String api_import_customers = "https://script.googleusercontent.com/macros/echo?user_content_key=gZu5HVrwvLLs2pK1cncADHwO5nkzfkIoPyQWq_TpvXSX8ZGcNyqNfHyMs38yJ26qmiWSegfhYCIgCmn29OeI3_fe7bpvY1xROJmA1Yb3SEsKFZqtv3DaNYcMrmhZHmUMWojr9NvTBuBLhyHCd5hHa1GhPSVukpSQTydEwAEXFXgt_wltjJcH3XHUaaPC1fv5o9XyvOto09QuWI89K6KjOu0SP2F-BdwUfC0C58ShCCpfS0-iYU-EcVsmzXRWwntQaGZ-SPMsTK2aULeaGuSKHtmXbd1wLwAVzr5ckcBEOoHtl92mnliCIMflQ8_xSmkKZv-KoJnabw0&lib=MnrE7b2I2PjfH799VodkCPiQjIVyBAxva";
    public static String api_import_ordered = "https://script.googleusercontent.com/macros/echo?user_content_key=SY1hxzuGv-zQhyS9-bK0mDCQpgzXrO9qzSY9nSlUhWm43dCTDa0_NyoDInoz4u0bt1UbOTSpuB0xjzOb6NYSkLrYbZV35fKDOJmA1Yb3SEsKFZqtv3DaNYcMrmhZHmUMWojr9NvTBuBLhyHCd5hHa1GhPSVukpSQTydEwAEXFXgt_wltjJcH3XHUaaPC1fv5o9XyvOto09QuWI89K6KjOu0SP2F-BdwUfC0C58ShCCpfS0-iYU-EcVsmzXRWwntQaGZ-SPMsTK2aULeaGuSKHtmXbd1wLwAVzr5ckcBEOoHtl92mnliCIN2SHbtm7-uz&lib=MnrE7b2I2PjfH799VodkCPiQjIVyBAxva";
    public static String api_import_customers_2 = "https://script.googleusercontent.com/macros/echo?user_content_key=cI01QNZWgxta7sJrteyyszXo1WERvG704UwZlPoYWSSlmxD-uHNwXgawzTh8Tt_wxm4Xh3Zl5R0frz6YXQBnCKHb53veVTQDOJmA1Yb3SEsKFZqtv3DaNYcMrmhZHmUMWojr9NvTBuBLhyHCd5hHa1GhPSVukpSQTydEwAEXFXgt_wltjJcH3XHUaaPC1fv5o9XyvOto09QuWI89K6KjOu0SP2F-BdwUkSK31dTRRx5N3XPtN5y_DM12EntOtKbOoSTZKGNMp-BmV16X3axOXNUeHM9PzUzMzr5ckcBEOoHtl92mnliCIMflQ8_xSmkKZv-KoJnabw0&lib=MnrE7b2I2PjfH799VodkCPiQjIVyBAxva";
    public static String api_import_ordered_2 = "https://script.googleusercontent.com/macros/echo?user_content_key=zomfy9uI8cAVzcjnMprdwMzr2in6lPH1mlicozITcMHGBLMNOx7vc-VVf4rPUgItGISVSDLmrTsfrz6YXQBnCBp0OvFq8VJQOJmA1Yb3SEsKFZqtv3DaNYcMrmhZHmUMWojr9NvTBuBLhyHCd5hHa1GhPSVukpSQTydEwAEXFXgt_wltjJcH3XHUaaPC1fv5o9XyvOto09QuWI89K6KjOu0SP2F-BdwUkSK31dTRRx5N3XPtN5y_DM12EntOtKbOoSTZKGNMp-BmV16X3axOXNUeHM9PzUzMzr5ckcBEOoHtl92mnliCIN2SHbtm7-uz&lib=MnrE7b2I2PjfH799VodkCPiQjIVyBAxva";

}
