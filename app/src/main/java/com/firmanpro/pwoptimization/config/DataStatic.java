package com.firmanpro.pwoptimization.config;

/**
 * Created by firmanmac on 1/1/18.
 */

public class DataStatic {

    public final static String import_tb_ordered = "{\n" +
            "    \"import_tb_ordered\": [\n" +
            "        {\n" +
            "            \"orderID\": \"ORD1\",\n" +
            "            \"productName\": \"POCARI SWEAT\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 18580,\n" +
            "            \"productProfit\": 384046\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD2\",\n" +
            "            \"productName\": \"POCARI SWEAT CAN BND 6\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 28580,\n" +
            "            \"productProfit\": 384046\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD3\",\n" +
            "            \"productName\": \"POCARI SWEAT PET BESAR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 228904\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD4\",\n" +
            "            \"productName\": \"POCARI SWEAT PET K BND 6\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 39100,\n" +
            "            \"productProfit\": 438516\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD5\",\n" +
            "            \"productName\": \"POCARI SWEAT CAN BND 4\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 48580,\n" +
            "            \"productProfit\": 384044\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD6\",\n" +
            "            \"productName\": \"POCARI SWEAT PET K BND 4\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 39100,\n" +
            "            \"productProfit\": 438516\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD7\",\n" +
            "            \"productName\": \"POCARI SWEAT 900ML\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 23400,\n" +
            "            \"productProfit\": 535002\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD8\",\n" +
            "            \"productName\": \"DF HOT COCOA BAG\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 1398800\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD9\",\n" +
            "            \"productName\": \"RITZ CHAMPANGE GP\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 9360,\n" +
            "            \"productProfit\": 1382160\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD10\",\n" +
            "            \"productName\": \"SILVER QUEEN KING STRAW\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 26000,\n" +
            "            \"productProfit\": 5433480\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD11\",\n" +
            "            \"productName\": \"SILVER QUEEN KING CASHEW\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 26000,\n" +
            "            \"productProfit\": 5433480\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD12\",\n" +
            "            \"productName\": \"ISARPLAS KALENG\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 18200,\n" +
            "            \"productProfit\": 4807069\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD13\",\n" +
            "            \"productName\": \"FOX KALENG KUNING\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 3349178\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD14\",\n" +
            "            \"productName\": \"FOX KALENG KUNING\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 18200,\n" +
            "            \"productProfit\": 3122269\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD15\",\n" +
            "            \"productName\": \"FOX BOTOL PVAC PUTIH\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 1940073\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD16\",\n" +
            "            \"productName\": \"FOX KANTONG BIRU (PVAC)\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2128407\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD17\",\n" +
            "            \"productName\": \"FOX KANTONG BIRU (PVAC)\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 15600,\n" +
            "            \"productProfit\": 1734720\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD18\",\n" +
            "            \"productName\": \"FOX KANTONG HIJAU (PVAC)\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 2024029\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD19\",\n" +
            "            \"productName\": \"FOX KANTONG HIJAU (PVAC)\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 20800,\n" +
            "            \"productProfit\": 1946880\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD20\",\n" +
            "            \"productName\": \"FUNTIME LONG GP VANILLA\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 10140,\n" +
            "            \"productProfit\": 452400\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD21\",\n" +
            "            \"productName\": \"SLMT BISCUIT TIN 900GR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 23400,\n" +
            "            \"productProfit\": 1046760\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD22\",\n" +
            "            \"productName\": \"SLMT WAFER 750GR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 19500,\n" +
            "            \"productProfit\": 942240\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD23\",\n" +
            "            \"productName\": \"SLMT WAFER 340GR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 8840,\n" +
            "            \"productProfit\": 636480\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD24\",\n" +
            "            \"productName\": \"MACKEREL BESAR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 11050,\n" +
            "            \"productProfit\": 3332727\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD25\",\n" +
            "            \"productName\": \"RANESA MACKEREL CHILI\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 11050,\n" +
            "            \"productProfit\": 737455\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD26\",\n" +
            "            \"productName\": \"RANESA CHILI SAUCE\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 8840,\n" +
            "            \"productProfit\": 425455\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD27\",\n" +
            "            \"productName\": \"RANESA SWEET CHILI SAUCE\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 8840,\n" +
            "            \"productProfit\": 425455\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD28\",\n" +
            "            \"productName\": \"RANESA SAOS TOMAT\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 11050,\n" +
            "            \"productProfit\": 1016364\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD29\",\n" +
            "            \"productName\": \"RANESA MACKEREL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 11050,\n" +
            "            \"productProfit\": 737455\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD30\",\n" +
            "            \"productName\": \"RANESA CHILI SAUCE LEMON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"productWeight\": 8840,\n" +
            "            \"productProfit\": 425455\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD31\",\n" +
            "            \"productName\": \"RANESA CHICKEN LUNCHEON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD32\",\n" +
            "            \"productName\": \"RANESA BEEF LUNCHEON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD33\",\n" +
            "            \"productName\": \"SARDINES PREMIUM BESAR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD34\",\n" +
            "            \"productName\": \"PRODUKSHINTA 05\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD35\",\n" +
            "            \"productName\": \"PRODUKSHINTA 06\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD36\",\n" +
            "            \"productName\": \"LASEGAR PET\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD37\",\n" +
            "            \"productName\": \"LASEGAR KALENG JAMBU\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 127636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD38\",\n" +
            "            \"productName\": \"LASEGAR KALENG JRK NPIS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD39\",\n" +
            "            \"productName\": \"LASEGAR KALENG LYCHEE\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD40\",\n" +
            "            \"productName\": \"LASEGAR KALENG MELON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8840,\n" +
            "            \"productProfit\": 1536364\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD41\",\n" +
            "            \"productName\": \"LASEGAR KALENG ORANGE\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 8840,\n" +
            "            \"productProfit\": 2009091\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD42\",\n" +
            "            \"productName\": \"LASEGAR KALENG STRAWBERRY\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 11050,\n" +
            "            \"productProfit\": 1347273\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD43\",\n" +
            "            \"productName\": \"LIANG TEH CAP PISTOL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 180818\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD44\",\n" +
            "            \"productName\": \"LASEGAR KALENG RASPBERRY\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 260000,\n" +
            "            \"productProfit\": 650000\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD45\",\n" +
            "            \"productName\": \"LASEGAR KALENG SIRSAK\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"productWeight\": 325000,\n" +
            "            \"productProfit\": 260000\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD46\",\n" +
            "            \"productName\": \"LIANG TEH CAP PISTOL BND\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 127636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD47\",\n" +
            "            \"productName\": \"LASEGAR CAN CONTAINER\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 179636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD48\",\n" +
            "            \"productName\": \"LIANG TEH CAP PISTOL BND6\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 127636\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD49\",\n" +
            "            \"productName\": \"TEANGIN\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 70909\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD50\",\n" +
            "            \"productName\": \"TEANGIN CONTAINER\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 70909\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD51\",\n" +
            "            \"productName\": \"TEANGIN BANDED 4\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 8320,\n" +
            "            \"productProfit\": 70909\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD52\",\n" +
            "            \"productName\": \"SENDANG AIR MINUM\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 8580,\n" +
            "            \"productProfit\": 953030\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD53\",\n" +
            "            \"productName\": \"SENDANG AIR MINUM\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 255273\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD54\",\n" +
            "            \"productName\": \"YEOS SOYA BEAN PET\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 9724,\n" +
            "            \"productProfit\": 262808\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD55\",\n" +
            "            \"productName\": \"WATER COOLANT\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 9750,\n" +
            "            \"productProfit\": 101192\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD56\",\n" +
            "            \"productName\": \"ALPO CHOP HOUSE FLM BACON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 992727\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD57\",\n" +
            "            \"productName\": \"KOKO KRUNCH DUO Cereal\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 891306\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD58\",\n" +
            "            \"productName\": \"FOXS FRUIT OVAL FLOWPACK\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 15600,\n" +
            "            \"productProfit\": 992727\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD59\",\n" +
            "            \"productName\": \"CARNATION Coffee-mateNIID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 15600,\n" +
            "            \"productProfit\": 988390\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD60\",\n" +
            "            \"productName\": \"MILO ACTIGEN-E BIB NI PR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"productWeight\": 26000,\n" +
            "            \"productProfit\": 58500\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD61\",\n" +
            "            \"productName\": \"PRE NAN B NW026-1 S\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 1462552\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD62\",\n" +
            "            \"productName\": \"FRISKIES ADL WET SEAF CAN\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 484146\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD63\",\n" +
            "            \"productName\": \"FRISKIES ADL WETPR TUNA\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 484146\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD64\",\n" +
            "            \"productName\": \"NESTLEMom MeBLVanila ID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2205944\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD65\",\n" +
            "            \"productName\": \"NESTLE Mom Me BL Choco ID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2205944\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD66\",\n" +
            "            \"productName\": \"MILO CEREAL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 8580,\n" +
            "            \"productProfit\": 953030\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD67\",\n" +
            "            \"productName\": \"NAN2 Probiotic LEB032 BIB\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2949856\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD68\",\n" +
            "            \"productName\": \"NAN1 PROBIOTIC NWB023 BIB\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 3140878\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD69\",\n" +
            "            \"productName\": \"NAN1 PROBIOTIC NWB023 BIB\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 3140878\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD70\",\n" +
            "            \"productName\": \"NAN2 probiotics leb032 bb\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2876068\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD71\",\n" +
            "            \"productName\": \"LACTOGEN 1 PROBIOTICS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2563179\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD72\",\n" +
            "            \"productName\": \"LACTOGEN 1 PREBIOTICS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 19500,\n" +
            "            \"productProfit\": 2381068\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD73\",\n" +
            "            \"productName\": \"DC. DATITA Madu+DHA ID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 1546350\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD74\",\n" +
            "            \"productName\": \"DC. DATITAVanilla+DHAID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 1546350\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD75\",\n" +
            "            \"productName\": \"FOXS Spring Tea Jar ID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"productWeight\": 13000,\n" +
            "            \"productProfit\": 144664\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD76\",\n" +
            "            \"productName\": \"CARNATION SCC Chocolate\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 9750,\n" +
            "            \"productProfit\": 749268\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD77\",\n" +
            "            \"productName\": \"LACTOGEN 2 Klasik BIB ID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 23400,\n" +
            "            \"productProfit\": 1540604\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD78\",\n" +
            "            \"productName\": \"LACTOGEN 3 PROBIOTICS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 21450,\n" +
            "            \"productProfit\": 2169374\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD79\",\n" +
            "            \"productName\": \"LACTOGEN 2 PROBIOTICS ID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2534981\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD80\",\n" +
            "            \"productName\": \"LACTOGEN 3 PROBIOTICS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2090993\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD81\",\n" +
            "            \"productName\": \"LACTOGEN 3 PROBIOTICS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 19500,\n" +
            "            \"productProfit\": 2081974\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD82\",\n" +
            "            \"productName\": \"LACTOGEN 2 PREBIOTICS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 19500,\n" +
            "            \"productProfit\": 2310124\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD83\",\n" +
            "            \"productName\": \"LACTOGEN Gro 3 Pro HONEY\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2090993\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD84\",\n" +
            "            \"productName\": \"LACTOGEN Gro 3 Pro HONEY\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 19500,\n" +
            "            \"productProfit\": 2169374\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD85\",\n" +
            "            \"productName\": \"DANSTART 2 Probio BL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 20800,\n" +
            "            \"productProfit\": 2047710\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD86\",\n" +
            "            \"productName\": \"DANSTART 2 Probio BL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 2558088\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD87\",\n" +
            "            \"productName\": \"DANSTART 1 Probio BL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 20800,\n" +
            "            \"productProfit\": 1434004\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD88\",\n" +
            "            \"productName\": \"DANSTART 1 Probio BL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 2720855\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD89\",\n" +
            "            \"productName\": \"LACT. 4 Probio VANILA\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 19500,\n" +
            "            \"productProfit\": 4340728\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD90\",\n" +
            "            \"productName\": \"LACTOGEN 4 Probio VANILA\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"productWeight\": 9100,\n" +
            "            \"productProfit\": 2006867\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD91\",\n" +
            "            \"productName\": \"DC DATITA VANILA+DHA\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 26000,\n" +
            "            \"productProfit\": 1392118\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD92\",\n" +
            "            \"productName\": \"DC. DATITA Madu+DHA\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 26000,\n" +
            "            \"productProfit\": 1392118\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD93\",\n" +
            "            \"productName\": \"DC. FULL CREAMBIB PRCTNID\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 2194681\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD94\",\n" +
            "            \"productName\": \"DANCOW FULL CREAM FE BIB\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 20800,\n" +
            "            \"productProfit\": 1992165\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD95\",\n" +
            "            \"productName\": \"DC. INST EN FORT BIB\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 1825952\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD96\",\n" +
            "            \"productName\": \"DC. INST EN FORT BIB\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 20800,\n" +
            "            \"productProfit\": 1820385\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD97\",\n" +
            "            \"productName\": \"ALPO PUPPY BEEF VEG\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 11700,\n" +
            "            \"productProfit\": 345826\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD98\",\n" +
            "            \"productName\": \"ALPO PUPPY BEEF VEG\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 33800,\n" +
            "            \"productProfit\": 435734\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD99\",\n" +
            "            \"productName\": \"DC. DATITA 5+ MADU\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 26000,\n" +
            "            \"productProfit\": 1392118\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD100\",\n" +
            "            \"productName\": \"DANCOW 1+COK EXC PROBIO\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 20800,\n" +
            "            \"productProfit\": 1972894\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD101\",\n" +
            "            \"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 26000,\n" +
            "            \"productProfit\": 2329782\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD102\",\n" +
            "            \"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 2011913\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD103\",\n" +
            "            \"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 20800,\n" +
            "            \"productProfit\": 1972894\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD104\",\n" +
            "            \"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 19500,\n" +
            "            \"productProfit\": 1686029\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD105\",\n" +
            "            \"productName\": \"DC. 1+ VAN EXTNUTRPROBIO\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"productWeight\": 10400,\n" +
            "            \"productProfit\": 2011913\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD106\",\n" +
            "            \"productName\": \"RANESA CHICKEN LUNCHEON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8320\",\n" +
            "            \"productProfit\": \"179636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD107\",\n" +
            "            \"productName\": \"RANESA BEEF LUNCHEON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8320\",\n" +
            "            \"productProfit\": \"179636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD108\",\n" +
            "            \"productName\": \"SARDINES PREMIUM BESAR\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8320\",\n" +
            "            \"productProfit\": \"179636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD109\",\n" +
            "            \"productName\": \"PRODUKSHINTA 05\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"9500\",\n" +
            "            \"productProfit\": \"139636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD110\",\n" +
            "            \"productName\": \"PRODUKSHINTA 06\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8320\",\n" +
            "            \"productProfit\": \"179636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD111\",\n" +
            "            \"productName\": \"LASEGAR PET\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8320\",\n" +
            "            \"productProfit\": \"179636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD112\",\n" +
            "            \"productName\": \"LASEGAR KALENG JAMBU\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"7000\",\n" +
            "            \"productProfit\": \"127636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD113\",\n" +
            "            \"productName\": \"LASEGAR KALENG JRK NPIS\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8320\",\n" +
            "            \"productProfit\": \"179636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD114\",\n" +
            "            \"productName\": \"LASEGAR KALENG LYCHEE\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8320\",\n" +
            "            \"productProfit\": \"179636\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD115\",\n" +
            "            \"productName\": \"LASEGAR KALENG MELON\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8840\",\n" +
            "            \"productProfit\": \"1536364\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD116\",\n" +
            "            \"productName\": \"LASEGAR KALENG ORANGE\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"8840\",\n" +
            "            \"productProfit\": \"2009091\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD117\",\n" +
            "            \"productName\": \"LASEGAR KALENG STRAWBERRY\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"11050\",\n" +
            "            \"productProfit\": \"1347273\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD118\",\n" +
            "            \"productName\": \"LIANG TEH CAP PISTOL\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"30000\",\n" +
            "            \"productProfit\": \"180818\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD119\",\n" +
            "            \"productName\": \"LASEGAR KALENG RASPBERRY\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"260000\",\n" +
            "            \"productProfit\": \"650000\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD120\",\n" +
            "            \"productName\": \"LASEGAR KALENG SIRSAK\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"productWeight\": \"325000\",\n" +
            "            \"productProfit\": \"260000\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"orderID\": \"ORD121\",\n" +
            "            \"productName\": \"LASEGAR KALENG RASPBERRY\",\n" +
            "            \"productStatus\": \"Tersedia\",\n" +
            "            \"customerID\": \"CUS9\",\n" +
            "            \"productWeight\": \"260000\",\n" +
            "            \"productProfit\": \"650000\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public final static String import_tb_customers = "{\n" +
            "    \"import_tb_customers\": [\n" +
            "        {\n" +
            "            \"customerID\": \"CUS1\",\n" +
            "            \"customerProvince\": \"Kota Tangerang\",\n" +
            "            \"customerLat\": \"-6.16825\",\n" +
            "            \"customerLng\": \"106.631\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS2\",\n" +
            "            \"customerProvince\": \"Kota Tangerang\",\n" +
            "            \"customerLat\": \"-6.19657\",\n" +
            "            \"customerLng\": \"106.64\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS3\",\n" +
            "            \"customerProvince\": \"Tangerang\",\n" +
            "            \"customerLat\": \"-6.21075\",\n" +
            "            \"customerLng\": \"106.55\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS4\",\n" +
            "            \"customerProvince\": \"Tangerang\",\n" +
            "            \"customerLat\": \"-6.24193\",\n" +
            "            \"customerLng\": \"106.563\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS5\",\n" +
            "            \"customerProvince\": \"Tangerang\",\n" +
            "            \"customerLat\": \"-6.26442\",\n" +
            "            \"customerLng\": \"106.588\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS6\",\n" +
            "            \"customerProvince\": \"Tangerang\",\n" +
            "            \"customerLat\": \"-6.31718\",\n" +
            "            \"customerLng\": \"106.494\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS7\",\n" +
            "            \"customerProvince\": \"Tangerang\",\n" +
            "            \"customerLat\": \"-6.25687\",\n" +
            "            \"customerLng\": \"106.439\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS8\",\n" +
            "            \"customerProvince\": \"Tangerang\",\n" +
            "            \"customerLat\": \"-6.31938\",\n" +
            "            \"customerLng\": \"106.627\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"customerID\": \"CUS9\",\n" +
            "            \"customerProvince\": \"Bogor\",\n" +
            "            \"customerLat\": \"-6.36879\",\n" +
            "            \"customerLng\": \"106.571\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public final static String import_tb_ordered_2 = "{\n" +
            "\"import_tb_ordered\": [\n" +
            "{\n" +
            "\"orderID\": \"ORD1\",\n" +
            "\"productName\": \"LIANG TEH CAP PISTOL BND\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 127636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD2\",\n" +
            "\"productName\": \"LASEGAR CAN CONTAINER\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD3\",\n" +
            "\"productName\": \"LIANG TEH CAP PISTOL BND6\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 127636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD4\",\n" +
            "\"productName\": \"TEANGIN\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 70909\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD5\",\n" +
            "\"productName\": \"TEANGIN CONTAINER\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 70909\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD6\",\n" +
            "\"productName\": \"TEANGIN BANDED 4\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 70909\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD7\",\n" +
            "\"productName\": \"SENDANG AIR MINUM\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 8580,\n" +
            "\"productProfit\": 953030\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD8\",\n" +
            "\"productName\": \"SENDANG AIR MINUM\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 255273\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD9\",\n" +
            "\"productName\": \"YEOS SOYA BEAN PET\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 9724,\n" +
            "\"productProfit\": 262808\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD10\",\n" +
            "\"productName\": \"WATER COOLANT\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 9750,\n" +
            "\"productProfit\": 101192\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD11\",\n" +
            "\"productName\": \"ALPO CHOP HOUSE FLM BACON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 992727\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD12\",\n" +
            "\"productName\": \"KOKO KRUNCH DUO Cereal\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 891306\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD13\",\n" +
            "\"productName\": \"FOXS FRUIT OVAL FLOWPACK\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 15600,\n" +
            "\"productProfit\": 992727\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD14\",\n" +
            "\"productName\": \"CARNATION Coffee-mateNIID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 15600,\n" +
            "\"productProfit\": 988390\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD15\",\n" +
            "\"productName\": \"MILO ACTIGEN-E BIB NI PR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"productWeight\": 26000,\n" +
            "\"productProfit\": 58500\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD16\",\n" +
            "\"productName\": \"RANESA CHICKEN LUNCHEON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8320\",\n" +
            "\"productProfit\": \"179636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD17\",\n" +
            "\"productName\": \"RANESA BEEF LUNCHEON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8320\",\n" +
            "\"productProfit\": \"179636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD18\",\n" +
            "\"productName\": \"SARDINES PREMIUM BESAR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8320\",\n" +
            "\"productProfit\": \"179636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD19\",\n" +
            "\"productName\": \"PRODUKSHINTA 05\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"9500\",\n" +
            "\"productProfit\": \"139636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD20\",\n" +
            "\"productName\": \"PRODUKSHINTA 06\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8320\",\n" +
            "\"productProfit\": \"179636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD21\",\n" +
            "\"productName\": \"LASEGAR PET\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8320\",\n" +
            "\"productProfit\": \"179636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD22\",\n" +
            "\"productName\": \"LASEGAR KALENG JAMBU\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"7000\",\n" +
            "\"productProfit\": \"127636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD23\",\n" +
            "\"productName\": \"LASEGAR KALENG JRK NPIS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8320\",\n" +
            "\"productProfit\": \"179636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD24\",\n" +
            "\"productName\": \"LASEGAR KALENG LYCHEE\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8320\",\n" +
            "\"productProfit\": \"179636\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD25\",\n" +
            "\"productName\": \"LASEGAR KALENG MELON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8840\",\n" +
            "\"productProfit\": \"1536364\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD26\",\n" +
            "\"productName\": \"LASEGAR KALENG ORANGE\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"8840\",\n" +
            "\"productProfit\": \"2009091\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD27\",\n" +
            "\"productName\": \"LASEGAR KALENG STRAWBERRY\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"11050\",\n" +
            "\"productProfit\": \"1347273\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD28\",\n" +
            "\"productName\": \"LIANG TEH CAP PISTOL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"30000\",\n" +
            "\"productProfit\": \"180818\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD29\",\n" +
            "\"productName\": \"LASEGAR KALENG RASPBERRY\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"260000\",\n" +
            "\"productProfit\": \"650000\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD30\",\n" +
            "\"productName\": \"LASEGAR KALENG SIRSAK\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"productWeight\": \"325000\",\n" +
            "\"productProfit\": \"260000\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD31\",\n" +
            "\"productName\": \"FOX KANTONG BIRU (PVAC)\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2128407\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD32\",\n" +
            "\"productName\": \"FOX KANTONG BIRU (PVAC)\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 15600,\n" +
            "\"productProfit\": 1734720\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD33\",\n" +
            "\"productName\": \"FOX KANTONG HIJAU (PVAC)\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 2024029\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD34\",\n" +
            "\"productName\": \"FOX KANTONG HIJAU (PVAC)\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 20800,\n" +
            "\"productProfit\": 1946880\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD35\",\n" +
            "\"productName\": \"FUNTIME LONG GP VANILLA\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 10140,\n" +
            "\"productProfit\": 452400\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD36\",\n" +
            "\"productName\": \"SLMT BISCUIT TIN 900GR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 23400,\n" +
            "\"productProfit\": 1046760\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD37\",\n" +
            "\"productName\": \"SLMT WAFER 750GR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 19500,\n" +
            "\"productProfit\": 942240\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD38\",\n" +
            "\"productName\": \"SLMT WAFER 340GR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 8840,\n" +
            "\"productProfit\": 636480\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD39\",\n" +
            "\"productName\": \"MACKEREL BESAR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 11050,\n" +
            "\"productProfit\": 3332727\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD40\",\n" +
            "\"productName\": \"RANESA MACKEREL CHILI\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 11050,\n" +
            "\"productProfit\": 737455\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD41\",\n" +
            "\"productName\": \"RANESA CHILI SAUCE\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 8840,\n" +
            "\"productProfit\": 425455\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD42\",\n" +
            "\"productName\": \"RANESA SWEET CHILI SAUCE\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 8840,\n" +
            "\"productProfit\": 425455\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD43\",\n" +
            "\"productName\": \"RANESA SAOS TOMAT\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 11050,\n" +
            "\"productProfit\": 1016364\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD44\",\n" +
            "\"productName\": \"RANESA MACKEREL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 11050,\n" +
            "\"productProfit\": 737455\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD45\",\n" +
            "\"productName\": \"RANESA CHILI SAUCE LEMON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"productWeight\": 8840,\n" +
            "\"productProfit\": 425455\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD46\",\n" +
            "\"productName\": \"POCARI SWEAT\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 18580,\n" +
            "\"productProfit\": 384046\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD47\",\n" +
            "\"productName\": \"POCARI SWEAT CAN BND 6\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 28580,\n" +
            "\"productProfit\": 384046\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD48\",\n" +
            "\"productName\": \"POCARI SWEAT PET BESAR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 228904\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD49\",\n" +
            "\"productName\": \"POCARI SWEAT PET K BND 6\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 39100,\n" +
            "\"productProfit\": 438516\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD50\",\n" +
            "\"productName\": \"POCARI SWEAT CAN BND 4\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 48580,\n" +
            "\"productProfit\": 384044\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD51\",\n" +
            "\"productName\": \"POCARI SWEAT PET K BND 4\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 39100,\n" +
            "\"productProfit\": 438516\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD52\",\n" +
            "\"productName\": \"POCARI SWEAT 900ML\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 23400,\n" +
            "\"productProfit\": 535002\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD53\",\n" +
            "\"productName\": \"DF HOT COCOA BAG\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 1398800\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD54\",\n" +
            "\"productName\": \"RITZ CHAMPANGE GP\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 9360,\n" +
            "\"productProfit\": 1382160\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD55\",\n" +
            "\"productName\": \"SILVER QUEEN KING STRAW\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 26000,\n" +
            "\"productProfit\": 5433480\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD56\",\n" +
            "\"productName\": \"SILVER QUEEN KING CASHEW\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 26000,\n" +
            "\"productProfit\": 5433480\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD57\",\n" +
            "\"productName\": \"ISARPLAS KALENG\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 18200,\n" +
            "\"productProfit\": 4807069\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD58\",\n" +
            "\"productName\": \"FOX KALENG KUNING\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 3349178\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD59\",\n" +
            "\"productName\": \"FOX KALENG KUNING\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 18200,\n" +
            "\"productProfit\": 3122269\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD60\",\n" +
            "\"productName\": \"FOX BOTOL PVAC PUTIH\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 1940073\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD61\",\n" +
            "\"productName\": \"CARNATION SCC Chocolate\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 9750,\n" +
            "\"productProfit\": 749268\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD62\",\n" +
            "\"productName\": \"LACTOGEN 2 Klasik BIB ID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 23400,\n" +
            "\"productProfit\": 1540604\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD63\",\n" +
            "\"productName\": \"LACTOGEN 3 PROBIOTICS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 21450,\n" +
            "\"productProfit\": 2169374\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD64\",\n" +
            "\"productName\": \"LACTOGEN 2 PROBIOTICS ID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2534981\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD65\",\n" +
            "\"productName\": \"LACTOGEN 3 PROBIOTICS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2090993\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD66\",\n" +
            "\"productName\": \"LACTOGEN 3 PROBIOTICS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 19500,\n" +
            "\"productProfit\": 2081974\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD67\",\n" +
            "\"productName\": \"LACTOGEN 2 PREBIOTICS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 19500,\n" +
            "\"productProfit\": 2310124\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD68\",\n" +
            "\"productName\": \"LACTOGEN Gro 3 Pro HONEY\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2090993\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD69\",\n" +
            "\"productName\": \"LACTOGEN Gro 3 Pro HONEY\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 19500,\n" +
            "\"productProfit\": 2169374\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD70\",\n" +
            "\"productName\": \"DANSTART 2 Probio BL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 20800,\n" +
            "\"productProfit\": 2047710\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD71\",\n" +
            "\"productName\": \"DANSTART 2 Probio BL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 2558088\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD72\",\n" +
            "\"productName\": \"DANSTART 1 Probio BL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 20800,\n" +
            "\"productProfit\": 1434004\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD73\",\n" +
            "\"productName\": \"DANSTART 1 Probio BL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 2720855\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD74\",\n" +
            "\"productName\": \"LACT. 4 Probio VANILA\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 19500,\n" +
            "\"productProfit\": 4340728\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD75\",\n" +
            "\"productName\": \"LACTOGEN 4 Probio VANILA\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2006867\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD76\",\n" +
            "\"productName\": \"LASEGAR KALENG RASPBERRY\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS6\",\n" +
            "\"productWeight\": \"260000\",\n" +
            "\"productProfit\": \"650000\"\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD77\",\n" +
            "\"productName\": \"RANESA CHICKEN LUNCHEON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD78\",\n" +
            "\"productName\": \"RANESA BEEF LUNCHEON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD79\",\n" +
            "\"productName\": \"SARDINES PREMIUM BESAR\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD80\",\n" +
            "\"productName\": \"PRODUKSHINTA 05\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD81\",\n" +
            "\"productName\": \"PRODUKSHINTA 06\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD82\",\n" +
            "\"productName\": \"LASEGAR PET\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD83\",\n" +
            "\"productName\": \"LASEGAR KALENG JAMBU\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 127636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD84\",\n" +
            "\"productName\": \"LASEGAR KALENG JRK NPIS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD85\",\n" +
            "\"productName\": \"LASEGAR KALENG LYCHEE\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8320,\n" +
            "\"productProfit\": 179636\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD86\",\n" +
            "\"productName\": \"LASEGAR KALENG MELON\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8840,\n" +
            "\"productProfit\": 1536364\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD87\",\n" +
            "\"productName\": \"LASEGAR KALENG ORANGE\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 8840,\n" +
            "\"productProfit\": 2009091\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD88\",\n" +
            "\"productName\": \"LASEGAR KALENG STRAWBERRY\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 11050,\n" +
            "\"productProfit\": 1347273\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD89\",\n" +
            "\"productName\": \"LIANG TEH CAP PISTOL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 180818\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD90\",\n" +
            "\"productName\": \"LASEGAR KALENG RASPBERRY\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 260000,\n" +
            "\"productProfit\": 650000\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD91\",\n" +
            "\"productName\": \"LASEGAR KALENG SIRSAK\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"productWeight\": 325000,\n" +
            "\"productProfit\": 260000\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD92\",\n" +
            "\"productName\": \"DC DATITA VANILA+DHA\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 26000,\n" +
            "\"productProfit\": 1392118\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD93\",\n" +
            "\"productName\": \"DC. DATITA Madu+DHA\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 26000,\n" +
            "\"productProfit\": 1392118\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD94\",\n" +
            "\"productName\": \"DC. FULL CREAMBIB PRCTNID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 2194681\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD95\",\n" +
            "\"productName\": \"DANCOW FULL CREAM FE BIB\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 20800,\n" +
            "\"productProfit\": 1992165\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD96\",\n" +
            "\"productName\": \"DC. INST EN FORT BIB\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 1825952\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD97\",\n" +
            "\"productName\": \"DC. INST EN FORT BIB\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 20800,\n" +
            "\"productProfit\": 1820385\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD98\",\n" +
            "\"productName\": \"ALPO PUPPY BEEF VEG\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 11700,\n" +
            "\"productProfit\": 345826\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD99\",\n" +
            "\"productName\": \"ALPO PUPPY BEEF VEG\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 33800,\n" +
            "\"productProfit\": 435734\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD100\",\n" +
            "\"productName\": \"DC. DATITA 5+ MADU\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 26000,\n" +
            "\"productProfit\": 1392118\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD101\",\n" +
            "\"productName\": \"DANCOW 1+COK EXC PROBIO\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 20800,\n" +
            "\"productProfit\": 1972894\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD102\",\n" +
            "\"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 26000,\n" +
            "\"productProfit\": 2329782\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD103\",\n" +
            "\"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 2011913\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD104\",\n" +
            "\"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 20800,\n" +
            "\"productProfit\": 1972894\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD105\",\n" +
            "\"productName\": \"DC. 1+ MADU EXCNUTR PRBIO\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 19500,\n" +
            "\"productProfit\": 1686029\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD106\",\n" +
            "\"productName\": \"DC. 1+ VAN EXTNUTRPROBIO\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 2011913\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD107\",\n" +
            "\"productName\": \"PRE NAN B NW026-1 S\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 1462552\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD108\",\n" +
            "\"productName\": \"FRISKIES ADL WET SEAF CAN\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 484146\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD109\",\n" +
            "\"productName\": \"FRISKIES ADL WETPR TUNA\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 10400,\n" +
            "\"productProfit\": 484146\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD110\",\n" +
            "\"productName\": \"NESTLEMom MeBLVanila ID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2205944\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD111\",\n" +
            "\"productName\": \"NESTLE Mom Me BL Choco ID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2205944\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD112\",\n" +
            "\"productName\": \"MILO CEREAL\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 8580,\n" +
            "\"productProfit\": 953030\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD113\",\n" +
            "\"productName\": \"NAN2 Probiotic LEB032 BIB\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2949856\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD114\",\n" +
            "\"productName\": \"NAN1 PROBIOTIC NWB023 BIB\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 3140878\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD115\",\n" +
            "\"productName\": \"NAN1 PROBIOTIC NWB023 BIB\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 3140878\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD116\",\n" +
            "\"productName\": \"NAN2 probiotics leb032 bb\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2876068\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD117\",\n" +
            "\"productName\": \"LACTOGEN 1 PROBIOTICS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 9100,\n" +
            "\"productProfit\": 2563179\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD118\",\n" +
            "\"productName\": \"LACTOGEN 1 PREBIOTICS\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 19500,\n" +
            "\"productProfit\": 2381068\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD119\",\n" +
            "\"productName\": \"DC. DATITA Madu+DHA ID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 1546350\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD120\",\n" +
            "\"productName\": \"DC. DATITAVanilla+DHAID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 1546350\n" +
            "},\n" +
            "{\n" +
            "\"orderID\": \"ORD121\",\n" +
            "\"productName\": \"FOXS Spring Tea Jar ID\",\n" +
            "\"productStatus\": \"Tersedia\",\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"productWeight\": 13000,\n" +
            "\"productProfit\": 144664\n" +
            "}\n" +
            "]\n" +
            "}";

    public final static String import_tb_customers_2 = "{\n" +
            "\"import_tb_customers\": [\n" +
            "{\n" +
            "\"customerID\": \"CUS1\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.928966660020919\",\n" +
            "\"customerLng\": \"107.0667403936386\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS2\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.607900262363815\",\n" +
            "\"customerLng\": \"107.12753400206566\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS3\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.735851630989372\",\n" +
            "\"customerLng\": \"107.0005900785327\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS4\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.836150633651553\",\n" +
            "\"customerLng\": \"106.95255339145662\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS5\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.616774602548527\",\n" +
            "\"customerLng\": \"106.92431040108202\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS6\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.698247793813708\",\n" +
            "\"customerLng\": \"107.14522652328014\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS7\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.734690258607476\",\n" +
            "\"customerLng\": \"106.82573653757571\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS8\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.854563229494178\",\n" +
            "\"customerLng\": \"106.81289982050656\"\n" +
            "},\n" +
            "{\n" +
            "\"customerID\": \"CUS9\",\n" +
            "\"customerProvince\": \"Jawa Barat\",\n" +
            "\"customerLat\": \"-6.93236914256211\",\n" +
            "\"customerLng\": \"106.94359213113785\"\n" +
            "}\n" +
            "]\n" +
            "}";

}
