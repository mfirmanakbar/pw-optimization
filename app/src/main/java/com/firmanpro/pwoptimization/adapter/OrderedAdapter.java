package com.firmanpro.pwoptimization.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.entity.OrderedEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;

import java.util.List;

/**
 * Created by firmanmac on 10/29/17.
 *  Urutan mudah membuat adapter:
 *  1. MyViewHolder
 *  2. extend RecyclerView.Adapter<NamaAdapter.MyViewHolder>
 *  3. implement method
 *  4. private Context context;
 *  5. List<NamaEntity> entityList;
 *  6. Buat constractor
 *  7. lengkapi onCreateViewHolder
 *  8. lengkapi getItemCount
 *  9. lengkapi onBindViewHolder
 */

public class OrderedAdapter extends RecyclerView.Adapter<OrderedAdapter.MyViewHolder> {

    private Context context;
    private List<OrderedEntity> orderedList;

    private EnumProHelper enumsHelper = new EnumProHelper();
    private OrderedEntity data;

    public OrderedAdapter(Context context, List<OrderedEntity> orderedList) {
        this.context = context;
        this.orderedList = orderedList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ordered, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        data = orderedList.get(position);
        holder.txtOrderName.setText(data.getProductName());
        holder.txtProductWeight.setText(MyConverting.format_angka(String.valueOf(data.getProductWeight())) + " gram");
        holder.txtProductProfit.setText("Rp " + MyConverting.format_angka(String.valueOf(data.getProductProfit())));
        holder.txtOrderID.setText(data.getOrderID());
        holder.txtCustomerID.setText(data.getCustomerID());
        if (data.getProductStatus().equals(enumsHelper.statusTersedia)){
            holder.linlayStatus.setBackgroundResource(R.color.colorAsbestos);
        }else if (data.getProductStatus().equals(enumsHelper.statusTerpilih)){
            holder.linlayStatus.setBackgroundResource(R.color.colorGreen);
        }else {
            holder.linlayStatus.setBackgroundResource(R.color.colorRed);
        }
    }

    @Override
    public int getItemCount() {
        return orderedList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtOrderName, txtProductWeight, txtProductProfit, txtOrderID, txtCustomerID;
        private LinearLayout linlayStatus;

        public MyViewHolder (View itemView) {
            super(itemView);
            txtOrderName = (TextView)itemView.findViewById(R.id.txtOrderName);
            txtProductWeight = (TextView)itemView.findViewById(R.id.txtProductWeight);
            txtProductProfit = (TextView)itemView.findViewById(R.id.txtProductProfit);
            txtOrderID = (TextView)itemView.findViewById(R.id.txtOrderID);
            txtCustomerID = (TextView)itemView.findViewById(R.id.txtCustomerID);
            linlayStatus = (LinearLayout) itemView.findViewById(R.id.linlayStatus);
        }
    }

}
