package com.firmanpro.pwoptimization.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.activity.GreedyActivity;
import com.firmanpro.pwoptimization.activity.GreedyresultActivity;
import com.firmanpro.pwoptimization.entity.SeqroutesEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 12/8/17.
 */

public class SeqroutesAdapter extends RecyclerView.Adapter<SeqroutesAdapter.MyViewHolder> {

    private Context context;
    private List<SeqroutesEntity> seqRoutesLists;
    private SeqroutesEntity data;
    private Intent intent;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private AlertDialog alertDialog;

    public SeqroutesAdapter(Context context, List<SeqroutesEntity> seqRoutesLists) {
        this.context = context;
        this.seqRoutesLists = seqRoutesLists;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_seqroutes, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        data = seqRoutesLists.get(position);

        final String custID = data.getCustomersID();
        final String statusDSS = data.getStatusDss();
        final int isGoodWeight = data.getIsGoodWeight();

        holder.txtCustID.setText(custID);

        if (isGoodWeight == 1) {
            holder.cv_custID.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));
        }else if(isGoodWeight == 0) {
            holder.cv_custID.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorRed));
        }else {
            holder.cv_custID.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorBlue));
        }

        holder.cv_custID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isGoodWeight == 1) {
                    Toasty.warning(context, "Mohon pilih ID yang harus dioptimasi.", Toast.LENGTH_SHORT).show();
                }else {
                    popupOptimization(custID);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return seqRoutesLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtCustID;
        private CardView cv_custID;
        public MyViewHolder (View itemView) {
            super(itemView);
            txtCustID = (TextView)itemView.findViewById(R.id.txtCustID);
            cv_custID = (CardView)itemView.findViewById(R.id.cv_custID);
        }
    }

    private void popupOptimization(final String custID) {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.popup_menu_optimization, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if (alertDialog != null && alertDialog.isShowing()) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        TextView txtPopMenuOptiTitle = (TextView) dialogView.findViewById(R.id.txtPopMenuOptiTitle);
        Button btnPopMenuOptiProfit = (Button) dialogView.findViewById(R.id.btnPopMenuOptiProfit);
        Button btnPopMenuOptiWeight = (Button) dialogView.findViewById(R.id.btnPopMenuOptiWeight);
        Button btnPopMenuOptiDensity = (Button) dialogView.findViewById(R.id.btnPopMenuOptiDensity);
        Button btnPopMenuOptiResult = (Button) dialogView.findViewById(R.id.btnPopMenuOptiResult);
        Button btnPopMenuOptiClose = (Button) dialogView.findViewById(R.id.btnPopMenuOptiClose);

        txtPopMenuOptiTitle.setText("Optimization " + custID);

        btnPopMenuOptiProfit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(context, GreedyActivity.class);
                intent.putExtra(enumProHelper.keyCustID, custID);
                intent.putExtra(enumProHelper.keyModeGreedy, enumProHelper.greedyByProfit);
                context.startActivity(intent);
            }
        });

        btnPopMenuOptiWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(context, GreedyActivity.class);
                intent.putExtra(enumProHelper.keyCustID, custID);
                intent.putExtra(enumProHelper.keyModeGreedy, enumProHelper.greedyByWeight);
                context.startActivity(intent);
            }
        });

        btnPopMenuOptiDensity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(context, GreedyActivity.class);
                intent.putExtra(enumProHelper.keyCustID, custID);
                intent.putExtra(enumProHelper.keyModeGreedy, enumProHelper.greedyByDensity);
                context.startActivity(intent);
            }
        });

        btnPopMenuOptiResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(context, GreedyresultActivity.class);
                intent.putExtra(enumProHelper.keyCustID, custID);
                context.startActivity(intent);
            }
        });

        btnPopMenuOptiClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }

}
