package com.firmanpro.pwoptimization.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.activity.ParameterActivity;
import com.firmanpro.pwoptimization.activity.ShippingpackingActivity;
import com.firmanpro.pwoptimization.activity.ShippingroutesActivity;
import com.firmanpro.pwoptimization.entity.ShippingEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.service.ShippingService;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 11/2/17.
 */

public class ShippingAdapter extends RecyclerView.Adapter<ShippingAdapter.MyViewHolder> {

    private Context context;
    private List<ShippingEntity> shippinhLists;
    private ShippingEntity data;
    private AlertDialog alertDialog;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private ShippingService shippingService;

    public ShippingAdapter(Context context, List<ShippingEntity> shippinhLists) {
        this.context = context;
        this.shippinhLists = shippinhLists;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shipping, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        data = shippinhLists.get(position);

        final String shippingIDget = data.getShippingID();
        final String shippingStatus = data.getStatus();

        if (shippingStatus.equals(enumProHelper.keyStatusCancel)){
            holder.cv_Shipping.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorRed));
        }else {
            holder.cv_Shipping.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorWet));
        }

        holder.txtShippingID.setText(shippingIDget);
        holder.txtShippingCapacity.setText(MyConverting.format_angka(String.valueOf(data.getMaxcapacity())));
        holder.txtShippingType.setText(data.getTypeResult());
        holder.txtShippingItemTotal.setText(MyConverting.format_angka(String.valueOf(data.getTotalItem())));
        holder.txtShippingWeightTotal.setText(MyConverting.format_angka(String.valueOf(data.getTotalWeigth())));
        holder.txtShippingProfitTotal.setText(MyConverting.format_angka(String.valueOf(data.getTotalProfit())));
        holder.txtShippingDistanceTotal.setText(MyConverting.meterToKM(data.getTotalDistance()));
        holder.txtShippingDurationTotal.setText(MyConverting.secondToMinutes(data.getTotalDuration()));
        holder.cv_Shipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupShippingDetail(shippingIDget);
            }
        });
    }


    private void popupShippingDetail(final String shippingID) {
        /**init dialog*/
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View dialogView = inflater.inflate(R.layout.popup_shipping_detail_type, null);
        builder.setView(dialogView);

        /**check dialog available*/
        if( alertDialog != null && alertDialog.isShowing() ) return;
        alertDialog = builder.create();

        /**start - action of dialog*/
        TextView txtTitleShipping = (TextView)dialogView.findViewById(R.id.txtTitleShipping);
        Button btnPackingPopupShipping = (Button)dialogView.findViewById(R.id.btnPackingPopupShipping);
        Button btnRoutesPopupShipping = (Button)dialogView.findViewById(R.id.btnRoutesPopupShipping);
        Button btnClosePopupShipping = (Button)dialogView.findViewById(R.id.btnClosePopupShipping);
        txtTitleShipping.setText(context.getString(R.string.shipping_view_type)+" "+shippingID);
        btnPackingPopupShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ShippingpackingActivity.class);
                intent.putExtra(enumProHelper.keyShippingID, shippingID);
                context.startActivity(intent);
                alertDialog.dismiss();
            }
        });
        btnRoutesPopupShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySession.beginInitialization(context);
                if (MySession.getSessionGlobal(MySession.key_session_warehouse_lat)==null){
                    Toasty.error(context,
                            "Maaf, mohon isi parameter titik awal transportasi pengiriman.",
                            Toast.LENGTH_SHORT, true).show();
                    context.startActivity(new Intent(context, ParameterActivity.class));
                }else {
                    Intent intent = new Intent(context, ShippingroutesActivity.class);
                    //Intent intent = new Intent(context, ShippingroutesActivity.class);
                    intent.putExtra(enumProHelper.keyShippingID, shippingID);
                    context.startActivity(intent);
                    alertDialog.dismiss();
                }
            }
        });
        btnClosePopupShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        /**end - action of dialog*/

        /**setting dialog*/
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(R.color.colorMidnightBlue);
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return shippinhLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtShippingID, txtShippingCapacity, txtShippingType, txtShippingItemTotal,
                txtShippingWeightTotal, txtShippingProfitTotal, txtShippingDistanceTotal, txtShippingDurationTotal;
        private CardView cv_Shipping;

        public MyViewHolder (View itemView) {
            super(itemView);
            txtShippingID = (TextView)itemView.findViewById(R.id.txtShippingID);
            txtShippingCapacity = (TextView)itemView.findViewById(R.id.txtShippingCapacity);
            txtShippingType = (TextView)itemView.findViewById(R.id.txtShippingType);
            txtShippingItemTotal = (TextView)itemView.findViewById(R.id.txtShippingItemTotal);
            txtShippingWeightTotal = (TextView)itemView.findViewById(R.id.txtShippingWeightTotal);
            txtShippingProfitTotal = (TextView)itemView.findViewById(R.id.txtShippingProfitTotal);
            txtShippingDistanceTotal = (TextView)itemView.findViewById(R.id.txtShippingDistanceTotal);
            txtShippingDurationTotal = (TextView)itemView.findViewById(R.id.txtShippingDurationTotal);
            cv_Shipping = (CardView)itemView.findViewById(R.id.cv_Shipping);
        }
    }

}
