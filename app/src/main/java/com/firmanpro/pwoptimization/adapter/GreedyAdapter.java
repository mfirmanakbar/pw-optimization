package com.firmanpro.pwoptimization.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.entity.GreedyEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.MyConverting;

import java.util.List;

/**
 * Created by firmanmac on 10/29/17.
 */

public class GreedyAdapter extends RecyclerView.Adapter<GreedyAdapter.MyViewHolder> {

    private Context context;
    private List<GreedyEntity> greedyLists;

    private GreedyEntity data;

    private EnumProHelper enumProHelper = new EnumProHelper();
    private String modeGreedy;

    public GreedyAdapter(Context context, List<GreedyEntity> greedyLists, String modeGreedy) {
        this.context = context;
        this.greedyLists = greedyLists;
        this.modeGreedy = modeGreedy;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_greedy, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        data = greedyLists.get(position);
        holder.txtGorderID.setText(data.getOrderID());
        holder.txtGcustID.setText(data.getCustID());
        holder.txtGweight.setText(MyConverting.format_angka(String.valueOf(data.getWeight())));
        holder.txtGprofit.setText(MyConverting.format_angka(String.valueOf(data.getProfit())));
        holder.txtGdensity.setText(String.valueOf(data.getDensity()));
        holder.txtGstatus.setText(data.getStatusGreedy());
        setColorModeGreedy(holder);
        setColorStatus(holder, data);
    }

    private void setColorStatus(MyViewHolder holder, GreedyEntity data) {
        if (data.getStatusGreedy().equals(enumProHelper.statusTerpilih)){
            holder.linlayGStatus.setBackgroundResource(R.color.colorGreen);
            holder.txtGstatus.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
        }else {
            holder.linlayGStatus.setBackgroundResource(R.color.colorRed);
        }
    }

    private void setColorModeGreedy(MyViewHolder holder) {
        holder.txtGprofit.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        holder.txtGweight.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        holder.txtGdensity.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        holder.txtGprofit.setTypeface(null, Typeface.NORMAL);
        holder.txtGweight.setTypeface(null, Typeface.NORMAL);
        holder.txtGdensity.setTypeface(null, Typeface.NORMAL);
        if (modeGreedy.equals(enumProHelper.greedyByProfit)) {
            holder.txtGprofit.setTypeface(null, Typeface.BOLD);
            holder.txtGprofit.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
        }else if (modeGreedy.equals(enumProHelper.greedyByWeight)) {
            holder.txtGweight.setTypeface(null, Typeface.BOLD);
            holder.txtGweight.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
        }else if (modeGreedy.equals(enumProHelper.greedyByDensity)) {
            holder.txtGdensity.setTypeface(null, Typeface.BOLD);
            holder.txtGdensity.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
        }
    }

    @Override
    public int getItemCount() {
        return greedyLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtGorderID, txtGcustID, txtGweight, txtGprofit, txtGdensity, txtGstatus;
        private LinearLayout linlayGStatus;

        public MyViewHolder (View itemView) {
            super(itemView);
            txtGorderID = (TextView)itemView.findViewById(R.id.txtGorderID);
            txtGcustID = (TextView)itemView.findViewById(R.id.txtGcustID);
            txtGweight = (TextView)itemView.findViewById(R.id.txtGweight);
            txtGprofit = (TextView)itemView.findViewById(R.id.txtGprofit);
            txtGdensity = (TextView)itemView.findViewById(R.id.txtGdensity);
            txtGstatus = (TextView)itemView.findViewById(R.id.txtGstatus);
            linlayGStatus = (LinearLayout) itemView.findViewById(R.id.linlayGStatus);
        }
    }

}
