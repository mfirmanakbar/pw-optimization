package com.firmanpro.pwoptimization.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.entity.GreedyresultEntity;
import com.firmanpro.pwoptimization.helper.MyConverting;

import java.util.List;

/**
 * Created by firmanmac on 11/1/17.
 */

public class GreedyresultAdapter extends RecyclerView.Adapter<GreedyresultAdapter.MyViewHolder> {

    private Context context;
    private List<GreedyresultEntity> greedyresultLists;

    private GreedyresultEntity data;

    public GreedyresultAdapter(Context context, List<GreedyresultEntity> greedyresultLists) {
        this.context = context;
        this.greedyresultLists = greedyresultLists;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_greedyresult, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        data = greedyresultLists.get(position);
        holder.txtGRorderID.setText(data.getGrorderID());
        holder.txtGRWeight.setText(MyConverting.format_angka(String.valueOf(data.getGrWeight())));
        holder.txtGRProfit.setText(MyConverting.format_angka(String.valueOf(data.getGrProfit())));
        setColorByValue(data.isGrTakeProfit(), holder.txtGRtakeProfit);
        setColorByValue(data.isGrTakeWeight(), holder.txtGRtakeWeight);
        setColorByValue(data.isGrTakeDensity(), holder.txtGRtakeDensity);
    }

    private void setColorByValue(boolean isTrue, TextView textView) {
        textView.setText("0");
        textView.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        textView.setTypeface(Typeface.DEFAULT);
        if (isTrue){
            textView.setText("1");
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorGreen));
            textView.setTypeface(Typeface.DEFAULT_BOLD);
        }
    }

    @Override
    public int getItemCount() {
        return greedyresultLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtGRorderID, txtGRWeight, txtGRProfit, txtGRtakeProfit, txtGRtakeWeight, txtGRtakeDensity;

        public MyViewHolder (View itemView) {
            super(itemView);
            txtGRorderID = (TextView)itemView.findViewById(R.id.txtGRorderID);
            txtGRWeight = (TextView)itemView.findViewById(R.id.txtGRWeight);
            txtGRProfit = (TextView)itemView.findViewById(R.id.txtGRProfit);
            txtGRtakeProfit = (TextView)itemView.findViewById(R.id.txtGRtakeProfit);
            txtGRtakeWeight = (TextView)itemView.findViewById(R.id.txtGRtakeWeight);
            txtGRtakeDensity = (TextView)itemView.findViewById(R.id.txtGRtakeDensity);
        }
    }
}
