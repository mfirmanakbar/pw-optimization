package com.firmanpro.pwoptimization.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firmanpro.pwoptimization.R;
import com.firmanpro.pwoptimization.activity.CustomerCrudActivity;
import com.firmanpro.pwoptimization.entity.CustomerEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Created by firmanmac on 10/31/17.
 */

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder>{

    private Context context;
    private List<CustomerEntity> customerLists;
    private CustomerEntity data;
    private Intent intent;
    private EnumProHelper enumProHelper = new EnumProHelper();
    private Boolean isPopupList = false;
    private TextView txtResultPopupSingleRoute;
    private android.support.v7.app.AlertDialog alertDialog;

    public CustomerAdapter(Context context, List<CustomerEntity> customerLists, boolean isPopupList,
                           TextView txtView, AlertDialog alertDialog) {
        this.context = context;
        this.customerLists = customerLists;
        this.isPopupList = isPopupList;
        this.txtResultPopupSingleRoute = txtView;
        this.alertDialog = alertDialog;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        data = customerLists.get(position);

        final String custID = data.getCustomerID();
        final String custProvince = data.getCustomerProvince();
        final double custLat = data.getCustomerLat();
        final double custLng = data.getCustomerLng();

        holder.txtCusID.setText(custID + " - " + custProvince);
        if (isPopupList){
            holder.txtCusID.setText(custID + " - " + custProvince + " (Lat: " + custLat + ", Lng: " + custLng + ")");
        }

        holder.cv_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPopupList){
                    if (txtResultPopupSingleRoute != null){
                        txtResultPopupSingleRoute.setText(custID);
                        alertDialog.dismiss();
                    }
                }else {
                    Toasty.warning(context, custProvince, Toast.LENGTH_SHORT).show();
                    intent = new Intent(context, CustomerCrudActivity.class);
                    intent.putExtra(enumProHelper.keyIsAddNewCustomer, "false");
                    intent.putExtra(enumProHelper.keyCustomersID, custID);
                    intent.putExtra(enumProHelper.keyCustomersProvince, custProvince);
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return customerLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtCusID;
        private CardView cv_customer;

        public MyViewHolder (View itemView) {
            super(itemView);
            txtCusID = (TextView)itemView.findViewById(R.id.txtCusID);
            cv_customer = (CardView)itemView.findViewById(R.id.cv_customer);
        }
    }

}
