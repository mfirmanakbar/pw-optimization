package com.firmanpro.pwoptimization.centerformula;

import android.content.Context;

import com.firmanpro.pwoptimization.adapter.GreedyAdapter;
import com.firmanpro.pwoptimization.entity.GreedyEntity;
import com.firmanpro.pwoptimization.helper.EnumProHelper;
import com.firmanpro.pwoptimization.helper.KGHelper;
import com.firmanpro.pwoptimization.helper.MySession;
import com.firmanpro.pwoptimization.service.FinalRoutesService;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by firmanmac on 10/29/17.
 */

public class KnapsackGreedy {

    private final String TAG = "kgformula_ser";
    private final String type_desc = "DESC", type_asc = "ASC";
    private long maxVolumeTruckTotal = 0;

    private ArrayList<Integer> ar_idbarang_sort_profit = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idbarang_sort_berat = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idbarang_sort_densitas = new ArrayList<Integer>();

    private ArrayList<Integer> ar_posisi_barang = new ArrayList<Integer>();
    private ArrayList<String> ar_order_id = new ArrayList<String>();
    private ArrayList<String> ar_cust_id = new ArrayList<String>();
    private ArrayList<Integer> ar_berat_barang = new ArrayList<Integer>();
    private ArrayList<Integer> ar_profit_barang = new ArrayList<Integer>();
    private ArrayList<Double> ar_hasil_densitas = new ArrayList<Double>();

    private ArrayList<Integer> ar_idposisi_profit = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idposisi_berat = new ArrayList<Integer>();
    private ArrayList<Integer> ar_idposisi_densitas = new ArrayList<Integer>();

    private HashMap<Integer,Integer> hm_data_barang1 = null;
    private HashMap<Integer,Double> hm_data_barang2 = null;

    private DecimalFormat df1 = new DecimalFormat("#.#");
    private KGHelper kgHelper = new KGHelper();
    private EnumProHelper enumProHelper = new EnumProHelper();

    public String modeGreedy = "";

    private GreedyEntity greedyEntity;

    /**start - save all greedy result*/
    private ArrayList<String> ar_gbp_result = new ArrayList<String>();
    private ArrayList<String> ar_gbw_result = new ArrayList<String>();
    private ArrayList<String> ar_gbd_result = new ArrayList<String>();
    private HashMap<String, ArrayList<String>> hashArGBResult = new HashMap<String, ArrayList<String>>();
    public String key_ar_gbp_result = "ar_gbp_result";
    public String key_ar_gbw_result = "ar_gbw_result";
    public String key_ar_gbd_result = "ar_gbd_result";
    /**end - save all greedy result*/

    public void cleanDataBarang() {
        if (hm_data_barang1 !=null) {
            hm_data_barang1.clear();
        }
        ar_posisi_barang.clear();
        ar_order_id.clear();
        ar_cust_id.clear();
        ar_berat_barang.clear();
        ar_profit_barang.clear();
        ar_idposisi_profit.clear();
        ar_idbarang_sort_profit.clear();
    }

    public void saveDataBarang(int position, String orderID, String custID, int weight, int profit) {
        ar_posisi_barang.add(position);
        ar_order_id.add(orderID);
        ar_cust_id.add(custID);
        ar_berat_barang.add(weight);
        ar_profit_barang.add(profit);
    }

    FinalRoutesService finalRoutesService = new FinalRoutesService();
    private void getMaxTruckCapacity(Context context) {
        MySession.beginInitialization(context);
        if (MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity) != null) {
            maxVolumeTruckTotal = Long.valueOf(MySession.getSessionGlobal(MySession.key_session_truck_maxcapacity));
        }
    }

    public void processKGProfit(Context context, List<GreedyEntity> greedyList, GreedyAdapter greedyAdapter, boolean show, boolean getGreedyResult) {

        /**get max truck capacity*/
        getMaxTruckCapacity(context);

        /**
         * PROFIT
         * #P1. save ke hashMap id_barang & harga_barang
         * #P2. sort harga_barang tertinggi dari hashMap
         * #P3. save ke arrayList ar_idbarang_sort_profit yang sudah diurutkan
         * #P4. munculkan data ar_idposisi_profit
         * */

        /**#P1*/
        hm_data_barang1 = new HashMap<Integer, Integer>();
        hm_data_barang1.clear();
        for (int a = 0; a< ar_posisi_barang.size(); a++) {
            Double hsl_dnt = Double.valueOf(ar_profit_barang.get(a)) / Double.valueOf(ar_berat_barang.get(a));
            ar_hasil_densitas.add(Double.valueOf(df1.format(hsl_dnt)));
            hm_data_barang1.put(ar_posisi_barang.get(a), ar_profit_barang.get(a));
        }

        /**#P2*/
        Map<Integer, Integer> map = kgHelper.sortByValues(hm_data_barang1, type_desc);
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator2.next();
            /**#P3*/
            ar_idbarang_sort_profit.add(Integer.valueOf(String.valueOf(me2.getKey())));
        }

        /**#P4*/
        int idbrg = 0;
        String statusBrg = "";
        int totalBeratNow = 0;
        for (int b = 0; b< ar_idbarang_sort_profit.size(); b++){
            statusBrg = "";
            idbrg = ar_idbarang_sort_profit.get(b);
            if (ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal && totalBeratNow <= maxVolumeTruckTotal){
                if (totalBeratNow+ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal) {
                    statusBrg = enumProHelper.statusTerpilih;
                    totalBeratNow += ar_berat_barang.get(idbrg);
                    ar_idposisi_profit.add(ar_posisi_barang.get(idbrg));
                }else {
                    statusBrg = "";
                }
            }else {
                statusBrg = "";
            }

            /**save all greedy result - profit*/
            if (getGreedyResult){
                if (statusBrg.equals(enumProHelper.statusTerpilih)){
                    ar_gbp_result.add(ar_order_id.get(idbrg));
                }
            }

            /**show it on GUI*/
            if (show) {
                greedyEntity = new GreedyEntity();
                greedyEntity.setDataPosition(ar_posisi_barang.get(idbrg));
                greedyEntity.setOrderID(ar_order_id.get(idbrg));
                greedyEntity.setCustID(ar_cust_id.get(idbrg));
                greedyEntity.setWeight(ar_berat_barang.get(idbrg));
                greedyEntity.setProfit(ar_profit_barang.get(idbrg));
                greedyEntity.setDensity(ar_hasil_densitas.get(idbrg));
                greedyEntity.setStatusGreedy(statusBrg);

                greedyList.add(greedyEntity);
                greedyAdapter.notifyDataSetChanged();
            }

        }
    }

    public void processKGBerat(Context context, List<GreedyEntity> greedyList, GreedyAdapter greedyAdapter, boolean show, boolean getGreedyResult) {

        /**get max truck capacity*/
        getMaxTruckCapacity(context);

        /**
         * BERAT
         * #P1. save ke hashMap id_barang & berat_barang
         * #P2. sort berat_barang teringan dari hashMap
         * #P3. save ke arrayList ar_idbarang_sort_berat yang sudah diurutkan
         * #P4. munculkan data ar_idposisi_berat
         * */

        /**#P1*/
        hm_data_barang1 = new HashMap<Integer, Integer>();
        hm_data_barang1.clear();
        for (int a = 0; a< ar_posisi_barang.size(); a++) {
            hm_data_barang1.put(ar_posisi_barang.get(a), ar_berat_barang.get(a));
        }

        /**#P2*/
        Map<Integer, Integer> map = kgHelper.sortByValues(hm_data_barang1, type_asc);
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator2.next();
            /**#P3*/
            ar_idbarang_sort_berat.add(Integer.valueOf(String.valueOf(me2.getKey())));
        }

        /**#P4*/
        int idbrg = 0;
        String statusBrg = "";
        int totalBeratNow = 0;
        for (int b = 0; b< ar_idbarang_sort_berat.size(); b++){
            statusBrg = "";
            idbrg = ar_idbarang_sort_berat.get(b);
            if (ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal && totalBeratNow <= maxVolumeTruckTotal){
                if (totalBeratNow+ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal) {
                    statusBrg = enumProHelper.statusTerpilih;
                    totalBeratNow += ar_berat_barang.get(idbrg);
                    ar_idposisi_berat.add(ar_posisi_barang.get(idbrg));
                }else {
                    statusBrg = "";
                }
            }else {
                statusBrg = "";
            }

            /**save all greedy result - weight*/
            if (getGreedyResult){
                if (statusBrg.equals(enumProHelper.statusTerpilih)){
                    ar_gbw_result.add(ar_order_id.get(idbrg));
                }
            }

            /**show it on GUI*/
            if (show) {
                greedyEntity = new GreedyEntity();
                greedyEntity.setDataPosition(ar_posisi_barang.get(idbrg));
                greedyEntity.setOrderID(ar_order_id.get(idbrg));
                greedyEntity.setCustID(ar_cust_id.get(idbrg));
                greedyEntity.setWeight(ar_berat_barang.get(idbrg));
                greedyEntity.setProfit(ar_profit_barang.get(idbrg));
                greedyEntity.setDensity(ar_hasil_densitas.get(idbrg));
                greedyEntity.setStatusGreedy(statusBrg);

                greedyList.add(greedyEntity);
                greedyAdapter.notifyDataSetChanged();
            }
        }
    }

    public void processKGDensitas(Context context, List<GreedyEntity> greedyList, GreedyAdapter greedyAdapter, boolean show, boolean getGreedyResult) {

        /**get max truck capacity*/
        getMaxTruckCapacity(context);

        /**
         * DENSITAS
         * #P1. save ke hashMap id_barang & densitas
         * #P2. sort densitas tertinggi dari hashMap
         * #P3. save ke arrayList ar_idbarang_sort_densitas yang sudah diurutkan
         * #P4. munculkan data ar_idposisi_densitas
         * */
        /**#P1*/

        hm_data_barang2 = new HashMap<Integer, Double>();
        hm_data_barang2.clear();
        for (int a = 0; a< ar_posisi_barang.size(); a++) {
            hm_data_barang2.put(ar_posisi_barang.get(a), ar_hasil_densitas.get(a));
        }

        /**#P2*/
        Map<Integer, Integer> map = kgHelper.sortByValues(hm_data_barang2, type_desc);
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        while(iterator2.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator2.next();
            /**#P3*/
            ar_idbarang_sort_densitas.add(Integer.valueOf(String.valueOf(me2.getKey())));
        }

        /**#P4*/
        int idbrg = 0;
        String statusBrg = "";
        int totalBeratNow = 0;
        for (int b = 0; b< ar_idbarang_sort_densitas.size(); b++){
            statusBrg = "";
            idbrg = ar_idbarang_sort_densitas.get(b);
            if (ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal && totalBeratNow <= maxVolumeTruckTotal){
                if (totalBeratNow+ar_berat_barang.get(idbrg) <= maxVolumeTruckTotal) {
                    statusBrg = enumProHelper.statusTerpilih;
                    totalBeratNow += ar_berat_barang.get(idbrg);
                    ar_idposisi_densitas.add(ar_posisi_barang.get(idbrg));
                }else {
                    statusBrg = "";
                }
            }else {
                statusBrg = "";
            }

            /**save all greedy result - density*/
            if (getGreedyResult){
                if (statusBrg.equals(enumProHelper.statusTerpilih)){
                    ar_gbd_result.add(ar_order_id.get(idbrg));
                }
            }

            /**show it on GUI*/
            if (show) {
                greedyEntity = new GreedyEntity();
                greedyEntity.setDataPosition(ar_posisi_barang.get(idbrg));
                greedyEntity.setOrderID(ar_order_id.get(idbrg));
                greedyEntity.setCustID(ar_cust_id.get(idbrg));
                greedyEntity.setWeight(ar_berat_barang.get(idbrg));
                greedyEntity.setProfit(ar_profit_barang.get(idbrg));
                greedyEntity.setDensity(ar_hasil_densitas.get(idbrg));
                greedyEntity.setStatusGreedy(statusBrg);

                greedyList.add(greedyEntity);
                greedyAdapter.notifyDataSetChanged();
            }

        }

    }

    public HashMap<String, ArrayList<String>> getResultAllGreedy(Context context){
        hashArGBResult.clear();
        ar_gbp_result.clear();
        ar_gbw_result.clear();
        ar_gbd_result.clear();
        processKGProfit(context, null, null, false, true);
        processKGBerat(context, null, null, false, true);
        processKGDensitas(context, null, null, false, true);
        hashArGBResult.put(key_ar_gbp_result,ar_gbp_result);
        hashArGBResult.put(key_ar_gbw_result,ar_gbw_result);
        hashArGBResult.put(key_ar_gbd_result,ar_gbd_result);
        return hashArGBResult;
    }

}
