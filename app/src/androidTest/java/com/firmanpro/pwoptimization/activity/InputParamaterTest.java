package com.firmanpro.pwoptimization.activity;

import android.support.test.rule.ActivityTestRule;

import com.firmanpro.pwoptimization.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by firmanmac on 11/14/17.
 */
public class InputParamaterTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private final String maxCapacity = "700000";

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testInputParameterScenario(){
        /**buka menu -> admin -> paramater*/
        onView(withId(R.id.btnMainAdmin)).perform(click());
        sleepTime(1000);
        onView(withId(R.id.btnAdminParamter)).perform(click());
        sleepTime(1000);

        /**Isi maksimal kapasitas transportasi*/
        onView(withId(R.id.txtParamMaxCapacity)).perform(clearText());
        onView(withId(R.id.txtParamMaxCapacity)).perform(typeText(maxCapacity));
        closeSoftKeyboard();
        onView(withId(R.id.txtParamMaxCapacity)).check(matches(withText(maxCapacity)));
        sleepTime(1000);

        /**set lokasi gudang A*/
        onView(withId(R.id.btnWarehouseA)).perform(click());
        sleepTime(1000);

        /**update paramater pada session*/
        onView(withId(R.id.btnParamUpdate)).perform(click());
        sleepTime(1000);

    }


    private void sleepTime(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

}