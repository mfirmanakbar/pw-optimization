package com.firmanpro.pwoptimization.activity;

import android.support.test.rule.ActivityTestRule;

import com.firmanpro.pwoptimization.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by firmanmac on 2/11/18.
 */

public class OrderListActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void OrderListActivityTest(){
        onView(withId(R.id.btnMainOredered)).perform(click());
        sleepTime(4000);
    }

    private void sleepTime(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

}
