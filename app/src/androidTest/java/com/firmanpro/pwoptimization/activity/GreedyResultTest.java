package com.firmanpro.pwoptimization.activity;

import android.app.Activity;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;

import static android.support.test.InstrumentationRegistry.getInstrumentation;


/**
 * Created by firmanmac on 11/14/17.
 */
public class GreedyResultTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testViewGreedyResultScenario(){
        /*onView(withId(R.id.btnMainByprofit)).perform(click());
        sleepTime(2000);
        getActivityInstance().finish();
        onView(withId(R.id.btnMainByweight)).perform(click());
        sleepTime(2000);
        getActivityInstance().finish();
        onView(withId(R.id.btnMainBydensity)).perform(click());
        sleepTime(2000);
        getActivityInstance().finish();
        sleepTime(1000);*/
    }


    private Activity getActivityInstance(){
        final Activity[] currentActivity = {null};

        getInstrumentation().runOnMainSync(new Runnable(){
            public void run(){
                Collection<Activity> resumedActivity = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
                Iterator<Activity> it = resumedActivity.iterator();
                currentActivity[0] = it.next();
            }
        });

        return currentActivity[0];
    }

    private void sleepTime(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

}