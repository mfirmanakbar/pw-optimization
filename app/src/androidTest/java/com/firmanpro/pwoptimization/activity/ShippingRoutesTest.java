package com.firmanpro.pwoptimization.activity;

import android.app.Activity;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;

import com.firmanpro.pwoptimization.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


/**
 * Created by firmanmac on 11/14/17.
 */
public class ShippingRoutesTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testCreateKnapsackMaxDensityScenario(){
        onView(withId(R.id.btnMainShipping)).perform(click());
        sleepTime(2000);
        onView(withId(R.id.rv_shipping)).perform(actionOnItemAtPosition(0, click()));
        sleepTime(1000);
        onView(withId(R.id.btnRoutesPopupShipping)).perform(click());
        sleepTime(3000);
        onView(withId(R.id.btnAllPath)).perform(click());
        sleepTime(3000);
        onView(withId(R.id.btnBestPath)).perform(click());
        sleepTime(3000);
    }


    private Activity getActivityInstance(){
        final Activity[] currentActivity = {null};

        getInstrumentation().runOnMainSync(new Runnable(){
            public void run(){
                Collection<Activity> resumedActivity = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
                Iterator<Activity> it = resumedActivity.iterator();
                currentActivity[0] = it.next();
            }
        });

        return currentActivity[0];
    }

    private void sleepTime(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {
    }

}